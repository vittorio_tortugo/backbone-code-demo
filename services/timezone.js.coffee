class Stoege.Services.Timezone
  save_to_cookie: ->
    timezone = jstz.determine()
    $.cookie('client_timezone', timezone.name(), { path: '/', expires: 20000 }) if timezone?
