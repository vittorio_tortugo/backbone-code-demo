class Stoege.Services.Countdown
  @zero_based_format: (number) ->
    number = Math.floor number
    number = "0#{number}" if number < 10
    number

  @get_time_diff_from_now: (time) ->
    parseInt (new Date(time) - new Date()) / 1000

  @get_countdown_data_from_now: (time) ->
    countdown_time_diff = @get_time_diff_from_now(time)
    countdown_time_diff = 0 if countdown_time_diff < 0
    data =
      countdown_days: @zero_based_format countdown_time_diff / (60 * 60 * 24)
      countdown_hours: @zero_based_format (countdown_time_diff / (60 * 60)) % 24
      countdown_minutes: @zero_based_format (countdown_time_diff / (60)) % 60
      countdown_time_diff: countdown_time_diff
