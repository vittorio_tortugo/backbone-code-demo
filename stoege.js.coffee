#= require_self
#= require_tree ./templates
#= require_tree ./models
#= require_tree ./collections
#= require_tree ./views
#= require_tree ./routers
#= require_tree ./services

window.Stoege =
  Models: {}
  Collections: {}
  Routers: {}
  Views: {}
  Services: {}
  Constants: {}
  init: ->
    new Stoege.Views.HeaderView()
    new Stoege.Views.FoxfordGlobalView()
    new Stoege.Routers.Landings()
    new Stoege.Routers.ContentPages()
    new Stoege.Routers.Course()
    new Stoege.Routers.CoursesLibrary()
    new Stoege.Routers.EventsLibrary()
    new Stoege.Routers.UserCourses()
    new Stoege.Routers.Teachers()
    new Stoege.Routers.FeedbackRouter()
    new Stoege.Routers.Rating()
    new Stoege.Routers.Account()
    new Stoege.Routers.CartRouter()
    new Stoege.Routers.TasksRouter()
    new Stoege.Routers.PagesRouter()
    new Stoege.Routers.ConspectsRouter()
    new Stoege.Routers.PasswordRouter()
    new Stoege.Routers.Promos()
    new Stoege.Routers.DialogsRouter()
    new Stoege.Routers.SmartBanner()

    Backbone.history.start(pushState: true)

$ ->
  initialize_routers_callback()
  Stoege.init()

initialize_routers_callback = ->
  registrationDialogModel = new Stoege.Models.RegistrationDialog
  registrationDialogView = new Stoege.Views.RegistrationDialog(model: registrationDialogModel)

  Backbone.Router::after = =>
    params = Backbone.History.prototype.getQueryParameters(window.location.search)
    if params['registration']?
      registrationDialogView.open_registration(params)
    else
      registrationDialogView.destroy() if $('.registration_dialog').length

    if params['dialog'] is 'feedback'
      feedback_dialog_view = new Stoege.Views.FeedbackDialogView
      feedback_dialog_view.open_feedback()

    if params['dialog'] is 'subscribe'
      subscription_dialog_view = new Stoege.Views.SubscriptionDialog
