class Stoege.Views.CoursesLibraryList extends Backbone.View

  el: '.library_courses_wrapper'

  course_template: JST["backbone/templates/course/course_list_item"]
  not_found_template: JST["backbone/templates/course/not_found"]

  events:
    'mouseenter .courses_list > li' : 'set_course_hover_color'
    'mouseleave .courses_list > li' : 'set_course_default_color'
    'click .warning_banner > .close_button' : 'close_banner'

  initialize: ({@collection_grades, @collection, @params}) ->
    @listenTo(@collection, 'reset', @render, @)
#    TODO починить в STOEGE-5507
#    @set_meta_info()
    @load_courses()

    $(window).on 'scroll', =>
      @load_more_courses()

  load_courses: ->
    grade_id = @collection_grades.findWhere(selected: true)?.get('id')
    @params.grade_id = grade_id if grade_id and grade_id isnt '0'
    delete @params.grade_id if not grade_id or @params.grade_id is '0'

    @collection.state.currentPage = 1
    @$('.courses_list').empty()

    @collection.fetch(data: @params)

  render: ->
    if @collection.length
      @collection.each (course) =>
        course_view = new Stoege.Views.CourseLibraryItem(model: course)
        @$('.courses_list').append(course_view.render().el)
      @load_more_courses()
    else
      @$('.courses_list').html(@not_found_template()) if @collection.state.currentPage is 1

    @$('.load_more_courses').addClass('hidden')
    @$(".dotdotdot").dotdotdot
      watch: "window"
    @

  load_more_courses: ->
    return if !@collection.hasNextPage() or @collection.length < 12
    scroll_bottom = $(window).scrollTop() + $(window).height()
    course_top = $('.courses_list > li').last().offset().top
    if course_top < scroll_bottom
      @collection.getNextPage
        data: @params
      @$('.load_more_courses').removeClass('hidden')

  set_course_hover_color: (e) ->
    $target = $(e.target).closest('li').find('.course_icon_wrapper')
    dark_color = $target.data('hover_color')

    $target.css('background', "##{dark_color}")

  set_course_default_color: (e) ->
    $target = $(e.target).closest('li').find('.course_icon_wrapper')
    color = $target.data('color')

    $target.css('background', "##{color}")

  remove_view: ->
    @undelegateEvents()
    @stopListening(@collection, 'reset')
    $(window).off('scroll')

#  set_meta_info: ->
#    discipline_id = parseInt(@params.discipline_id) if @params.discipline_id?
#    course_type_id = parseInt(@params.course_type_id) if @params.course_type_id?
#    level_id = parseInt(@params.level_id) if @params.level_id?
#
#    courses_meta = [
#      discipline_id: 1
#      course_type_id: 2
#      title: 'Математика краткий курс'
#      meta_description: 'Краткий курс математики для школьников в Фоксфорд, поможет сдать экзамены на отлично.'
#    ,
#      discipline_id: 1
#      level_id: 1
#      title: 'Базовый курс математики'
#      meta_description: 'Фоксфорд предлагает изучить базовый курс по метематике для школьников. Наши учителя помогут вам освоить основы математики.'
#    ,
#      discipline_id: 2
#      level_id: 1
#      title: 'Базовый курс информатики'
#      meta_description: 'Пройдите базовый курс по информатике в учебном центре Фоксфорд.'
#    ,
#      discipline_id: 3
#      course_type_id: 2
#      title: 'Экспресс курс физики'
#      meta_description: 'Экспресс курс по физики с лучшими учителями страны в центре онлайн образования Фоксфорд.'
#    ,
#      discipline_id: 4
#      course_type_id: 2
#      title: 'Краткий интенсивный курс русского языка'
#      meta_description: 'Интенсивные экспресс курсы по русскому языку в центре онлайн образования Фоксфорд.'
#    ,
#      discipline_id: 4
#      level_id: 1
#      title: 'Базовый курс русского языка'
#      meta_description: 'Базовые курсы по русскому языку онлайн для школьников в центре обучения Фоксфорд.'
#    ,
#      discipline_id: 7
#      title: 'Курс истории онлайн для школьников 5, 6, 7, 8, 9, 10 и 11 классов'
#      meta_description: 'Школьный курс истории в центре онлайн обучения Фоксфорд. У нас всегда инстересно и позновательно.'
#    ,
#      discipline_id: 8
#      title: 'Курсы английского языка онлайн для школьников 5, 6, 7, 8, 9, 10 и 11 классов'
#      meta_description: 'Лучшие курсы английского онлайн в Фоксфорд. Изучение английского языка у нас - это всегда интересно.'
#    ,
#      discipline_id: 8
#      course_type_id: 2
#      level_id: 1
#      title: 'Базовый курс английского языка'
#      meta_description: 'Подготовительные базовые курсы по английскому языку в центре онлайн образования Фоксфорд.'
#    ,
#      discipline_id: 9
#      title: 'Онлайн курсы по биологии для школьников 5, 6, 7, 8, 9, 10 и 11 классов'
#      meta_description: 'Изучение школьного курса биологии с лучшими учителями со всей России. Вам осталось только выбрать необходимый курс по биологии и начать дистанционное обучение.'
#    ,
#      discipline_id: 9
#      level_id: 2
#      title: 'Биология углубленный курс'
#      meta_description: 'Начните изучать углубленный курс по биологии онлайн.'
#    ,
#      discipline_id: 10
#      title: 'Курсы по химии онлайн для школьников 5, 6, 7, 8, 9, 10 и 11 классов'
#      meta_description: 'Изучайте школьный курс химии онлайн с нашими преподавателями. Наш учебный центр Фоксфорд - это высшие стандарты образования.'
#    ]
#
#    meta_data = courses_meta.filter (item) ->
#      item.discipline_id is discipline_id and item.course_type_id is course_type_id and item.level_id is level_id
#
#    if meta_data.length
#      meta = meta_data[0]
#      @page_model.set
#        title: meta.title
#        meta_description: meta.meta_description

  change_title: ->
    document.title = @page_model.get('title')

  change_meta_description: ->
    $('meta[name=description]').attr('content', @page_model.get('meta_description'))

  close_banner: ->
    $('.warning_banner').addClass('hidden')
