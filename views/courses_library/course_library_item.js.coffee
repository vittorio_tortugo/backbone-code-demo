class Stoege.Views.CourseLibraryItem extends Backbone.View

  tagName: 'li'

  template: JST["backbone/templates/course/course_item"]

  render: ->
    @$el.html(@template(course: @model.toJSON()))
    @
