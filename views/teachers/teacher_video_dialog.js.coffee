class Stoege.Views.TeacherVideoDialog extends Stoege.Views.Dialog

  template: JST["backbone/templates/teacher_video_dialog"]

  events:
    'click .close_button' : 'close'

  className: 'teacher_video_dialog'

  open: ->
    @render()
    super

  close: ->
    @$el.dialog('close')
