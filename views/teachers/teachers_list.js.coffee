class Stoege.Views.TeachersList extends Backbone.View

  el: '.teachers_list'

  not_found_template: JST["backbone/templates/not_found"]

  initialize: (options)->
    @params = _.clone(options.params)
    @alias_url = options.alias_url
    @listenTo(@collection, 'reset', @render, @)
    @listenTo(@collection, 'error', @redirect, @)
    @load_teachers()

    $(window).scroll =>
      @load_more_teachers()

  render: ->
    if @collection.length
      @collection.each (teacher) =>
        teacher_view = new Stoege.Views.TeacherItem(model: teacher)
        @$el.append(teacher_view.render().el)
    else
      @$el.html(@not_found_template(text: 'преподавателя')) if @collection.state.currentPage is 1
    @

  redirect: ->
    window.location = '/404'

  load_teachers: ->
    @$el.html('')
    @params.alias_url = @alias_url if @alias_url
    @collection.fetch
      data: @params

  load_more_teachers: ->
    return if !@collection.hasNextPage() or !@collection.length or @collection.length < 10
    return unless $('.teachers_list li').last().length
    if $('.teachers_list li').last().offset().top < ($(window).scrollTop() + $(window).height())
      @collection.getNextPage
        data: @params
        beforeSend: =>
          @$el.after('<div class="teachers_loaded">ЗАГРУЖАЕМ ОСТАЛЬНЫХ ПРЕПОДАВАТЕЛЕЙ</div>')
        complete: ->
          $('.teachers_loaded').remove()

  remove_view: ->
    @undelegateEvents()
    @stopListening(@collection, 'reset')
    @stopListening(@collection, 'error')
    $(window).off('scroll')
