class Stoege.Views.TeacherItem extends Backbone.View

  tagName: 'li'

  className: 'teacher'

  template: JST["backbone/templates/teacher"]
  courses_template: JST["backbone/templates/teacher_courses_block"]

  events:
    'click .teacher_comments .value.pseudo' : 'get_comments'
    'click .teacher_courses .text.pseudo' : 'get_courses'
    'click .teacher_footer .teacher_video' : 'open_video'

  initialize: ->
    @listenTo(@model, 'change:courses_opened', @toggle_courses_slide_block)
    @listenTo(@model, 'change:comments_opened', @toggle_comments_slide_block)

  render: ->
    @$el.html(@template(@model.toJSON()))
    @

  get_comments: ->
    if @model.get('comments_opened')
      @model.set(comments_opened: false)
    else
      @comments_wrapper_view.remove_view() if @comments_wrapper_view
      @comments_wrapper_view = new Stoege.Views.TeacherCommentsWrapper(teacher: @model)
      @comments_wrapper_view.get_all_comments()
      @listenTo(@comments_wrapper_view, 'rendered', @set_comments_opened, @)

  get_courses: (e) ->
    @model.set(animation: true)
    if @model.get('courses_opened')
      @model.set(courses_opened: false)
    else
      if @model.get('courses_collection')
        @model.set(courses_opened: true)
      else
        @model.set(courses_collection: new Stoege.Collections.TeacherCourses())
        @listenTo(@model.get('courses_collection'), 'sync', @set_courses_opened, @)
        courses_collection = @model.get('courses_collection')
        courses_collection.fetch(url: @model.get('courses_url'))

  render_teacher_courses: ->
    @$('.teacher_slide_block').html(@courses_template(courses: @model.get('courses_collection').toJSON()))

  render_teacher_comments: ->
    @$('.teacher_slide_block').html(@comments_wrapper_view.el)

  set_courses_opened: ->
    @model.set(courses_opened: true)

  set_comments_opened: ->
    @model.set(comments_opened: true)

  toggle_courses_slide_block: (model, value) ->
    if value
      if @model.get('comments_opened')
        @close_comments_slide_block(true)
      else
        @open_courses_slide_block()
    else
      @close_courses_slide_block()

  toggle_comments_slide_block: (model, value) ->
    if value
      if @model.get('courses_opened')
        @close_courses_slide_block(true)
      else
        @open_comments_slide_block()
    else
      @close_comments_slide_block()

  open_courses_slide_block: ->
    @render_teacher_courses()
    @$('.teacher_courses .text.pseudo').html('Скрыть курсы')
    @$('.teacher_slide_block').slideDown =>
      @model.set(animation: false)

  close_courses_slide_block: ->
    @$('.teacher_courses .text.pseudo').html("Курсы преподавателя (#{@model.get('stats').courses_count})")
    @$('.teacher_slide_block').slideUp =>
      @model.set(animation: false)
      if @model.get('comments_opened')
        @model.set(courses_opened: false)
        @open_comments_slide_block()

  open_comments_slide_block: ->
    @render_teacher_comments()
    @$('.teacher_comments .value.pseudo').html('Скрыть отзывы')
    @$('.teacher_slide_block').slideDown =>
      @model.set(animation: false)

  close_comments_slide_block: ->
    @$('.teacher_comments .value.pseudo').html("#{@model.get('stats').comments_count} #{@model.get('stats').comments_count_text}")
    @$('.teacher_slide_block').slideUp =>
      @model.set(animation: false)
      if @model.get('courses_opened')
        @model.set(comments_opened: false)
        @open_courses_slide_block()

  open_video: ->
    @teacherVideoView = new Stoege.Views.TeacherVideoDialog(model: @model)
    @teacherVideoView.open()
