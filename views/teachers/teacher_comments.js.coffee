class Stoege.Views.TeacherComments extends Backbone.View

  tagName: 'ul'
  className: 'comments'

  initialize: (options) ->
    @teacher = options.teacher
    @listenTo(@collection, 'reset', @render, @)

  render: ->
    @$el.empty()
    for comment in @collection.models
      teacher_comment_view = new Stoege.Views.TeacherComment(model: comment)
      @$el.append(teacher_comment_view.render().el)
    @
