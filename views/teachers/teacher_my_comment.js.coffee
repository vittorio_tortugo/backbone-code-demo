class Stoege.Views.TeacherMyComment extends  Stoege.Views.MyComment
  className: 'teacher_my_comment'
  tagName: 'div'
  el: ''

  events:
    'click .edit' : 'render_form'
    'click .send_button:not(.disabled)' : 'save'
    'keyup .textarea_box textarea' : 'update_text'

  template: JST["backbone/templates/course/my_comment"]

  initialize: (options) ->
    @text = 'преподавателя'
    @teacher = options.teacher
    @listenTo(@model, 'sync', @render, @)
    @listenTo(@model, 'error', @render_form, @)
    @listenTo(@model, 'change:text', @rerender_form, @)

  render_form: ->
    @$el.html(@form_template(comment: @model.toJSON(), text: @text))
    @move_cursor_to_input_end(@$('textarea')[0])
    @

  save: ->
    if @model.get('text')
      @model.save()
    else
      @show_error(@$('.send_button.blue_button'), @$('.send_comment'), "#{you_text('Оставьте', 'Оставь')} отзыв для #{@text}")

  render: ->
    #TODO какой то копипастный говнокод
    # Если модель не сохранена - рендерим форму
    unless @model.get('id')
      return @render_form()

    @$el.html(@template(comment: @model.toJSON()))
    @

  rerender_form: ->
    return unless @model.get('id')
    if @model.get('text').length == 0 || (@model.previous('text').length == 0 && @model.get('text').length > 0)
      @render_form()
