class Stoege.Views.TeacherCommentsWrapper extends Backbone.View

  className: 'comments_wrapper'

  events:
    'click .more_comments' : 'load_more_comments'

  initialize: (options) ->
    @teacher = options.teacher
    @set_comments()
    @set_my_comment() if user.is_authorized()

    @listenTo(@teacher, 'change:comments_loaded', @render, @)
    @listenTo(@teacher, 'change:my_comment_loaded', @render, @)

  get_all_comments: ->
    @reset_loades()
    @comments_collection.fetch()
    @my_comment_model.fetch(url: "/api/teachers/#{@teacher.id}/comments/mine") if user.is_authorized()

  reset_loades: ->
    @teacher.set(comments_loaded: false)
    # если нет пользователя, то выставляем для my_comment_loaded true
    @teacher.set(my_comment_loaded: !user.is_authorized())

  set_comments: ->
    @comments_collection = new Stoege.Collections.TeacherComments(teacher_id: @teacher.get('id')) unless @comments_collection
    @comments_view = new Stoege.Views.TeacherComments(collection: @comments_collection) unless @comments_view
    @listenTo(@comments_collection, 'reset', @set_comments_loaded, @)

  set_my_comment: ->
    @my_comment_model = new Stoege.Models.TeacherMyComment(teacher: @teacher) unless @my_comment_model
    @my_comment_view = new Stoege.Views.TeacherMyComment(model: @my_comment_model, teacher: @teacher)
    @listenTo(@my_comment_model, 'sync', @set_my_comment_loaded, @)
    @listenTo(@my_comment_model, 'error', @set_my_comment_loaded, @)

  render: ->
    # если загружены все комменты и коммент пользователя, то регдерим
    return unless @teacher.get('comments_loaded') && @teacher.get('my_comment_loaded')

    @$el.append(@my_comment_view.el) if user.is_authorized()
    @$el.append(@comments_view.el)
    if (@teacher.get('stats').comments_count - @teacher.get('stats').my_comment_count) > 3
      @$el.append('<div class="more_comments"><span class="pseudo">Другие отзывы</span></div>')

    @trigger('rendered')
    @

  load_more_comments: ->
    if ((@teacher.get('stats').comments_count - @teacher.get('stats').my_comment_count)  - @comments_collection.state.currentPage*3) > 0
      @comments_collection.getNextPage()
    else
      @comments_collection.getFirstPage
        fetch: true

  set_comments_loaded: ->
    @teacher.set(comments_loaded: true)

  set_my_comment_loaded: ->
    @teacher.set(my_comment_loaded: true)

  remove_view: ->
    @undelegateEvents()
    @.remove()
