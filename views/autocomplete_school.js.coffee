#= require ./application.js.coffee
class Stoege.Views.AutocompleteSchool extends Stoege.Views.Application

  initialize: ->
    @$country_select = @$('#school_country_id')
    @$region_name = @$('#user_pupil_school_attributes_address_attributes_region_name')
    @$region_id = @$('#user_pupil_school_attributes_address_attributes_region_id')
    @$city_name = @$('#user_pupil_school_attributes_address_attributes_city_name')
    @$city_id = @$('#user_pupil_school_attributes_address_attributes_city_id')
    @$school_name = @$('#user_pupil_school_attributes_name')
    @$school_id = @$('#user_pupil_school_attributes_id')

    @change_country()
    @change_region()
    @change_city()
    @remove_no_existing_match()

  clean_fields: ($name_field, $id_field) ->
    $name_field.val('')
    $id_field.val('')

  disable_fields: ($name_field, $id_field) ->
    $name_field.attr('disabled', 'disabled').attr('data-disabled', true).addClass('disabled')
    $id_field.attr('disabled', 'disabled').attr('data-disabled', true)

  enable_fields: ($name_field, $id_field) ->
    $name_field.removeAttr('disabled').attr('data-disabled', false).removeClass('disabled')
    $id_field.removeAttr('disabled').attr('data-disabled', false)

  change_country: ->
    @$country_select.change =>
      country_id = @$country_select.val()

      @clean_fields(@$region_name, @$region_id)
      @clean_fields(@$city_name, @$city_id)
      @clean_fields(@$school_name, @$school_id)
      @disable_fields(@$city_name, @$city_id)
      @disable_fields(@$school_name, @$school_id)
      @$region_name.attr('data-autocomplete', "/api/regions?country_id=#{country_id}")

  change_region: ->
    @$region_name.on 'autocompleteselect', (event, ui) =>
      region_id = $(ui.item)[0].id

      @clean_fields(@$city_name, @$city_id)
      @enable_fields(@$city_name, @$city_id)
      @clean_fields(@$school_name, @$school_id)
      @$city_name.attr('data-autocomplete', "/api/cities?region_id=#{region_id}")

    @$region_name.on 'autocompletechange', (event, ui) =>
      unless ui.item
        @clean_fields(@$city_name, @$city_id)
        @disable_fields(@$city_name, @$city_id)
        @clean_fields(@$school_name, @$school_id)
        @disable_fields(@$school_name, @$school_id)
        error_message = if @$region_name.val() then 'Такого нет в списке' else 'Не может быть пустым'
        @show_error(@$region_name, @$region_name.parent(), error_message)

    @$region_name.on 'keyup', =>
      unless @$region_name.val()
        @clean_fields(@$city_name, @$city_id)
        @disable_fields(@$city_name, @$city_id)
        @clean_fields(@$school_name, @$school_id)
        @disable_fields(@$school_name, @$school_id)

  change_city: ->
    @$city_name.on 'autocompleteselect', (event, ui) =>
      city_id = $(ui.item)[0].id

      @clean_fields(@$school_name, @$school_id)
      @enable_fields(@$school_name, @$school_id)
      @$school_name.attr('data-autocomplete', "/api/schools?approved=true&city_id=#{city_id}")

    @$city_name.on 'keyup', =>
      @clean_fields(@$school_name, @$school_id)
      if @$city_name.val()
        @enable_fields(@$school_name, @$school_id)
      else
        @disable_fields(@$school_name, @$school_id)

  remove_no_existing_match: ->
    @$el.on 'autocompleteresponse', '#user_pupil_school_attributes_address_attributes_region_name, \
                                     #user_pupil_school_attributes_address_attributes_city_name, \
                                     #user_pupil_school_attributes_name', (event, ui) ->
      $(@).autocomplete('close') if ui.content?[0].id.length == 0
