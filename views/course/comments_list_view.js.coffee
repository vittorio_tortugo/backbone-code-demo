class Stoege.Views.CommentsListView extends Backbone.View

  el: 'ul.comments'

  template: JST["backbone/templates/my_comment"]
  form_template: JST["backbone/templates/my_comment_form"]

  initialize: ({@course_id}) ->
    @course_model = new Backbone.Model id: @course_id
    @collection = new Stoege.Collections.Comments(null, course_model: @course_model) unless @collection

  render: ->
    @listenTo @collection, 'reset', @render_comments_list, @
    @collection.getFirstPage()
    @

  render_comments_list: ->
    for comment in @collection.models
      comment_view = new Stoege.Views.CommentView(model: comment)
      @$el.append(comment_view.render().el)


  add_my_comment: ->
    @my_comment = new Stoege.Models.MyComment(course: @course)
    new Stoege.Views.MyComment(model: @my_comment, course: @course)
    @my_comment.fetch(url: "/api/courses/#{@course.id}/my_comment")

  load_more_items: (e) ->
    e.preventDefault()
    @collection.getNextPage()
    @

  remove: ->
    @stopListening()
    @