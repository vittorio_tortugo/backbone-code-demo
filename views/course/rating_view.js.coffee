class Stoege.Views.CourseRatingView extends Backbone.View

  template_rating_list: JST["backbone/templates/course_rating_list"]
  template_rating_list_item: JST["backbone/templates/rating_list_item"]
  template_rating_list_empty: JST["backbone/templates/rating_list_empty"]

  events:
    'click tfoot .load_more': 'load_more_items'

  initialize: ({@el, @course_model, @refresh_callback}) ->
    @rating_collection = @rating_collection or new Stoege.Collections.CourseRating
    @rating_collection.course_id = @course_model.id
    @rating_collection.state.pageSize = 10
    @

  render: ->
    @$el.html @template_rating_list title: 'Рейтинг учеников по курсу'
    @refresh_callback()
    @listenTo @rating_collection, 'reset', @update_list
    @rating_list = @$('.fxf_rating_list')
    @prepend_current_user_flag = false
    @rating_collection.getFirstPage()
    @

  update_list: ->
    items_elements = []
    
    if @rating_collection.length
      @rating_collection.each (model) =>
        if @rating_collection.user_rating and (@rating_collection.user_rating.position > 10 and @rating_collection.state.currentPage is 1) and !@prepend_current_user_flag
          items_elements.unshift @template_rating_list_item @rating_collection.user_rating
          @prepend_current_user_flag = true
          if model.id isnt @rating_collection.user_rating.user.id
            items_elements.push @template_rating_list_item model.toJSON()
        else
          items_elements.push @template_rating_list_item model.toJSON()

      @rating_list.children('tbody').append items_elements.join ''
      @rating_list.children('tfoot').toggleClass 'hidden', @rating_collection.length < 10
      @rating_list.children('thead').toggleClass 'hidden', !@rating_list.children('tbody').text().length
    else
      @rating_list.html @template_rating_list_empty
    @refresh_callback false
    @

  load_more_items: (e) ->
    e.preventDefault()
    @rating_collection.getNextPage()
    @

  remove: ->
    @stopListening()
    @
