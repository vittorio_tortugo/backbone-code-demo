class Stoege.Views.CommentView extends Backbone.View

  tagName: 'li'
  className: 'comment'

  template: JST["backbone/templates/course/comment"]

  render: ->
    @$el.html @template comment: @model.toJSON()
    @
