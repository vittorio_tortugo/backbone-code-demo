class Stoege.Views.MoreCommentsLink extends Backbone.View
    
  el: '.comments_content .more_comments'

  events:
    'click .more_comments_link' : 'load_more'

  more_comments_link_template: JST["backbone/templates/course/more_comments_link"]

  initialize: (options) ->
    @listenTo(@collection, 'reset', @render, @)

    # нужно для первого корректного отображения ссылки
    @listenTo(@model, 'change', @render, @)

  render: ->
    @$el.html(@more_comments_link_template(comments_remain: @comments_remain()))

  comments_remain: ->
    if (@model.get('comments_count') - @collection.fullCollection.length) >= 10
      10
    else
      @model.get('comments_count') - @collection.fullCollection.length

  load_more: ->
    @collection.getNextPage()
