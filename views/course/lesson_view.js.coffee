class Stoege.Views.CourseLessonView extends Backbone.View

  el: '.lesson_content_box'

  events:
    'click .video_overlay': 'play_video'
    'click .open_cart.fxf_button' : 'open_cart'
    'click .navigation_link': 'open_cart' # TODO грязный хак, не делайте так, дети

  initialize: ({@course_model}) ->
    @listenTo @course_model.top_menu_collection, 'change:active', @render_content
    if @$el[0]
      $('.wrapper_add').css paddingBottom: 0
      unless @lessons_scroll
        @lessons_scroll = new IScroll '.lesson_content_box > div',
          click: true
          scrollbars: true
          probeType: 3
          mouseWheel: true
          fadeScrollbars: true
          disableMouse: false
          interactiveScrollbars: true

    @is_kpk = @$('.lesson_banner').data('type') is 3
    @is_from_registration = parseInt window.localStorage.getItem 'is_from_registration'

    $(window).resize => _.debounce @set_elements_height(), 300
    @render_content @course_model.top_menu_collection.findWhere active: true
    @initialize_video_messages()
    @open_cart() if @is_from_registration and @is_kpk
    window.localStorage.removeItem 'is_from_registration'
    @

  render: ->
    @set_elements_height()
    @

  render_content: (model) ->
    if model.get 'active'
      params =
        el: '.lesson_content'
        course_model: @course_model
        refresh_callback: @set_elements_height

      @current_view?.remove()

      switch model.get 'id'
        when 'comments'
          @comments_view = new Stoege.Views.CommentsView(params)
          @comments_view.render()
          @current_view = @comments_view
        when 'rating'
          @rating_view or= new Stoege.Views.CourseRatingView(params)
          @rating_view.render()
          @current_view = @rating_view
        when 'about'
          @about_view or= new Stoege.Views.CourseAboutView(params)
          @about_view.render()
          @current_view = @about_view
        when 'program'
          params.refresh_callback = =>
            @set_elements_height()
            _.delay =>
              if localStorage.getItem('lesson_video_options') is 'autoplay'
                localStorage.removeItem('lesson_video_options')
                @play_video()
            , 1000

          @program_view or= new Stoege.Views.CourseProgramView(params)
          @program_view.render(@lessons_scroll)
          @current_view = @program_view

      @set_elements_height()
    @

  set_elements_height: (scroll_to_top = true) =>
    lessons_box_height = $(window).innerHeight() - $('#header').outerHeight() - 24
    @$el.find('>div').css minHeight: lessons_box_height
    _.defer =>
      @lessons_scroll?.refresh()
      if scroll_to_top
        _.defer =>
          @lessons_scroll?.scrollTo 0, 0, 200
    @

  play_video: (e) ->
    e?.preventDefault()
    iframe_el = document.querySelector('iframe')
    (iframe_el.contentWindow or iframe_el.contentDocument).postMessage({type:'control',cmd: "play"}, '*')
    @

  initialize_video_messages: ->
    window.addEventListener 'message', (ev) =>
      data = ev.data
      type = data.type
      if type is 'play'
        @$el.find('iframe').css 'opacity': 1
        @$el.find('.video_overlay').hide()
    @

  add_kpk_course: ->
    course = new Stoege.Models.KpkCourse(course_id: @course_model.id)
    course.save {},
      # TODO success и error
      complete: (data) ->
        window.location.reload()

  prepare_to_auth: ->
    window.localStorage.setItem("is_from_registration", 1)
    path = if @is_kpk then "/courses/#{@course_model.id}" else "/courses/#{@course_model.id}/pay"
    $.cookie "after_sign_in_path", path, path: '/'
    true

  open_cart: (e) ->
    # TODO refactoring
    ###
      Довольно сложная магия получилась
      Если юзер не авторизован, то сохраняем его цель в куку и нативно отправляем по ссылке
      Если авторизован, то нативное действие привентим и отправляем его либо на добавление кпк, либо на оплату курса
    ###
    return @prepare_to_auth() unless user.is_authorized()

    if @is_kpk
      if user.is_teacher()
        @add_kpk_course()
      else
        window.notificationCollection.add
          content: "Этот курс предназначен только для учителей."
          status: 'warning'
    else
      Backbone.history.navigate("/courses/#{@course_model.id}/pay", true)
    false
