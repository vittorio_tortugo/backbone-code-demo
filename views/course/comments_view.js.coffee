class Stoege.Views.CommentsView extends Backbone.View
  template: JST["backbone/templates/course/comments"]
  template_rating: JST["backbone/templates/course/course_rating"]
  template_comment_item: JST["backbone/templates/course/comment_item"]
  template_my_comment_item: JST["backbone/templates/course/my_comment_item"]

  events:
    'click tfoot .more_comments': 'load_more_items'
    'click .edit' : 'edit_form'

  initialize: ({@el, @course_model, @refresh_callback}) ->
    @comments_info or= new Stoege.Models.CommentInfo(course_id: @course_model.id)
    @my_comment or= new Stoege.Models.MyComment(course: @course_model) if user.is_authorized()

    @comments_collection = @comments_collection or new Stoege.Collections.Comments
    @comments_collection.course_id = @course_model.id
    @comments_collection.state.pageSize = 10
    @

  render: ->
    @add_listeners()

    @comments_info.fetch
      success: (collection, response, _) =>
        @fetch_data()

        @$el.html @template(comments_info: @comments_info.toJSON())
        @comments_list = @$('.course_comments_list')
        @my_comment_view or= new Stoege.Views.MyComment(model: @my_comment, course: @course_model) if user.is_authorized()
        @refresh_callback()
        @

  render_my_comment: ->
    @comments_list.find('tbody tr').first().html @template_my_comment_item @my_comment.toJSON()
    @refresh_callback(false)
    @

  fetch_data: ->
    @my_comment?.fetch(url: "/api/courses/#{@course_model.id}/my_comment")

    @comments_collection.getFirstPage()

  add_listeners: ->
    @listenTo @my_comment, 'sync', @render_my_comment if @my_comment
    @listenTo @comments_collection, 'reset', @update_list

  edit_form: ->
    @comments_list.find('tbody tr').first().hide()
    @my_comment_view.render_form()

  update_list: ->
    items_els = []

    @comments_collection.each (model) => items_els.push @template_comment_item model.toJSON(), stats: @comments_collection.length
    @comments_list.find('tbody').append items_els.join ''
    @refresh_callback(false)

    @comments_list.children('tfoot').hide() if @comments_collection.length < 10
    @

  load_more_items: (e) ->
    e.preventDefault()
    @comments_collection.getNextPage()
    @

  remove: ->
    @my_comment_view.remove() if @my_comment_view?
    @stopListening()
    @
