class Stoege.Views.Course extends Backbone.View

  el: '.course_page'

  events:
    'mouseenter .recommendations ul > li': 'set_course_hover_color'
    'mouseleave .recommendations ul > li': 'set_course_default_color'

  initialize: ({@course_model}) ->
    @init_share_links()
    @

  init_share_links: ->
    share_url = location.href.split('/').splice(0, 5).join('/')
    course_full_title = @$('.social_buttons').data('course_full_title')
    @$('.share div').ShareLink
      url: share_url
      title: @$('.social_buttons').data('data-course_full_title')
      text:  "Рекомендую курс #{course_full_title} в Фоксфорде!"
      width: 640
      height: 480
    @$('.share .count').ShareCounter
      url: share_url
    @

  render: ->
    @program_view or= new Stoege.Views.CourseProgramLessonsView(course_model: @course_model)
    @lesson_view or= new Stoege.Views.CourseLessonView(course_model: @course_model)

    @program_view.render()
    @lesson_view.render()
    @

  set_course_hover_color: (e) ->
    $target = $(e.currentTarget).find('.header')
    dark_color = $target.data('hover_color')

    $target.css('background-color', "##{dark_color}")
    @

  set_course_default_color: (e) ->
    $target = $(e.currentTarget).find('.header')
    color = $target.data('color')

    $target.css('background-color', "##{color}")
    @