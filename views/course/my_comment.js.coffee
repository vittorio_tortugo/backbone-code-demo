class Stoege.Views.MyComment extends Stoege.Views.Application

  el: '.add_comment_wrapper'

  events:
    'click .send_button:not(.disabled)' : 'save'
    'click .send_comment .fxf_rating .star' : 'update_points'
    'keyup .textarea_box' : 'update_text'

  form_template: JST["backbone/templates/course/comments_form"]

  initialize: ({@el, @course}) ->
    @text = 'курс'
    @listenTo(@model, 'sync', @render, @)
    @listenTo(@model, 'error', @render_form, @)
    @listenTo(@model, 'change:points', @rerender_form, @)
    # на следующий спиринт
    #@listenTo(@model, 'change:text', @rerender_form, @)

  render: ->
    # Если модель не сохранена - рендерим форму
    if not @model.get('id') and @course.get('access')
      @render_form()
    else
      @hide_form()
    @

  save: ->
    if @model.get('text') and @model.get('points') > 0
      @model.save null,
        error: =>
          _.defer => @show_error('Доступно только для<br />записавшихся на курс')
    else
      @show_error("#{you_text('Оставьте', 'Оставь')} отзыв и #{you_text('оцените', 'оцени')} #{@text}")

    $('.course_comments_list tbody tr').first().show()

  render_form: ->
    @$el.html(@form_template(comment: @model.toJSON()))
    @

  hide_form: ->
    @$el.empty()
    @

  rerender_form: ->
    @render_form()
    @move_cursor_to_input_end(@$('textarea')[0])

  update_points: (e) ->
    @model.set(points: $(e.target).data('point'))

  update_text: (e) ->
    @model.set(text: $(e.target).val())

  move_cursor_to_input_end: (element)->
    if element
      $element = $(element)
      scroll_height = element.scrollHeight
      originalValue = $element.val()
      $element.blur().val('').focus().val(originalValue)

      $element.animate
        scrollTop: scroll_height
        , 0

  show_error: (text) =>
    super(@$('.send_button'), @$('.rating_box'), text)
