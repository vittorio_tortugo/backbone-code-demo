class Stoege.Views.AutoPurchase extends Backbone.View

  el: '.auto_transfer'

  events:
    'click .pseudo' : 'toggle'

  template: JST["backbone/templates/course/auto_purchase"]

  initialize: ->
    @listenTo(@model, 'sync', @render, @)

  toggle: ->
    @model.toggle()

  render: ->
    @$el.html(@template(users_course: @model.toJSON()))
    @
