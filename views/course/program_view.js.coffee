class Stoege.Views.CourseProgramView extends Backbone.View

  template: JST["backbone/templates/course/program"]

  events:
    'click a.disabled': 'block_disabled_links'

  initialize: ({@el, @course_model, @refresh_callback}) ->
    $(window).resize => _.debounce(@resize_video(), 250)
    @

  render: (lessons_scroll) ->
    @lessons_scroll = lessons_scroll
    @initialize_model()
    @

  initialize_model: ->
    current_model = @course_model.lessons_collection.findWhere(active: true)
    if current_model.loaded
      @model = current_model
      @render_content()
    else
      @model = current_model
      @listenTo @model, 'sync', @render_content
      @model.fetch()
    @

  render_content: ->
    @model.loaded = true

    @$el.html @template @model.toJSON()
    @resize_video()
    @refresh_callback()
    @initialize_pin_header()
    @change_title()
    @

  resize_video: ->
    if @model.get 'video_url'
      $iframe = @$el.find('iframe')
      block_width = @$el.width()
      scale_factor = 2.029
      h = Math.round(block_width / scale_factor)
      if $iframe
        $iframe.css width: '100%', height: h
        @$el.find('.video_overlay').css marginTop: h * -1

      $('.block_video').css height: h
    @

  initialize_pin_header: ->
    @lessons_scroll.refresh()
    $banner = $('.lesson_banner')

    @lessons_scroll.on 'scroll', =>
      if @model.get('access_state') isnt 'available' and @model.get('number') isnt 0
        if @lessons_scroll.y < -65
          $banner.addClass('pinned')
                 .css 'top', Math.abs(@lessons_scroll.y + 12)
        else
          $banner.removeClass('pinned')
                 .css 'top', 0

  block_disabled_links: (e) ->
    e.preventDefault()
    @

  remove: ->
    @stopListening()
    @

  change_title: ->
    document.title = @model.get('lesson_page_title')
    @
