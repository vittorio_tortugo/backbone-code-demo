class Stoege.Views.CourseLessonTopMenuView extends Backbone.View

  el: '.lesson_menu_top'
  template_item: JST['backbone/templates/course/components/top_menu_item']

  events:
    'click a': 'menu_item_select'

  initialize: ({@course_model}) ->
    @listenTo(@course_model.top_menu_collection, 'change:active', @set_active_item)
    @$item_wrapper = @$el.find('.lesson_menu_items')
    @

  render: ->
    lesson_id = @course_model.lessons_collection.findWhere(active: true)?.get 'id'
    course_id = @course_model.get 'id'
    top_menu_items = []

    @course_model.top_menu_collection.each (model) =>
      item_id = model.get 'id'
      return if item_id is 'rating' and not gon.show_rating

      if item_id is 'program'
        link = "/courses/#{course_id}/#{item_id}/#{lesson_id}"
      else
        link = "/courses/#{course_id}/#{item_id}"
      model_attributes =
        link: link
        active: false
      model_options =
        silent: true
      model.set model_attributes, model_options

      top_menu_items.push @template_item model.toJSON()

    @$item_wrapper.html top_menu_items.join ''
    @

  set_active_item: (model) ->
    @$item_wrapper.find('a.active').removeClass 'active'
    @$item_wrapper.find("a[data-id=#{model.get 'id'}]").addClass 'active'
    @animate_border()

  animate_border: ->
    active_item = @$item_wrapper.find('a.active').children().eq(0)
    left = active_item.position().left + parseInt active_item.css('marginLeft')

    @$('.tab_menu_roller').css
      left: left
      width: active_item.width()
    @

  menu_item_select: (e)->
    e.preventDefault()
    Backbone.history.navigate($(e.currentTarget).attr('href'), true)
    @