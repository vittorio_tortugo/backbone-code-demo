class Stoege.Views.CourseAboutView extends Backbone.View

  template: JST["backbone/templates/course/about"]

  events:
    'click .teacher_video' : 'open_teacher_video'
    'click .course_video' : 'open_course_video'

  initialize: ({@el, @course_model, @refresh_callback}) ->
    @

  render: ->
    @init_model()
    @

  init_model: ->
    if !@model
      @model = new Stoege.Models.CourseAboutModel(id: @course_model.get 'id')
      @listenTo @model, 'sync', @render_content
      @model.fetch()
    else
      @render_content()
    @

  render_content: ->
    @$el.html @template @model.toJSON()
    @refresh_callback()
    @course_model.lessons_collection.first().fetch
      silent: true
      success: (model) =>
        @$('.course_video').toggleClass 'hidden', !model.get('video_url')
    @$el.find('.dotdotdot').dotdotdot(watch: 'window')
    @

  open_teacher_video: (e) ->
    e.preventDefault()
    @teacherVideoView = new Stoege.Views.TeacherVideoDialog(model: new Backbone.Model(video_link: $(e.currentTarget).attr 'href'))
    @teacherVideoView.open()
    @

  open_course_video: (e) ->
    e.preventDefault()
    localStorage.setItem('lesson_video_options', 'autoplay')
    _.defer =>
      $(".lesson_box .lesson[data-lesson_id=#{@course_model.lessons_collection.first().id}]").click()
    @

  remove: ->
    @stopListening()
    @