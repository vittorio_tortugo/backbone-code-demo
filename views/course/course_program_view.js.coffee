class Stoege.Views.CourseProgramLessonsView extends Backbone.View

  el: '.lesson_box_wrapper'

  events:
    'click .lesson': 'select_lesson'

  initialize: ({@course_model}) ->
    @listenTo(@course_model.lessons_collection, 'change:active', @update_selected_item)
    if @$el[0]
      $('.wrapper_add').css paddingBottom: 0
      unless @lessons_scroll
        @lessons_scroll = new IScroll '.lesson_box_wrapper',
          click: true
          scrollbars: true
          mouseWheel: true
          fadeScrollbars: true
          disableMouse: false
          interactiveScrollbars: true
          HWCompositing: false
          useTransition: false

      @set_elements_height()

      $(window).resize => _.debounce @set_elements_height(), 300
      @update_progress()
    @

  update_progress: ->
    $progress_elements = $('.fxf_progress.fxf_progress_circle')
    progress_view = new Stoege.Views.Progress
    progress_view.draw_circle_progress $(progress) for progress in $progress_elements
    @

  render: ->
    current_model = @course_model.lessons_collection.findWhere(active: true)
    @update_selected_item current_model
    @lessons_scroll.scrollToElement @$el.find(".lesson[data-lesson_id=#{current_model.id}]")[0], 500, 0, 0
    @

  select_lesson: (e) ->
    e.preventDefault()
    @course_model.lessons_collection.set_active_model $(e.currentTarget).data('lesson_id')
    Backbone.history.navigate($(e.currentTarget).attr('href'), true)
    @

  update_selected_item: (model) ->
    @$el.find('.lesson').removeClass 'active'
    $active_element = @$el.find(".lesson[data-lesson_id=#{model.get 'id'}]")
    if $active_element
      $active_element.addClass 'active'
    @

  set_width_course_info: ->
    $('.full_course_info .info').css('width', 'auto') unless $('.full_course_info .info .next_lesson').length?

  set_course_min_height: ->
    $('.course_content').css('min-height', $('.sidebar_content').height() - 100)

  show_next_lesson: (e) ->
    next_lesson_id = $(e.target).closest('.next_lesson').data('lesson_id')

    @lessons.where(id: next_lesson_id)[0].set(open: true)
    $(window).scrollTo($(".lessons > li[data-lesson_id=#{next_lesson_id}]"), 200)

  scroll_to_comments: ->
    $(window).scrollTo($(".comments_wrapper"), 200)

  show_lesson_info: (e) ->
    $lesson_id = $(e.target).closest('li').data('lesson_id')
    lesson_model = @lessons.where(id: $lesson_id)[0]

    if lesson_model.get('open')
      lesson_model.set(open: false)
    else
      lesson_model.set(open: true)

  toggle_lesson_visibility: (lesson) ->
    lesson_id = lesson.get('id')
    $lesson = $(".lessons > li[data-lesson_id=#{lesson_id}]")

    if lesson.get('open')
      $lesson.addClass('open')
      .find('.lesson_content').slideDown('fast')
    else
      $lesson.removeClass('open')
      .find('.lesson_content').slideUp('fast')

    @check_lessons_button_state()

  toggle_all_lesson_visibility: (e) ->
    $target = $(e.target).closest('.toggle_lessons_visable')

    if $target.hasClass('show_all')
      $target.find('.text').html('Свернуть все')
      $target.removeClass('show_all')
      _.each @lessons.where(open: false), (lesson) =>
        lesson.set(open: true)
    else
      $target.find('.text').html('Развернуть все')
      $target.addClass('show_all')

      _.each @lessons.where(open: true), (lesson) =>
        lesson.set(open: false)

  check_lessons_button_state: ->
    $all_lessons_button = $('.toggle_lessons_visable')

    if @lessons.where(open: true).length
      $all_lessons_button.find('.text').html('Свернуть все')
      $all_lessons_button.removeClass('show_all')
    else
      $all_lessons_button.find('.text').html('Развернуть все')
      $all_lessons_button.addClass('show_all')

  open_cart: (e) ->
    Backbone.history.navigate("/courses/#{@course_id}/pay", true)

  set_elements_height: ->
    lessons_box_height = $(window).innerHeight() - $('#header').outerHeight() - 160
    @$el.css minHeight: lessons_box_height
    _.defer =>
      @lessons_scroll?.refresh()
      #хак, чтобы бордер не обрезался.
      @$el.css 'padding-bottom': 5
    @
