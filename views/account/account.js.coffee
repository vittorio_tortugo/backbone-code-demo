#= require ../application.js.coffee
class Stoege.Views.Account extends Stoege.Views.Application

  el: '.account_content'

  events:
    'click input.error, label.error' : 'remove_error'
    'click .bin_button' : 'remove_avatar'
    'click .change_email' : 'unlock_email'
    'click a.delete_document': 'remove_document'
    'click .tab_menu_item' : 'show_tab'
    'click .email [type=submit]' : 'show_submit_spinner'
    'click .datepicker_trigger': 'show_datepicker'
    'mouseenter .social_button.linked' : 'hover_on_social_button'
    'mouseleave .social_button.linked' : 'hover_out_social_button'
    'mouseenter .account_document_pack .blue_border_button' : 'show_description'
    'mouseleave .account_document_pack .blue_border_button' : 'remove_description'

  initialize: ({@phone_view}) ->
    @init_custom_select()
    @bind_form()
    new Stoege.Views.AutocompleteSchool(el: @el)
    new Stoege.Views.AutocompleteHome(el: @el)
    @$('.datepicker').datepicker
      dateFormat: 'dd.mm.yy'
      nextText: '⟩'
      prevText: '⟨'
    @upload_document()
    @upload_avatar()
    @animate_border()

    $("#new_document").change (e) =>
      @upload_document()

  show_submit_spinner: (event) ->
    _.defer ->
      $(event.target).addClass 'waiting'
      $('.blue_button').addClass('disabled').prop('disabled', true)
    true

  hide_submit_spinner: ->
    $('.waiting').removeClass 'waiting'
    $('.blue_button.disabled').removeClass('disabled').prop('disabled', false)
    true

  show_tab: (e) ->
    tab_id = $(e.target).data('tab-id')
    if tab_id?
      @$('.active').removeClass('active')
      @$("form [data-tab-id=#{tab_id}]").addClass('active')
      $(e.target).addClass('active')
      @animate_border()
      false

  show_datepicker: ->
    @$('.datepicker').datepicker 'show'
    false

  animate_border: ->
    #TODO в принципе можно сделать табы как отдельный компонент, чтобы не дублировать код на странице курса и здесь
    #app/assets/javascripts/backbone/views/course/lesson_top_menu.js.coffee
    active_item = @$('.tab_menu_items .active')
    left = active_item.position().left

    @$('.tab_menu_roller').css
      left: left
      width: active_item.width()
    @

  upload_avatar: ->
    @$('#avatar').fileupload
      type: 'POST'
      url: '/api/user/avatar'
      formData:
        _method: 'POST'

      beforeSend: =>
        @$('.account_avatar_form .blue_border_button').addClass('waiting disabled')

      done: (e, data) =>
        @$('.account_avatar').html("<img src=#{data.result.avatar}>")

        # TODO событие avatar_uploaded и подписка на него в шапке вместо прямого вкорячивания нового аватара
        $('.header_user_wrapper .avatar').html("<img src=#{data.result.avatar}>")
        @$('.account_avatar_form .blue_border_button').removeClass('waiting disabled')
        window.notificationCollection.add
          content: 'Аватар успешно изменен'
          status: 'notice'
        @$('.account_avatar_file').append('<div class="bin_button"></div>') unless @$('.account_avatar_file .bin_button').length

      error: =>
        @$('.account_avatar_form .blue_border_button').removeClass('waiting disabled')
        $button = $('.account_avatar_file .blue_border_button')
        @show_error($button, $button.parent(), "#{you_text('Укажите', 'Укажи')} другой файл с фотографией")

  upload_document: ->
    # TODO отрефакторить эту хрень!!
    @$('.user_document_file').fileupload
      dataType: 'json'
      type: 'POST'
      url: '/api/user/documents'

      send: (e, data) =>
        $(e.target).closest('.blue_border_button').addClass('waiting disabled')

      submit: (e, data) =>
        data.formData = document_type_id: $(e.target).data('document-type-id')

      done: (e, data) =>
        @$('.account_document_pack .blue_border_button').removeClass('waiting disabled')
        $(e.target).closest('.account_row').find('.document').html "<span data-document-id=#{data.result.document_id}><a class='delete delete_document'></a></span><span>#{data.result.document_type_name}</span>"
        $(e.target).closest('.account_row').find('.blue_border_button').addClass('hidden')
        window.notificationCollection.add
          content: 'Документ успешно загружен'
          status: 'notice'

      fail: (e, data) =>
        errors_str = ''
        $input = @$(e.target).closest('.blue_border_button')
        $input.removeClass('waiting disabled')
        switch data.jqXHR.status
          when 413
            errors_str = 'Слишком большой размер загружаемого файла'
          else
            errors = []
            errors.push value for key, value of data.jqXHR.responseJSON.errors
            errors_str = errors[0].join ', '
        @show_error($input, $input.parent(), errors_str)

  remove_document: (e) ->
    e.preventDefault()
    return unless confirm("#{you_text('Вы уверены', 'Ты уверен')}, что #{you_text('хотите', 'хочешь')} удалить документ?")
    $current_target = $(e.currentTarget)
    document_id = $current_target.parent().data 'document-id'
    $.ajax
      dataType: 'json'
      type: 'DELETE'
      url: "/api/user/documents/#{document_id}"
      success: (data) =>
        $row = $current_target.closest('.account_row')
        $row.find('.blue_border_button').removeClass('waiting disabled hidden')
        $row.find('.user_document_file').removeAttr('disabled')
        $row.find('input:file').prop('disabled', false)
        $current_target.closest('.document').html(
          if data.document_type.required is true then "#{data.document_type.name} - не загружено<br>(обязательно)" else "#{data.document_type.name} - не загружено")
        window.notificationCollection.add
          content: 'Документ успешно удален'
          status: 'notice'
    @

  remove_avatar: ->
    return unless confirm("#{you_text('Вы уверены', 'Ты уверен')}, что #{you_text('хотите', 'хочешь')} удалить фотографию?")
    $.ajax
      dataType: 'json'
      type: 'DELETE'
      url: '/api/user/avatar'
      success: (data) =>
        @$('.account_avatar').html("<img src=#{data.avatar}>")
        $('.header_user_wrapper .avatar').html("<img src=#{data.avatar}>")
        @$('.account_avatar_file .bin_button').remove()
        window.notificationCollection.add
          content: 'Аватар успешно удален'
          status: 'notice'

  # 1) two emails - 1st - confirmed, 2nd - unconfirmed
  # 2) one unconfirmed email
  # 3) one email, confirmed
  lock_email: (data) ->
    @$('.confirm_email').addClass('hidden_block')
    @$('.change_email').removeClass('hidden_block')

    if data.unconfirmed_email
      email_class = 'unconfirmed'
      @$('.account_row.confirmation_message').html('Ожидает подтверждения')
      @$('.account_row.email').find('.hint').html("#{you_text('Ваш', 'Твой')} текущий email: #{data.email}")
    else if data.first_email_unconfirmed
      email_class = 'unconfirmed'
    else
      email_class = 'confirmed'

    @$('#user_pupil_email').addClass("disabled #{email_class}").attr('disabled': true)

  unlock_email: ->
    @$('.change_email').addClass('hidden_block')
    @$('.confirm_email').removeClass('hidden_block')
    @$('#user_pupil_email').removeClass('disabled confirmed unconfirmed')
      .attr('disabled': false, 'data-disabled': false)

  message_on_success: (data) =>
    if data.unconfirmed_email or data.first_email_unconfirmed
      content: "У #{you_text('Вас', 'тебя')} сменился email? #{you_text('Пройдите', 'Пройди')} по ссылке в письме, которое мы #{you_text('Вам', 'тебе')} отправили на новый адрес."
      status: 'warning'
    else
      content: "Данные профиля успешно сохранены. #{you_text('Можете', 'Можешь')} продолжать обучение!"
      status: 'notice'

  init_custom_select: ->
    @$('#user_pupil_grade_id').customSelect(customClass: "custom_select")
    @$('#school_country_id').customSelect(customClass: "custom_select")
    @$('#address_country_id').customSelect(customClass: "custom_select")

  bind_form: ->
    $('.edit_user_profile')
      .on 'ajax:success', (evt, data, status, xhr) =>
        $.cookie 'grade_id', @$("#user_pupil_grade_id").val(), path: '/', expires: unless user.is_authorized() then 60
        @hide_submit_spinner()
        @lock_email(data)
        # меняем имя в хедере
        $('#header .user_name').html(data.short_name)
        @clear_passwords_fields()

        window.notificationCollection.add @message_on_success(data)

      .on 'ajax:error', (evt, xhr, status, error) =>
        @hide_submit_spinner()
        window.notificationCollection.add
          content: 'Не удалось сохранить изменения'
          status: 'alert'

        json = $.parseJSON(xhr.responseText)
        $.each json, (key, value) =>
          if key.indexOf('school') isnt -1
            key = key.replace(/\./g, "_attributes_")

          $input = @$('#user_pupil_' + key)
          @show_error($input, $input.parent(), value[0])

  clear_passwords_fields: ->
    @$('[type="password"]').val('')

  hover_on_social_button: (e) ->
    $target = $(e.target)
    $current_element = if $target.hasClass('.linked') then $target else $target.closest('.linked')
    $current_element.find('.unlink_title').removeClass('hidden_block')
    $current_element.find('.mark').addClass('hidden_block')

  hover_out_social_button: (e) ->
    $target = $(e.target)
    $current_element = if $target.hasClass('.linked') then $target else $target.closest('.linked')
    $current_element.find('.unlink_title').addClass('hidden_block')
    $current_element.find('.mark').removeClass('hidden_block')

  show_description: (e) ->
    $input = $(e.target)
    text_note = $input.data('description')
    @remove_error(e)
    @show_tooltip($input, $input.parent(), text_note) if text_note

  remove_description: (e) ->
    @remove_tooltip(e)
