class Stoege.Views.Account.PhoneView extends Stoege.Views.Application
  el: '.account_phone'

  events:
    'click .state_phone .blue_button': 'save_or_delete_phone_number'
    'click .state_confirm .blue_button': 'confirm_activation_code'
    'keydown .fields.state_confirm input[name=user_phone_confirm]': 'handle_code_input_keydown'

  initialize: ({@phone_model, @phone_confirmation_model}) ->
    @confirm_countdown_timer = null
    @confirmed_phone_number = ''
    @$confirm_block = @$('.fields.state_confirm')
    @$confirm_block_hint = @$('.fields.state_confirm .hint')
    @$phone_input = @$('.fields.state_phone input[name=user_phone]')
    @$phone_code_input = @$('.fields.state_confirm input[name=user_phone_confirm]')
    @$phone_button = @$('.fields.state_phone .blue_button')
    @$phone_code_button = @$('.fields.state_confirm .blue_button')
    @$phone_input.mask "+7 (999) 999 99 99", autoclear: false
    @$phone_code_input.mask "9999", autoclear: false
    @listenTo @phone_confirmation_model, 'change:state', @update_view_state, @
    @phone_confirmation_model.fetch()
    @observe_phone_changes true
    @

  save_or_delete_phone_number: (e) ->
    e.preventDefault()
    return if $(e.target).hasClass 'disabled'
    if @phone_confirmation_model.get 'phone_confirmed'
      @phone_confirmation_model.set
        state: 'not_confirmed'
        phone_confirmed: false
    else
      clean_phone_number = @phone_model.clear_phone @$phone_input.val()
      is_same_number = clean_phone_number is @confirmed_phone_number
      if is_same_number and clean_phone_number.length
          @phone_confirmation_model.set
            state: 'confirmed'
            phone_confirmed: true
      @phone_model.set 'phone', clean_phone_number
      if !@phone_model.get('phone').length
        @phone_model.destroy
          success: =>
            @phone_model = new Stoege.Models.AccountPhoneModel
            @phone_confirmation_model.fetch()
      else
        unless is_same_number
          if @phone_model.isValid()
            @phone_confirmation_model.set
              state: 'wait_confirmation'
            @phone_model.save {},
              patch: false
              wait: true
              success: =>
                @update_confirmation_countdown true
              error: (error, response) =>
                @phone_confirmation_model.set
                  state: 'not_confirmed'
          else
            @show_error @$phone_input, @$phone_input.parent(), @phone_model.validationError
    @

  confirm_activation_code: (e) ->
    e.preventDefault()
    return if $(e.target).hasClass 'disabled'
    @phone_confirmation_model.set 'code', @$phone_code_input.val()
    @phone_confirmation_model.save {},
      wait: true
      success: =>
        @update_confirmation_countdown false
        @phone_confirmation_model.set
          state: 'confirmed'
      error: (error, response) =>
        @show_error @$phone_code_input, @$phone_code_input.parent(), response.responseJSON.errors.code.join(', ')
    @

  update_view_state: (model) ->
    switch model.get 'state'
      when 'confirmed'
        @$confirm_block.addClass 'hidden_block'
        @$phone_input.addClass('disabled confirmed').attr 'disabled', 'disabled'
        @$phone_button.removeClass('disabled')
        @$phone_button.val 'Сменить номер'
        @confirmed_phone_number = @phone_model.clear_phone @$phone_input.val()
      when 'not_confirmed'
        @$confirm_block.addClass 'hidden_block'
        @$phone_input.removeClass('disabled unconfirmed confirmed').attr 'disabled', null
        @$phone_button.removeClass('disabled')
        @$phone_button.val 'Сохранить'
      when 'not_confirmed_again'
        @$confirm_block_hint.addClass 'hidden_block'
        @$phone_input.removeClass('disabled unconfirmed confirmed').attr 'disabled', null
        @$phone_button.removeClass('disabled')
        @$phone_button.val 'Отправить повторно'
      when 'wait_confirmation'
        @$phone_input.addClass('disabled unconfirmed').attr 'disabled', 'disabled'
        @$phone_button.addClass('disabled')
        @$confirm_block.removeClass 'hidden_block'
        @$confirm_block_hint.removeClass 'hidden_block'
        @$phone_code_input.val('')
        @$phone_code_input.removeClass('disabled').attr 'disabled', null
        @$phone_code_button.removeClass('disabled')
        @$('.fields.state_confirm .hint').text 'Код подтверждения будет выслан в SMS'
        @$phone_button.val 'Отправить повторно'
        @update_confirmation_countdown true
    @

  update_confirmation_countdown: (start = false) ->
    clearInterval @confirm_countdown_timer if @confirm_countdown_timer
    if start
      timer = 91
      @confirm_countdown_timer = setInterval =>
        timer--
        @$('.fields.state_confirm .hint').text "Повторная отправка кода возможна через #{timer} c."
        if timer <= 0
          @phone_model.set 'phone', ''
          @update_confirmation_countdown()
          @phone_confirmation_model.set
            state: 'not_confirmed_again'
      , 1000
    @

  observe_phone_changes: (start = false) ->
    clearInterval @observe_phone_timer if @observe_phone_timer
    if start
      @observe_phone_timer = setInterval =>
        old_value = @$phone_input.data 'old_value'
        new_value = @$phone_input.val()
        if old_value isnt new_value
          @$phone_input.data 'old_value', new_value
          @reset_confirmation_process() if old_value?
      , 100
    @

  reset_confirmation_process: ->
    @update_confirmation_countdown false
    @phone_confirmation_model.set
      state: 'not_confirmed'

  handle_code_input_keydown: (e) ->
    keycode = (if e.keyCode then e.keyCode else e.which)
    if keycode is 13
      e.preventDefault()
      @$phone_code_button.click()
