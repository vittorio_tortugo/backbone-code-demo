class Stoege.Views.AlertView extends Backbone.View

  template: JST["backbone/templates/alert"]

  el: 'body'

  events:
    'mouseenter .close_button' : 'mouseenter'
    'mouseleave .close_button' : 'mouseleave'
    'mousedown .close_button' : 'mousedown'
    'click .close_button' : 'close'

  initialize: (options) =>
    @open(options.message)

  open: (message) =>
    bottom_value = 58
    if $('.alert_message').length
      $last_alert = $('.alert_message:last')

      item_value = parseInt($last_alert.attr('id').substring(6))

      # если сообщение совпадает с сообщением предыдущего алерта и до этого было уже 3 алерта, то не показываю его
      return if ($('.alert_message').length >= 3) and ($('.alert_message:last').find('.alert_content').text() is message)

      item_value += 1
      # присваиваю номер по порядку
      alert_element = '<div class="alert_message" id="alert_'+ item_value + '" style="bottom:' + bottom_value + 'px ">' + @template(data: message) + '</div>'
      @alert_append(alert_element, item_value)
    else
      alert_element = '<div class="alert_message" id="alert_1" >' + @template(data: message) + '</div>'
      @alert_append(alert_element, 1)

  close: (e) =>
    $alert_element = $(e.target).parent()
    @alert_animation_in($alert_element)

  alert_append: (alert_element, item) ->

    # вставляю и анимирую алерт
    $('body').append(alert_element)
    $alert_element = $('#alert_' + item)

    alert_count = ($('.alert_message').length) - 1
    # анимирую алерты
    if alert_count is 0
      @alert_animation_out(item)
    else
      setTimeout ( () =>
        @alert_animation_out(item)
      ), 300 * alert_count

    # закрываю алерт через 10 секунд, если он еще существует
    setTimeout ( () =>
      if $alert_element.length
        @alert_animation_in($alert_element)
    ), 10000

  alert_animation_out: (item) ->
    $alert_element = $('#alert_' + item)
    alert_height = $alert_element.height()
    alert_width = $alert_element.width()

    $alert_element.height('0')
    $alert_element.width('0')

    if item > 1
      for prev_item in [(item - 1)..1]
        # пересчитываю bottom предыдущих алертов относительно нового
        $prev_alert_element = $('#alert_' + prev_item)

        if $prev_alert_element.length
          prev_alert_height = $prev_alert_element.height()
          bottom_value = Math.floor($(window).height() - $prev_alert_element.position().top - prev_alert_height + alert_height + 10)
          $prev_alert_element.animate
            bottom: bottom_value

    $alert_element.show()

    $alert_element.animate
      height: alert_height
      width: alert_width
      paddingTop: '10px'
      paddingRight: '46px'
      paddingBottom: '10px'
      paddingLeft: '10px'
      300

  alert_animation_in: ($alert_element) ->

    $alert_element.animate
      height: '0px'
      width: '0px'
      paddingTop: '0px'
      paddingRight: '0px'
      paddingBottom: '0px'
      paddingLeft: '0px'
      300
      -> $alert_element.remove()

  mouseenter:(e) =>
    $(e.currentTarget).addClass('hover')

  mouseleave:(e) =>
    $(e.currentTarget).removeClass('hover')
    $(e.currentTarget).removeClass('active')

  mousedown: (e) =>
    $(e.currentTarget).removeClass('hover')
    $(e.currentTarget).addClass('active')
