class Stoege.Views.RatingList extends Backbone.View

  el: '.container_rating'

  events:
    'click .fxf_rating_list tfoot a' : 'load_more_ratings'

  not_found_template: JST["backbone/templates/not_found"]
  more_ratings_link_template: JST["backbone/templates/more_ratings_link"]
  template_rating_list_item: JST["backbone/templates/rating_list_item"]

  initialize: ({@grades, @user_ratings, @collection}) ->
    @list_element = @$('.fxf_rating_list')
    @list_tbody_element = @list_element.children('tbody')
    @list_thead_element = @list_element.children('thead')
    @list_tfoot_element = @list_element.children('tfoot')
    @listenTo(@collection, 'reset', @render, @)
    @listenTo(@collection, 'error', @redirect, @)

    $(window).scroll =>
      @load_more_records()
    @

  load_more_records: ->
    return if !@collection.hasNextPage() or !@collection.length or @collection.length < 10
    return unless @list_tbody_element.children().last().length
    if @list_tbody_element.children().last().offset().top < ($(window).scrollTop() + $(window).height())
      @load_more_ratings()
    @

  load_rating: (params) ->
    @params = _.clone(params)
    grade_id = @grades.findWhere(selected: true)?.get('id')
    @params.grade_id = grade_id if grade_id and grade_id isnt '0'
    delete @params.grade_id if !grade_id or @params.grade_id is '0'

    @collection.state.currentPage = 1

    @list_element.show()
    @list_tbody_element.empty()

    @render_user_rating()
    @collection.fetch(data: @params)
    @

  redirect: ->
    window.location = '/404'

  render_user_rating: ->
    return unless user.is_authorized()

    user_rating = @user_ratings.where(discipline_id: parseInt(@params.discipline_id) or null, grade_id: parseInt(@params.grade_id) or null)[0]
    if user_rating? and user_rating.get('position') > 10
      @add_one(user_rating)
    @

  render: ->
    if @collection.length < 3 and @collection.state.currentPage is 1
      @list_element.hide()
      @list_tbody_element.html(@not_found_template(text: 'рейтинга учеников'))
    else
      @add_many()

    @list_tfoot_element.toggleClass 'hidden', @collection.length < 10
    @list_thead_element.toggleClass 'hidden', !@list_tbody_element.text().length
    @

  add_one: (model) ->
    @list_tbody_element.append @template_rating_list_item model.toJSON()
    @

  add_many:  ->
    html = []
    @collection.each (model) => html.push @template_rating_list_item model.toJSON()
    @list_tbody_element.append html.join('')
    @

  load_more_ratings: (e) ->
    e?.preventDefault()
    @collection.getNextPage
      data: @params
      beforeSend: =>
        @list_tfoot_element.show()
      complete: =>
        @list_tfoot_element.hide()
    @

  remove_view: ->
    @undelegateEvents()
    @stopListening(@collection, 'reset')
    @stopListening(@collection, 'error')
    $(window).off('scroll')
