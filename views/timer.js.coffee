class Stoege.Views.Timer extends Backbone.View

  el: '.timer'

  timer_template: JST["backbone/templates/timer"]

  initialize: (options) ->
    finished_date = parseInt @$el.data('value')
    @finished_date = new Date(finished_date)
    @render()
    @update_timer = (setInterval @render, 1000) unless @update_timer

  render: =>
    time = @get_time()
    @$el.html @timer_template(time: time)
    @

  get_time: ->
    remaining_time = (@finished_date - new Date()) / 1000
    time =
      days: @add_zero(remaining_time / 86400)
      hours: @add_zero((remaining_time / 3600) % 24)
      minutes: @add_zero((remaining_time / 60) % 60)
      seconds: @add_zero(remaining_time % 60)

  add_zero: (number) ->
    string_number = Math.floor(number).toString()
    if string_number.length < 2
      "0#{string_number}"
    else
     string_number
