class Stoege.Views.FeedbackDialogView extends Stoege.Views.Dialog

  template:       JST["backbone/templates/dialogs/feedback/dialog"]
  template_file:  JST["backbone/templates/dialogs/feedback/components/file"]

  className: 'feedback_dialog'

  events: ->
    _.extend {}, super,
      'click input.error, select.error, textarea.error' : 'remove_error'
      'select2-focus .select.error' : 'remove_error'
      'click input[type="file"]' : 'remove_file_error'

  initialize: ->
    @model = new Stoege.Models.DialogModel
    @model.url = '/feedbacks/new'

  open_feedback: ->
    @model.fetch
      success: =>
        @render()
        @open(800)
        @initialize_feedback_view()

  initialize_feedback_view: ->
    @prepare()

  destroy: ->
    @$el.dialog('destroy')
    @.remove()

    if location.pathname.indexOf('feedback') != -1
      Backbone.history.navigate('/', false)
    else
      @delete_dialog_param('feedback')

  prepare: ->
    @is_ie = (navigator.userAgent.indexOf('MSIE') isnt -1)
    @ie_version = parseInt(navigator.userAgent.substr(navigator.userAgent.indexOf('MSIE') + 5, 3 )) || 0
    @fileuploaded = false

    @upload_form()
    @upload_form_with_file()
    @init_custom_select()

  upload_form: ->
    $('#new_feedback').submit (e) =>
      e.preventDefault()
      # если не было прикрепления файла, применяем классический сабмит
      return if @fileuploaded
      $.ajax
        type: 'POST'
        url: '/feedbacks'
        data: $('#new_feedback').serialize()
        dataType: "JSON"
        beforeSend: =>
          @show_submit_spinner()
        success: =>
          @show_success_result()
        error: (xhr, status, error) =>
          response = xhr.responseText
          @show_error_result(response)
      return false

  upload_form_with_file: ->
    _this = @

    $('#new_feedback').fileupload
#for correction error for rails 3.2.13
      paramName: 'feedback[feedback_attachments_attributes][][file]'
      singleFileUploads: false
      dropZone: @$('.feedback_file')
      pasteZone: @$('.feedback_file')

# обрабатываю объект iframe после отправки файлов
      done: (e, data) =>
        if @is_ie and @ie_version <= 9
          response = data.result[0].body.innerHTML
          if response is 'success'
            @show_success_result()
          else
            @show_error_result(response)

      add: (e, data) ->
# если файл прикреплен применяем сабмит fileupload
        _this.fileuploaded = true

        if !_this.if_ie or ( _this.if_ie  and _this.ie_version > 9 )
# @files - файлы, которые уже прикреплены
          if @files
            data.files = data.files.concat(@files)

          # если количество прикрепленных файлов больше 5, дизейблим кнопку и удаляем лишние файлы
          if (data.files.length >= 5)
            $('.fields input').attr('disabled', 'disabled')
            $('.feedback_file_text').removeClass('hover').addClass('disabled')
            to_remove = data.files.length - 5
            data.files.splice(5, to_remove)

          # показываю и индексирую файлы
          data.files = _this.render_file_list(data.files)
          @files = data.files

          # удаление файла

          # отвязываю удаление от предыдущих файлов
          _this.$el.off 'click', '.feedback_file_remove'

          # и привязываю к новым
          _this.$el.on 'click', '.feedback_file_remove', ->
            file_index = $(@).data('index')
            data.files.splice(file_index, 1)
            _this.remove_file_error()

            # если количество прикрепленных файлов стало меньше 5, раздизейбливаем кнопку
            if (data.files.length <= 5)
              $('.fields input').removeAttr('disabled')
              $('.feedback_file_text').removeClass('disabled')

            # показ списка файлов
            data.files = _this.render_file_list(data.files)

            # если прикрепленных файлов нет применяем классический сабмит
            if (data.files.length is 0)
              _this.fileuploaded = false
              _this.$el.find('.button_row .blue_button').off 'click'

# присоединение и удаление одного файла для ie <= 9
        else
          $('.fields input').attr('disabled', 'disabled')
          $('.feedback_file_text').removeClass('hover').addClass('disabled')
          data.files = _this.render_file_list(data.files)

          $('#feedback').on 'click', '.feedback_file_remove', ->
            delete data.fileInput
            $('.feedback_file_list li').remove()

            _this.fileuploaded = false
            $('.fields input').removeAttr('disabled')
            $('.feedback_file_text').removeClass('disabled')


        if _this.fileuploaded is true
          _this.$el.find('.button_row .blue_button').off 'click'
          _this.$el.find('.button_row .blue_button').on 'click', ->

            if !_this.is_ie or ( _this.is_ie  and _this.ie_version > 9 )
              data.submit()
              .error (xhr, status, error) ->
                response = xhr.responseText
                _this.show_error_result(response)
              .success (data, status, xhr) ->
                _this.show_success_result()
            else
              data.submit()

# показываю список прикрепленных файлов
  render_file_list: (files) ->
    @$('ul.feedback_file_list').html('')
    $.each files, (index, file) =>
      if !@is_ie or ( @is_ie  and @ie_version > 9 )
        file_size = Math.round(file.size/1000)
      else
        file_size = ''
      # назначаю индекс для каждого файла
      file.index = index
      @$('ul.feedback_file_list').append(@template_file(file_name: file.name, file_index: file.index, file_size: file_size))
      @set_dialog_position()
    return files

  show_success_result: ->
    @hide_submit_spinner()
    @destroy()
    window.notificationCollection.add
      content: 'Спасибо за обращение! Мы получили твоё письмо и ответим на него в ближайшее время.'
      status: 'notice'

  show_error_result: (response) ->
    @hide_submit_spinner()
    # валидация на стороне сервера
    response = $.parseJSON(response)
    $.each response, (key, value) =>
      if (key == "feedback_attachments.file")
        if (value[1] == 'недопустимый формат файла')
          @show_error($('.feedback_file_text'), $('.feedback_file_link'), 'Допустимые типы файлов: png, jpeg, jpg, gif, doc, docx, odf, pdf')
        else
          @show_error($('.feedback_file_text'), $('.feedback_file_link'), value[0])
      else
        $input = $('#feedback_'+ key)
        @show_error($input, $input.parent(), value[0])

  init_custom_select: ->
    @$('#feedback_feedback_theme_id').select2
      templateResult: null
      containerCssClass: "global_select"
      dropdownCssClass: "global_select feedback_theme"
      placeholder: "Тема"

  remove_file_error: ->
    $('.feedback_file_text')
    .removeClass('error')
    .parent()
    .find('.error_note_wrapper')
    .remove()
