class Stoege.Views.Dialog extends Stoege.Views.Application

  template: JST['backbone/templates/dialogs/dialog']

  events: ->
    'click .close_button' : 'destroy'

  initialize: ->
    @current_path = "#{Backbone.history.getFragment()}#{location.search}"
    if (@current_path.indexOf('/forgot_password') isnt -1) or (@current_path.indexOf('/reset_password') isnt -1)
      @current_path = '/'
    @

  render: ->
    @$el.html(@template(@model.toJSON()))
    $(window).resize =>
      @set_dialog_position()

    @$el.on 'dialogopen', =>
      @set_dialog_position()
      @$('.inner_wrapper').perfectScrollbar('destroy')
      @$('.inner_wrapper').perfectScrollbar({wheelSpeed: 250, minScrollbarLength: 80, suppressScrollX: true})
    @

  open: (dialog_width = 640) ->
    @$el.dialog
      autoOpen: false
      modal: true
      resizable: false
      width: dialog_width
      closeOnEscape: false

    @$el.dialog 'open'

    $('.ui-widget-overlay').click =>
      @destroy()

  close: ->
    @$el.dialog('close')
    Backbone.history.navigate(@current_path, false)

  destroy: ->
    @$el.dialog('destroy')
    Backbone.history.navigate(@current_path, false)

  set_dialog_position: ->
    $inner_wrapper = @$('.inner_wrapper')
    tabs_height = @$('.dialog_tabs').height() or 0
    dialog_width = @$el.width()
    max_height = $(window).height()
    dialog_height = @initial_dialog_height or @$el.height()
    margin_top = (max_height + tabs_height - dialog_height)/2
    margin_top = tabs_height if margin_top < tabs_height

    @$el.parent().css
      marginLeft: "#{-(dialog_width/2)}px"
      marginTop: "#{margin_top}px"
      maxHeight: "#{max_height}px"

    _.delay =>
      @initial_dialog_height or= $inner_wrapper.height()
      min_height = if @initial_dialog_height + tabs_height > max_height then max_height - tabs_height else @initial_dialog_height
      $inner_wrapper.css height: "#{min_height}px"
    , 0

  delete_dialog_param: (dialog_param) ->
    query_params = Backbone.History.prototype.getQueryParameters(window.location.search)
    if query_params.dialog is dialog_param
      delete query_params.dialog

      path = location.pathname
      unless _.isEmpty(query_params)
        path = "#{path}?#{$.param(query_params)}"
      Backbone.history.navigate(path, false)
