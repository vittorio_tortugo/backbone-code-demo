class Stoege.Views.SiteSwitcherDialogView extends Stoege.Views.Dialog

  template: JST['backbone/templates/dialogs/site_switcher']

  className: 'site_switcher_dialog'

  initialize: ->
    @current_path = "#{Backbone.history.getFragment()}#{location.search}"
    @model = new Stoege.Models.DialogModel
    @render()
    @open(748)

  open: (dialog_width = 640) ->
    super(dialog_width)
    @$('a').blur()