class Stoege.Views.RegistrationDialog extends Stoege.Views.Dialog

  template: JST['backbone/templates/dialogs/registration/dialog']

  className: 'registration_dialog'

  events: ->
    _.extend {}, super,
      'click .dialog_tabs li' : 'change_role'
      'submit' : 'enable_email'
      'click input.error, select.error' : 'remove_error'
      'select2-focus .select.error' : 'remove_error'

  initialize: ->
    super
    @listenTo(@model, 'change:role', @set_role_tab)
    @listenTo(@model, 'change:role', @set_role_form)
    @

  open_registration: (params) ->
    new Stoege.Services.Timezone().save_to_cookie()
    dataLayer?.push('event': 'form-sign-up-show')

    if params?.provider
      social_type = switch params.provider
        when 'vkontakte' then 'vk'
        when 'google_oauth2' then 'google'
        when 'mail_ru' then 'mailru'
        when 'facebook' then 'facebook'
        when 'odnoklassniki' then 'ok'

      dataLayer?.push('event': 'signup_social_form_show', 'signup_type': social_type) if social_type?

    if $('.registration_dialog').length
      @open()
    else
      # устанавливаю атрибуты provide и authorization
      if params?
        @model.set(params)

      @model.fetch
        data: params
        success: =>
          @render()
          @open()
          @change_role_to_teacher() if location.pathname is '/kpk'

          @initialize_registration_view()
          @mask_phone()

    @set_role_form @model, 'teacher' if user.is_teacher()
    $.removeCookie('is_registrated', { path: '/'})
    @

  initialize_registration_view: ->
    @prepare()
    $('.user_middle_name').addClass('hidden_block') unless user.is_teacher()
    @model.set(role: 'teacher') if user.is_teacher()

  mask_phone: ->
    @$phone_input = @$('.registration_phone')
    @$phone_input.mask '+7 (999) 999 99 99', autoclear: false

  change_role: (e) ->
    role = $(e.currentTarget).attr('data-type')
    @model.set(role: role)

  change_role_to_teacher: ->
    role = 'teacher'
    @model.set(role: role)

  set_role_tab: (model, value) ->
    $('.dialog_tabs li.active').removeClass('active')
    $(".dialog_tabs li[data-type='#{value}']").addClass('active')

  set_role_form: (model, value) ->
    # TODO перевести на модель user
    $middle_name = $('.user_middle_name')
    $user_grade_block = $('.user_grade_id')
    $user_grade_select = $('#user_grade_id')
    $user_is_teacher = $('#user_is_teacher')
    $social_buttons = $('ul.dialog_social')
    $dialog_header = @$('.dialog_header h2')
    graduated_id = $user_grade_select.attr('data-graduated-id')
    if value is 'teacher'
      $middle_name.removeClass('hidden_block')
      $user_grade_select.append("<option value=#{graduated_id}></option>")
      $user_grade_select.val(graduated_id)
      $user_grade_block.addClass('hidden_block')
      $user_is_teacher.attr('disabled', false)
      $user_is_teacher.val('true')
      user.be_teacher()
      $dialog_header.text('Зарегистрируйтесь и начните учиться бесплатно!')
    else
      $middle_name.addClass('hidden_block')
      $user_grade_select.val('')
      $user_grade_select.find("option[value='#{graduated_id}']").remove()
      $user_grade_block.removeClass('hidden_block')
      $user_is_teacher.attr('disabled', true)
      $user_is_teacher.val('false')
      user.be_pupil()
      $dialog_header.text('Зарегистрируйся и начни учиться бесплатно!')

  destroy_view: ->
    @$el.dialog('destroy')
    @clear_params()

  destroy: ->
    @$el.dialog('close')
    $.removeCookie('after_sign_in_path', { path: '/' })
    @clear_params()

  clear_params: ->
    query_params = Backbone.History.prototype.getQueryParameters(window.location.search)
    delete query_params.provider
    delete query_params.authorization
    delete query_params.registration

    path = Backbone.history.getFragment().replace(window.location.search, '')
    path = "#{path}?#{$.param(query_params)}" unless _.isEmpty(query_params)
    Backbone.history.navigate(path, true)

  prepare: (options = {}) ->
    @current_path = $.cookie('after_sign_in_path') or $.cookie('registration_page') or options.current_path
    @set_ajax_callbacks()
    @init_custom_select()

  enable_email: ->
# для случая подтверждения emailа, поле должно быть задизейблено.
    @$('input[type="email"][data-disabled="true"]').prop('disabled', false)

  set_ajax_callbacks: ->
    @$el
    .on 'ajax:beforeSend', =>
      @show_submit_spinner()
    .on 'ajax:success', (evt, data, status, xhr) =>
      @destroy_view()
      # если авторизавались на сайт, перезагружаю страницу
      if data.authenticated
        window.location = @current_path
# если, нужно подтверждение пароля, показываю форму авторизации
      else if data.need_authentication
        query_params = Backbone.History.prototype.getQueryParameters(@current_path)
        query_params.provider = data.provider
        query_params.authorization = true
        query_params.registration = true

        path = "#{@current_path.split('?')[0]}?#{$.param(query_params)}"
        Backbone.history.navigate(path, true)
# если, нужно подтверждение почты
      else
        dataLayer.push('event': 'message-email-confirmation') if dataLayer?
        window.notificationCollection.add
          content: 'Чтобы закончить регистрацию, перейди по ссылке в письме от нас. Если письма нет — проверь папку "Спам".'
          status: 'warning'
      $.cookie('is_registrated', true, { path: '/', expires: 7 })
    .on 'ajax:error', (evt, xhr, status, error) =>
      @hide_submit_spinner()
      json = $.parseJSON(xhr.responseText)
      $.each json, (key, value) =>
# ошибку соцсети выводим в алерт поля email
        if key in ['social_nets.uid', 'social_nets.provider', 'social_nets.user']
          $input = @$('#user_email')
        else
          $input = @$('#user_' + key)
        $input = @$('#user_' + key + '_id') if key is 'grade'

        @show_error($input, $input.parent(), value[0])

  init_custom_select: ->
    @$('#user_grade_id').select2
      templateResult: null
      containerCssClass: 'global_select registration_user_grade'
      dropdownCssClass: 'global_select registration_user_grade'
      placeholder: 'Выберите класс'
