class Stoege.Views.ResetPasswordDialogView extends Stoege.Views.Dialog

  template: JST["backbone/templates/dialogs/reset_password"]

  className: 'reset_password_dialog'

  open_reset_password: (params) ->
    @model.fetch
      data: params
      success: =>
        @render()
        @open()
        @initialize_reset_password_view()

  initialize_reset_password_view: ->
    @resetPasswordView = new Stoege.Views.ResetPasswordView(current_path: @current_path)
    @listenTo(@resetPasswordView, 'reset_password_success', @destroy, this)
