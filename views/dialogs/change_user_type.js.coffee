class Stoege.Views.UserTypeChangesDialogView extends Stoege.Views.Dialog

  template: JST["backbone/templates/dialogs/change_user_type"]

  className: 'user_type_changes_dialog'

  initialize: ->
    @model = new Stoege.Models.DialogModel

    if user.is_teacher()
      @render()
      @open(836)
    else
      window.notificationCollection.add
        content: 'Эта ссылка доступна только учителям'
        status: 'alert'

  destroy: ->
    @$el.dialog('destroy')
    @remove()

    if location.pathname.indexOf('user_type_changes') isnt -1
      Backbone.history.navigate('/', false)
    else
      @delete_dialog_param('user_type_changes')