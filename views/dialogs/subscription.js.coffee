class Stoege.Views.SubscriptionDialog extends Stoege.Views.Dialog

  template: JST["backbone/templates/dialogs/subscription"]

  className: 'subscription_dialog'

  initialize: ->
    @model = new Stoege.Models.DialogModel
    @model.url = '/subscribers/new'
    @model.fetch()
    @listenTo(@model, 'sync', @open_subscription, @)
    @listenTo(@model, 'error', @open_notification, @)

  open_notification: ->
    window.notificationCollection.add
      content: "#{you_text('Вы уже подписаны', 'Ты уже подписан')} на нашу рассылку."
      status: 'warning'

    @delete_dialog_param('subscribe')

  open_subscription: ->
    @render()
    @open()
    @initialize_subscription_view()

  initialize_subscription_view: ->
    subscription_view = new Stoege.Views.Subscription(current_path: @current_path)
    @listenTo(subscription_view, 'subscription_success', @destroy, this)

  destroy: ->
    @$el.dialog('destroy')
    @undelegateEvents()
    @.remove()

    @delete_dialog_param('subscribe')
