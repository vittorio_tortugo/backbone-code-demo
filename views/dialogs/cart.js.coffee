class Stoege.Views.CartDialog extends Stoege.Views.Dialog

  template: JST["backbone/templates/cart/cart"]
  promo_code_template: JST["backbone/templates/cart/promo_code"]
  transaction_template: JST["backbone/templates/cart/transaction"]
  qiwi_template: JST["backbone/templates/cart/transaction/qiwi_transaction_form"]
  qiwi_check_template: JST["backbone/templates/cart/transaction/qiwi_check"]
  rapida_template: JST["backbone/templates/cart/transaction/rapida_transaction_form"]
  rapida_check_template: JST["backbone/templates/cart/transaction/rapida_check"]
  error_note_template: JST["backbone/templates/error_note"]

  className: 'cart_content'

  events:
    'click .close_button': 'destroy'
    'click .cart_content_info .submit_payment:not(.rapida)': 'submit_cart'
    'click .qiwi.fxf_button, .rapida.fxf_button': 'submit_form'
    'click .print': 'print_content'
    'click .cart_prices_panels > li:not(.barcode)': 'choose_period'
    'click .payment_list > li': 'choose_payment_method'
    'click .off_auto_purchase': 'disable_auto_purchase'
    'click .promo_codes_wrapper .pseudo': 'open_promo_codes_input'
    'click .promo_codes_wrapper input[type=submit]': 'check_promo_codes'
    'click .submit_promo_code': 'submit_promo_code'
    'click .video_wrapper .text': 'visibility_rapida_video'
    'click input[type=text]': 'reset_error'

  initialize: ->
    @current_path = "/courses/#{@model.get('course_id')}"
    @listenToOnce(@model, 'sync', @render_dialog_cart, @)
    @listenTo(@model, 'sync', @update_summary, @)
    @listenTo(@model, 'change', @update_summary, @)

    @fetch_cart()

  reset_error: ->
    #TODO реализоваться подобное поведение для всех инпутов в приложении
    @$('input[type=text]').removeClass 'error'

  render_dialog_cart: ->
    @render()
    @hide_payment_button()
    @$('.cart_prices_panels > li.selected:not(.barcode)').first().click()
    @open(660)

  render_promo_code: ->
    if @model.get('code_campaign_type') is 'invite'
      @$('.cart_content_info').html(@promo_code_template(cart: @model))
      @set_dialog_position()

  submit_promo_code: ->
    $.ajax
      type: "POST"
      url: " /api/courses/#{@model.get('course_id')}/invite_activations"
      dataType: 'text'
      data:
        code: @model.get('code')
      success: =>
        @destroy()
        window.notificationCollection.add
          content: 'Промо-код активирован'
          status: 'notice'
      error: ->
        window.notificationCollection.add
          content: 'Ошибка при активации промо-кода'
          status: 'alert'

  fetch_cart: ->
    @model.fetch
      url: @model.urlRoot() + '/new'

  render: ->
    if @model.is_rapida()
      @$el.html(@rapida_template(model: @model))
      @$("#phone_number").mask("+7 9999999999", autoclear: false)
    else if @model.is_qiwi()
      @$el.html(@qiwi_template(model: @model))
      @$("#phone_number").mask("+7 9999999999", autoclear: false)
    else
      title = $('.detail_block_wrapper.prices, .detail_block_wrapper.paid').find('.red_button').text()
      @$el.html(@template(cart: @model.attributes, title: title))
      dataLayer?.push('event': 'form-pay-show')

    #TODO переделать под общий render в app/assets/javascripts/backbone/views/dialog.js.coffee
    $(window).resize =>
      @set_dialog_position()
    @$el.on 'dialogopen', () =>
      @set_dialog_position()
      @$('.inner_wrapper').perfectScrollbar('destroy')
      @$('.inner_wrapper').perfectScrollbar({wheelSpeed: 250, minScrollbarLength: 80, suppressScrollX: true})
    @

  destroy: ->
    @$el.dialog('destroy').remove()
    Backbone.history.navigate(@current_path, true)

  send_error_msg: ->
    window.notificationCollection.add
      content: "Не удалось купить курс!"
      status: 'alert'

  submit_cart: ->
    if @model.is_rapida() or @model.is_qiwi()
      @render()
    else
      @submit()

  submit: ->
    call_data_layer = (model) ->
      dataLayer?.push('event': 'form-pay-click', 'type': model.get('type'), 'payment_method': model.get('payment_method')) if model

    @model.save null,
      success: (data) =>
        call_data_layer data
        @render_assist_transaction_form(data)
      error: =>
        call_data_layer null # TODO: send data from error to call_data_layer
        @send_error_msg()

  render_assist_transaction_form: (model) ->
    return unless model

    template = @transaction_template(type: model.get('type'), payment_method: model.get('payment_method'), transaction: model)

    $(".assist_form").append template
    $('.assist_form form').submit()

  validate_fields: ->
    is_valid = true
    @$("input").each (index, input) =>
      $input = $(input)
      unless $input.val().length
        @show_error($input, $input.parent(), 'Не может быть пустым')
        is_valid = false
    is_valid

  submit_form: ->
    if @validate_fields()
      phone = $('#phone_number').val()
      phone_formatted = phone.substr(3, phone.length)

      @model.set(phone: phone_formatted)
      @model.save {},
        success: (data) =>
          if @model.is_rapida()
            @render_rapida_check(data)
          else if @model.is_qiwi()
            @render_qiwi_check(data)
          @set_dialog_position()
        error: ->
          window.notificationCollection.add
            content: "Не удалось купить курс!"
            status: 'alert'

  render_rapida_check: (model) ->
    $('.cart_content').html(@rapida_check_template(model: model))

  render_qiwi_check: (model) ->
    $('.cart_content').html(@qiwi_check_template(model: model))

  choose_period: (e) ->
    return if @model.get('payment_type') is $(e.target).closest('li').data('type')
    $('.cart_prices_panels > li.selected').removeClass('selected')
    $(e.target).closest('li').addClass('selected')

    @model.set(payment_type: $(e.target).closest('li').data('type'))
    @hide_payment_button()
    @toggle_annotation()

  toggle_annotation: ->
    is_hidden = @model.get('payment_type') isnt 'month_access'
    @$('.annotation').toggleClass('hidden', is_hidden)

  choose_payment_method: (e) ->
    $payment_item = $(e.target).closest('li')
    $('.payment_list > li').removeClass('selected')
    $payment_item.addClass('selected')

    @model.set(payment_method: $payment_item.data('payment_method'))
    $('.submit_button.fxf_button').attr('data-payment', $payment_item.data('payment_method'))

  disable_auto_purchase: (event) ->
    @model.disable_auto_purchase().success ->
      $(event.target).parent().text("Автоплатеж отключен")
    false

  #TODO: Фронты корзины сделаны на джейквери, без ререндера. Код сложно поддерживать. Отрефакторить при ближайших изменениях бизнес-логики.
  hide_payment_button: ->
    $selected_payment_item = @$(".payment_list > li.selected")
    hidden_payments = @model.get_hidden_payments()
    full_price = @model.get_full_price()
    big_payments_supports = @model.get_big_payments_supports()
    last_selected_payment_method = $selected_payment_item.data('payment_method') or 'card_payment'
    $selected_payment_item.removeClass('selected')

    for payment in hidden_payments
      $payment_item = @$(".payment_list > li[data-payment_method=#{payment}]")
      if full_price >= 15000 and @model.get('payment_type') is 'entire_course'
        $payment_item.toggleClass 'hidden_block', (big_payments_supports.indexOf(payment) is -1)
      else
        $payment_item.removeClass('hidden_block')

    payment_options = @$(".payment_list > li:not(.hidden_block)").map(-> $(@).data('payment_method'))
    last_selected_payment_method = payment_options[0] if _.indexOf(payment_options, last_selected_payment_method) is -1
    @$(".payment_list > li[data-payment_method=#{last_selected_payment_method}]").addClass('selected')
    @model.set(payment_method: last_selected_payment_method)
    @

  open_promo_codes_input: (e) ->
    $(e.target).siblings('.input').removeClass('hidden_block').children('[type=text]').focus()
    $(e.target).addClass('hidden_block')
    @

  check_promo_codes: (e) =>
    promo_code = $('.promo_codes_wrapper input[type=text]').val()
    $.ajax
      type: "POST"
      url: " /api/courses/#{@model.get('course_id')}/check_promo_codes"
      data:
        check_promo_code:
          cart_item_type: @model.get('payment_type')
          code: promo_code
    .error (request) =>
      data = JSON.parse request.responseText
      @$('input[type=text]').addClass('error') if data.errors
    .done (response) =>
      if response.errors
        window.notificationCollection.add
          content: JSON.stringify response.errors
          status: 'alert'
      else
        @model.apply_promocode(promo_code, response)

        if response.success_message
          success_message_formatted = response.success_message.replace(/руб\./gi, '<span class="rouble">q</span>')

          $('.promo_codes_wrapper .pseudo.success').removeClass('hidden_block').html success_message_formatted

          window.notificationCollection.add
            content: success_message_formatted
            status: 'notice'

        $('.promo_codes_wrapper .input').addClass('hidden_block')
        @render_promo_code()
        @hide_payment_button()

  update_summary: (model, options) ->
    return if @$('.cart_promo_code').length

    if(model.get('payment_type') is 'entire_course')
      full_price = model.get 'full_price'
      full_price_with_discount = model.get 'full_price_with_discount'
      full_price_note = "(#{@model.get('remaining_months')} #{model.get('remaining_months_text')} x #{model.get 'full_price_by_month'}<span class='rouble'>q</span>)"
    else
      full_price = model.get 'month_price'
      full_price_with_discount = model.get 'month_price_with_discount'
      full_price_note = '' # удаляем запись для помесячной оплаты
    #TODO FIXME сравниваются декорированные строки (они кастуюются нормально, но это не дело)
    has_discount = full_price > full_price_with_discount

    $('.cart_content .full_price:not(.discount)').text full_price_with_discount
    if has_discount
      $('.cart_content .full_price.discount').text full_price
    else
      $('.cart_content .full_price.discount').empty()
    $('.cart_content .full_price_note').html full_price_note
    @

  print_content: ->
    $('.print').parent().jqprint()

  visibility_rapida_video: ->
    $content = @$('.video_wrapper')
    text = if $content.hasClass('active') then 'Посмотрите видеоинструкцию об оплате' else 'Свернуть'

    $content.toggleClass('active')
    $content.find('.video_content').slideToggle()
    $content.find('.open_button .text').html(text)
