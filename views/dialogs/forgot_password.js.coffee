class Stoege.Views.ForgotPasswordDialogView extends Stoege.Views.Dialog
  template: JST['backbone/templates/dialogs/forgot_password']
  className: 'forgot_password_dialog'
  events: ->
    _.extend {}, super,
      'keyup input.email' : 'toggle_submit_button'
      'change input.email' : 'toggle_submit_button'
      'input input.email' : 'toggle_submit_button'
      'click input.error' : 'remove_error'

  initialize: ->
    super
    @current_path = '/' if @current_path.indexOf('/forgot_password') isnt -1

  open_forgot_password: ->
    @model.fetch
      success: =>
        @render()
        @open()
        @initialize_forgot_password_view()

  initialize_forgot_password_view: ->
    @prepare()

  prepare: ->
    @set_ajax_callbacks()

  set_ajax_callbacks: ->
    @$el
    .on 'ajax:beforeSend', =>
      @show_submit_spinner()
    .on 'ajax:success', (evt, data, status, xhr) =>
      @destroy()
      window.notificationCollection.add
        content: "Чтобы установить новый пароль, #{you_text('перейдите', 'перейди')} по ссылке в письме, которое мы #{you_text('Вам', 'тебе')} отправили."
        status: 'warning'
    .on 'ajax:error', (evt, xhr, status, error) =>
      @hide_submit_spinner()
      json = $.parseJSON(xhr.responseText)
      $.each json, (key, value) =>
        $input = @$('input.email')
        @show_error($input, $input.parent(), value)

  toggle_submit_button: ->
    is_disable = not @$('input.email').val().length > 0
    @$('input.blue_button').prop('disabled', is_disable).toggleClass('disabled', is_disable)
