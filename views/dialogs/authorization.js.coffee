#= require ./dialog.js.coffee
class Stoege.Views.AuthorizationDialogView extends Stoege.Views.Dialog
  template: JST["backbone/templates/dialogs/authorization"]

  className: 'authorization_dialog'

  events: ->
    _.extend {}, super,
      'click .dialog_tabs li' : 'change_role'
      'keyup input.email' : 'enable_submit_button'
      'change input.email' : 'enable_submit_button'
      'input input.email' : 'enable_submit_button'
      'keyup input.password' : 'enable_submit_button'
      'change input.password' : 'enable_submit_button'
      'input input.password' : 'enable_submit_button'
      # FIXME форму можно сабмитить не кликая по кнопке, а нажав enter
      'click .submit_blue_button' : 'remove_password_spaces'
      'click input.error' : 'remove_error'

  open_authorization: ->
    $('ul.dialog_social').show()
    new Stoege.Services.Timezone().save_to_cookie()
    @close_dialogs()
    @sanitize_url_from_registration_params()
    if $('.authorization_dialog').length
      @$('h2').text(you_text('Авторизуйтесь и продолжите обучение', 'Авторизуйся и продолжи обучение'))
      @open()
    else
      @render()
      @open()
      @prepare()
    @

  close_dialogs: ->
    $('.ui-dialog-content').dialog('close')

  destroy: ->
    $.removeCookie('after_sign_in_path', { path: '/' })
    @$el.dialog('close')

  sanitize_url_from_registration_params: ->
    query_params = Backbone.History.prototype.getQueryParameters(window.location.search)
    delete query_params.provider
    delete query_params.authorization
    delete query_params.registration

    path = @root_striped_path()

    unless _.isEmpty(query_params)
      path = "#{path}?#{$.param(query_params)}"
    Backbone.history.navigate(path, false)

  #FIX_ME same method in global view
  root_striped_path: ->
    root_regexp = new RegExp("^#{Backbone.history.root}")
    Backbone.history.location.pathname.replace(root_regexp, '')

  prepare: ->
    @set_ajax_callbacks()
    @check_form = setInterval(@enable_submit_button, 200)
    setTimeout(@clear_check_form, 5000)

  clear_check_form: =>
    clearInterval @check_form

  set_ajax_callbacks: ->
    @$el
    .on 'ajax:beforeSend', =>
      @show_submit_spinner()
    .on 'ajax:success', (evt, data, status, xhr) =>
      window.location = $.cookie('after_sign_in_path') or data.after_sign_in_path if data.authenticated?
    .on 'ajax:error', (evt, xhr, status, error) =>
      @hide_submit_spinner()
      return window.notificationCollection.add_error 'Произошла ошибка' unless xhr.responseText.length
      $input = @$('input.password')
      json = $.parseJSON(xhr.responseText)
      $.each json, (key, value) =>
        @show_error($input, $input.parent(), value)

  enable_submit_button: =>
    if @$('input.email').val() and @$('input.password').val()
      @$('input.fxf_button').prop('disabled', false).removeClass('disabled')
    else
      @$('input.fxf_button').prop('disabled', true).addClass('disabled')

  remove_password_spaces: ->
    $input_password = @$('input.password')
    $input_password.val($input_password.val().replace(/^\s+/g, '').replace(/\s+$/g, ''))
