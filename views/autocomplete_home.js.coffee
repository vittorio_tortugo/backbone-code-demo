#= require ./autocomplete_school.js.coffee
class Stoege.Views.AutocompleteHome extends Stoege.Views.AutocompleteSchool

  initialize: ->
    @$country_select = @$('#address_country_id')
    @$region_name = @$('#user_pupil_address_attributes_region_name')
    @$region_id = @$('#user_pupil_address_attributes_region_id')
    @$city_name = @$('#user_pupil_address_attributes_city_name')
    @$city_id = @$('#user_pupil_address_attributes_city_id')
    @$street = @$('#user_pupil_address_attributes_street')
    @$house = @$('#user_pupil_address_attributes_house')
    @$building = @$('#user_pupil_address_attributes_building')
    @$room = @$('#user_pupil_address_attributes_room')

    @change_country()
    @change_region()
    @change_city()
    @remove_no_existing_match()

  change_country: ->
    @$country_select.change =>
      country_id = @$country_select.val()

      @clean_fields(@$region_name, @$region_id)
      @clean_fields(@$city_name, @$city_id)
      @clean_fields(@$street, @$street)
      @disable_fields(@$city_name, @$city_id)
      @disable_fields(@$street, @$street)
      @$region_name.attr('data-autocomplete', "/api/regions?country_id=#{country_id}")

  change_region: ->
    @$region_name.on 'autocompleteselect', (event, ui) =>
      region_id = $(ui.item)[0].id

      @clean_fields(@$city_name, @$city_id)
      @enable_fields(@$city_name, @$city_id)
      @clean_fields(@$street, @$street)
      @$city_name.attr('data-autocomplete', "/api/cities?region_id=#{region_id}")

    @$region_name.on 'autocompletechange', (event, ui) =>
      unless ui.item
        @clean_fields(@$city_name, @$city_id)
        @disable_fields(@$city_name, @$city_id)
        @clean_fields(@$street, @$street)
        @disable_fields(@$street, @$street)
        error_message = if @$region_name.val() then 'Такого нет в списке' else 'Не может быть пустым'
        @show_error(@$region_name, @$region_name.parent(), error_message)

    @$region_name.on 'keyup', =>
      unless @$region_name.val()
        @clean_fields(@$city_name, @$city_id)
        @disable_fields(@$city_name, @$city_id)
        @clean_fields(@$street, @$street)
        @disable_fields(@$street, @$street)

  change_city: ->
    @$city_name.on 'autocompleteselect', (event, ui) =>
      city_id = $(ui.item)[0].id

      @clean_fields(@$street, @$street)
      for element in [@$street, @$house, @$building, @$room]
        @enable_fields(element, element)
      @

    @$city_name.on 'keyup', =>
      @clean_fields(@$street, @$street)
      if @$city_name.val()
        for element in [@$street, @$house, @$building, @$room]
          @enable_fields(element, element)
      else
        for element in [@$street, @$house, @$building, @$room]
          @disable_fields(element, element)

  remove_no_existing_match: ->
    @$el.on 'autocompleteresponse', '#user_pupil_address_attributes_region_name, \
                                     #user_pupil_address_attributes_city_name, \
                                     #user_pupil_address_attributes_street', (event, ui) ->
      $(@).autocomplete('close') if ui.content?[0].id.length == 0