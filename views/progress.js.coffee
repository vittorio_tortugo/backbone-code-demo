class Stoege.Views.Progress extends Backbone.View

  polar_to_cartesian: (center_x, center_y, radius, angle_in_degrees) ->
    angle_in_radians = (angle_in_degrees-90) * Math.PI / 180.0

    x: center_x + (radius * Math.cos(angle_in_radians))
    y: center_y + (radius * Math.sin(angle_in_radians))

  describe_arc: (x, y, radius, start_angle, end_angle, reverse) ->
    start = @polar_to_cartesian(x, y, radius, end_angle)
    end = @polar_to_cartesian(x, y, radius, start_angle)

    angle_diff = end_angle - start_angle
    angle_diff = -angle_diff if angle_diff < 0

    arc_sweep = if angle_diff <= 180 then '1' else '0'
    arc_sweep = +!+arc_sweep if reverse

    [
      "M", start.x, start.y
      "A", radius, radius, 0, arc_sweep, 0, end.x, end.y
    ].join(" ")

  draw_circle_progress: ($progress) ->
    value = +($progress.attr 'data-progress')
    $svg = $progress.find('svg')
    center = parseInt($svg.width() / 2)
    thick = parseInt($svg.find('path.progress_line').attr 'stroke-width')

    progress_gap = 0.5
    progress_start_angle = 180
    progress_end_angle = progress_start_angle - progress_gap + (value / 100) * 360

    bg_start_angle = progress_start_angle
    bg_end_angle = 180 - progress_gap

    center_x = center - thick / 2
    center_y = center - thick / 2
    radius = center - thick

    progress_end_coords = @polar_to_cartesian(center_x, center_y, radius, progress_end_angle)

    $progress.find('.progress_line').attr('d', @describe_arc(center_x, center_y, radius, progress_start_angle, progress_end_angle, true)) unless !value

    bg_angle_diff = bg_start_angle - bg_end_angle
    bg_angle_diff = -bg_angle_diff if bg_angle_diff < 0

    $progress.find('.progress_bg').attr('d', @describe_arc(center_x, center_y, radius, bg_start_angle, bg_end_angle)) if bg_angle_diff < 360

