class Stoege.Views.FooterEmailDialog extends Backbone.View

  el: '.main_footer'

  events:
    'click .google_play .link' : 'open_dialog_for_email'
    'click .window_store .link' : 'open_dialog_for_email'

  open_dialog_for_email: (e) ->
    mobile_os = $(e.target).data('mobile')
    @email_dialog_view = new Stoege.Views.LandingEmailDialogView
    @email_dialog_view.open_dialog_for_email(mobile_os)
