class Stoege.Views.EventsLibraryItem extends Backbone.View

  tagName: 'li'

  template_cards: JST["backbone/templates/event/item_table"]
  template_list: JST["backbone/templates/event/item_list"]

  initialize: ({@filter_type}) -> @
  render: ->
    @$el.html @["template_#{@filter_type}"]
      course: @model.toJSON()
    @
