class Stoege.Views.EventsPageView extends Backbone.View
  el: '.course_page'

  course_template: JST['backbone/templates/course/course_list_item']
  not_found_template: JST['backbone/templates/course/not_found']

  events:
    'click .state .fxf_button[data-state]': 'check_event_availability_and_apply_action'
    'mouseenter .library_list ul > li': 'set_course_hover_color'
    'mouseleave .library_list ul > li': 'set_course_default_color'
    'mouseenter .recommendations ul > li': 'set_course_hover_color'
    'mouseleave .recommendations ul > li': 'set_course_default_color'

  initialize: ({@params, @event_id, @users_collection}) ->
    @users_scroller or= new Stoege.Views.EventsUsersList collection: @users_collection
    @update_countdown or= (setInterval @remaining_update, 1000 * 30)
    @remaining_update()
    @init_share_links()
    @

  init_share_links: ->
    @$('.share div').ShareLink
      url: location.href
      width: 640
      height: 480
    @$('.share .count').ShareCounter
      url: location.href
    @

  open_webinar_url: ->
    webinar_url = @$el.attr('data-webinar-url')
    if gon.state in ['started', 'finished']
      window.open "#{location.protocol}//#{location.host}#{webinar_url}", '_blank'
    @

  check_event_availability_and_apply_action: (e) =>
    e.preventDefault()

    unless user.is_authorized()
      $.cookie 'registration_page', location.pathname, path: '/'
      $.cookie 'subscribe_to_event', @event_id, path: '/'
      Backbone.history.navigate("#{location.pathname}?registration=true", true)
      return

    if gon?.email_not_confirmed
      addressee = you_text('Подтвердите', 'Подтверди')
      button_code = "<a class='email_confirmation_link blue_button'>Прислать мне письмо повторно</a>"
      message = ' свой email, кликнув по ссылке в письме-подтверждении.'
      notificationCollection.add_success "#{addressee}#{message}#{button_code}"
      return

    if gon.state is 'not_subscribed'
      $.ajax
        type: 'POST'
        async: false
        url: "/api/events/#{@event_id}/user_events"
        beforeSend: ->
          dataLayer?.push('event': 'oz_register_click')
        success: (msg) =>
          gon.state = msg.state if msg.state
          @remaining_update().open_webinar_url()
          @users_collection.reset()
          @users_collection.fetch()
          dataLayer?.push('event': 'oz_register_success')
        error: (xhr, textStatus) ->
          window.notificationCollection.add
            content: xhr?.responseJSON?.error or 'Неизвестная ошибка, попробуйте позже'
            status: 'alert'
    else
      @open_webinar_url()
    @

  render_action_block: (data = {}) ->
    data.state = gon.state
    data.remaining = gon.remaining
    data.event_state = gon.event_state

    @$('.fxf_events_header .state').html JST['backbone/templates/event/action_block'](data)
    @

  set_course_hover_color: (e) ->
    $target = $(e.target).closest('li').find('.icon')
    $target = $(e.target).closest('li') unless $target.length
    dark_color = $target.data('hover_color')

    $target.css('background-color', "##{dark_color}")

  set_course_default_color: (e) ->
    $target = $(e.target).closest('li').find('.icon')
    $target = $(e.target).closest('li') unless $target.length
    color = $target.data('color')

    $target.css('background-color', "##{color}")

  remove_view: ->
    @users_scroller.reset()
    @

  remaining_update: =>
    if gon.remaining and gon.starts_at
      data = Stoege.Services.Countdown.get_countdown_data_from_now(gon.starts_at)
      delete gon.remaining if data.countdown_time_diff <= 0
    else
      data = {}

    @render_action_block data
    @
