class Stoege.Views.EventsLibraryList extends Backbone.View

  el: '.library_courses_wrapper'

  course_template: JST["backbone/templates/course/course_list_item"]
  not_found_template: JST["backbone/templates/course/not_found"]
  courses_list_template: JST["backbone/templates/event/courses_list"]

  events:
    'mouseenter .library_cards > li': 'set_course_hover_color'
    'mouseleave .library_cards > li': 'set_course_default_color'

  initialize: ({@params, @grades, @disciplines, @states, @series, @filter_view}) ->
    @listenTo(@collection, 'reset', @render, @)
    @listenTo(@collection, 'sync', @collection_sync, @)
    @listenTo(@filter_view.filter_type, 'change:type', @render, @)
    @load_courses()

    $(window).on 'scroll', =>
      @load_more_courses()
    @

  load_courses: ->
    @collection.state.currentPage = 1
    grade_id = @grades.findWhere(selected: true)?.get('id')
    delete @params.grade_id if not grade_id or @params.grade_id is '0'
    @$('.courses_list_block').hide().html(@courses_list_template(params: @params))
    @collection.fetch
      data: @params

  render: ->
    $courses_list_block = @$('.courses_list_block')
    $courses_list_block.show()
    $courses_list_past = $courses_list_block.find('.courses_list_past')
    $courses_list_future = $courses_list_block.find('.courses_list_future')
    $courses_list_now = $courses_list_block.find('.courses_list_now')
    if @collection.length
      if @params.state?
        @collection.each (course) =>
          @render_course course, $courses_list_block
      else
        # прошлые
        @render_courses_list ['finished'], $courses_list_past
        # будущие
        @render_courses_list ['created', 'ready_to_start'], $courses_list_future
        # сейчас онлайн
        @render_courses_list ['started'], $courses_list_now
    else if @collection.state.currentPage is 1
      $courses_list_block.html(@not_found_template(title: 'занятия'))

    @$('.load_more_courses').addClass('hidden')
    @$(".dotdotdot").dotdotdot
      watch: "window"
    @

  render_courses_list: (states, $block) ->
    courses = _.filter @collection.models, (course_model) ->
      course_model.get('event_state') in states

    unless courses.length
      unless $block.find('li').length
        $block.hide()
    else
      for course in courses
        @render_course course, $block

  render_course: (course, $block) ->
    course_view = new Stoege.Views.EventsLibraryItem(model: course, filter_type: @filter_view.filter_type.get('type'))
    $block.find('.library_cards').append(course_view.render().el)

  load_more_courses: ->
    return if !@collection.hasNextPage() or @collection.length < 3
    scroll_bottom = $(window).scrollTop() + $(window).height()
    course_top = $('.library_cards > li').last().offset().top
    if course_top < scroll_bottom
      @collection.getNextPage
        data: @params
      @$('.load_more_courses').removeClass('hidden')

  collection_sync: ->
    @$('.load_more_courses').addClass('hidden')
    @

  set_course_hover_color: (e) ->
    $target = $(e.target).closest('li').find('.course_icon_wrapper')
    dark_color = $target.data('hover_color')

    $target.css('background', "##{dark_color}")

  set_course_default_color: (e) ->
    $target = $(e.target).closest('li').find('.course_icon_wrapper')
    color = $target.data('color')

    $target.css('background', "##{color}")

  remove_view: ->
    @undelegateEvents()
    @stopListening(@collection, 'reset')
    $(window).off('scroll')
