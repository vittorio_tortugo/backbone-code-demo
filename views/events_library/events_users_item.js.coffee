class Stoege.Views.EventsUsersItem extends Backbone.View

  tagName: 'li'

  template: JST["backbone/templates/event/item_user"]

  render: ->
    @$el.html(@template(user: @model.toJSON()))
    @
