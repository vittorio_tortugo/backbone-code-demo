class Stoege.Views.EventsUsersList extends Backbone.View

  el: '.users'

  events:
    'click .button_left': 'scrollPrev'
    'click .button_right': 'scrollNext'

  initialize: ->
    @user_element_default_width = 85
    @$user_count_element = @$('.users_count')
    @$users_list_element = @$('ul')
    @$users_content_element = @$('.content')
    @listenTo @collection, 'reset', @update_users_scroller, @
    $(window).on 'resize', => @reflow()
    @$el.hide()
    @load_users()
    @

  reset: ->
    @stopListening @collection
    @$users_list_element.css
      marginLeft: 0
    @$users_list_element.empty()
    @

  update_users_scroller: ->
    if @collection.length
      @collection.each (user) =>
        user_view = new Stoege.Views.EventsUsersItem
          model: user
        @$users_list_element.append user_view.render().el
        @reflow()
      @$el.show()
    else if @collection.state.currentPage is 1
      @$users_list_element.empty()
      @$el.hide()

    @$user_count_element.text @collection.users_count
    @reflow()
    @


  reflow: ->
    @page_width = @$users_content_element.width()
    $user_blocks = @$users_list_element.children('li')
    $user_blocks.width if @page_width % @user_element_default_width < @user_element_default_width then Math.ceil(@page_width / Math.floor(@page_width / @user_element_default_width)) else @user_element_default_width

    @content_width = $user_blocks.width() * $user_blocks.size()
    @$users_list_element.width @content_width
    has_buttons_flag = ($user_blocks.width() * @collection.users_count) > @page_width
    @$('.button_left').toggle has_buttons_flag
    @$('.button_right').toggle has_buttons_flag
    @scrollPrev() if !has_buttons_flag
    @

  load_users: ->
    @collection.state.currentPage = 1
    @$users_list_element.empty()
    @collection.fetch
      data: @params
      success: =>
        @scrollNext false
    @

  load_more_users: ->
    return if !@collection.hasNextPage() or @collection.length < 12
    @collection.getNextPage
      data: @params
    @

  scrollNext: (scroll = true) ->
    return if @content_width < @page_width
    @page_width = @$users_content_element.width()
    current_offset = parseInt @$users_list_element.css 'margin-left'

    current_offset -= @page_width
    current_offset = -(@content_width - @page_width) if current_offset < -(@content_width - @page_width)

    if @collection.state.currentPage < @collection.state.lastPage
      @collection.getNextPage
        data: @params

    if scroll
      @$users_list_element.css
        marginLeft: current_offset
    @

  scrollPrev: ->
    @page_width = @$users_content_element.width()
    current_offset = parseInt @$users_list_element.css 'margin-left'

    current_offset += @page_width
    current_offset = 0 if current_offset > 0

    @$users_list_element.css
      marginLeft: current_offset
    @

