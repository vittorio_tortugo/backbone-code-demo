class Stoege.Views.Application extends Backbone.View

  error_note_template: JST["backbone/templates/error_note"]
  tooltip_note_template: JST["backbone/templates/tooltip_note"]

  show_submit_spinner: ($submit_button) ->
    $inputs = @$('input, select, textarea, .custom_select')
    # если были задизейбленные поля, то помечаем их атрибутом
    $inputs.each ->
      if $(@).prop('disabled')
        $(@).attr('data-disabled', true)
    $inputs.prop('disabled', true).addClass('disabled')
    $submit_button ||= @$('input[type="submit"]')
    $submit_button.addClass('disabled waiting')

  hide_submit_spinner: ($submit_button) ->
    @$('input:not([data-disabled="true"]), select, textarea, .custom_select').prop('disabled', false).removeClass('disabled')
    $submit_button ||= @$('input[type="submit"]')
    $submit_button.removeClass('disabled waiting')

  show_error: ($field, $wrap, text = '') ->
    return if $field.attr('disabled')
    $error_note = $wrap.find('.error_note_wrapper')

    if $error_note.length
      $error_note.find('.error_note_text').html(text)
    else
      $error = $(@error_note_template(text: text))
      $wrap.append($error)
      $wrap.one 'keydown', ->
        $error.remove()
        $field.removeClass('error')
      $field.addClass('error')

  remove_error: (e) ->
    $field = $(e.target)
    return if $field.attr('disabled')

    $field.removeClass('error')
          .parent().find('.error_note_wrapper')
          .remove()

  show_tooltip: ($field, $wrap, text = '') ->
    return if $field.attr('disabled')
    $tooltip_note = $wrap.find('.tooltip_note_wrapper')

    if $tooltip_note.length
      $tooltip_note.find('.tooltip_note_text').html(text)
    else
      $tooltip = $(@tooltip_note_template(text: text))
      $wrap.append($tooltip)
      $wrap.one 'keydown', ->
        $tooltip.remove()
        $field.removeClass('tooltip')
      $field.addClass('tooltip')

  remove_tooltip: (e) ->
    $field = $(e.target)
    return if $field.attr('disabled')

    $field.removeClass('tooltip')
          .parent().find('.tooltip_note_wrapper')
          .remove()