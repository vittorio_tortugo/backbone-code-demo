class Stoege.Views.EmailSubscription extends Stoege.Views.Application

  el: '.email_subscription_wrapper'

  events:
    'click .close_button' : 'close'

  initialize: ->
    @open()

  open: ->
    #12 hours = 43200
    #72 hours = 25920000

    if $.cookie('email_subscription_date')?
      time = parseInt(new Date() - new Date($.cookie('email_subscription_date')))/1000

      switch (parseInt($.cookie('email_subscription_count')))
        when 1
          if time >= 43200
            @fetch_model()
            @set_cookie(new Date(), 1)
        when 2
          if time >= 25920000
            @fetch_model()
            @set_cookie(null, 2)

    else
      @fetch_model()
      @set_cookie(new Date(), 1)
  
  fetch_model: ->
    @model = new Stoege.Models.DialogModel
    @model.url = '/subscribers/new'
    @model.fetch()
    
    @listenTo(@model, 'sync', @open_subscription, @)

  open_subscription: =>
    $('.form').html(@model.get('content'))
    @initialize_subscription_view()
    setTimeout @animate_subscription, 30000
    
  animate_subscription: ->
    $('.email_subscription_wrapper').toggleClass('active')

    if $('.email_subscription_wrapper').hasClass('active')
      dataLayer?.push({'event':'subscription_teaser_show', 'email_subscription_date': $.cookie('email_subscription_date'), 'email_subscription_count': $.cookie('email_subscription_count')})
  
  shut_down_subscription: ->
    email = $('.email_subscription #subscriber_email').val()

    $.cookie('email_subscription_count', 3, { path: '/', expires: 20000 })
    dataLayer?.push({'event':'subscription_teaser_subscribed', 'subscriber_email': email})

  close: ->  
    @animate_subscription()

    if $.cookie('email_subscription_count')?
      switch (parseInt($.cookie('email_subscription_count')))
        when 1
          @set_cookie(new Date(), 2)
        when 2
          @set_cookie(null, 3)
    else
      @set_cookie(null, 2)

    dataLayer.push({'event':'subscription_teaser_closed'}) if dataLayer?

  set_cookie: (date, count) ->
    $.cookie('email_subscription_date', date, { path: '/', expires: 20000 }) if date?
    $.cookie('email_subscription_count', count, { path: '/', expires: 20000 }) if count?

  initialize_subscription_view: ->
    subscription_view = new Stoege.Views.Subscription()
    @listenTo(subscription_view, 'subscription_success', @animate_subscription, @)
    @listenTo(subscription_view, 'subscription_success', @shut_down_subscription, @)