class Stoege.Views.GlobalView extends Backbone.View
  el: 'body'

  events: ->
    'click .navigation_link' : 'navigate_to_route'
    'click .fxf_navigation_link' : 'navigate_to_route'
    'click .authorization_link' : 'open_authorization_window'
    'click .forgot_password_link' : 'open_forgot_password_window'
    'click .feedback_link' : 'open_feedback'
    'click #footer .apps_links li, .about_wrapper .download_menu li, .uchebnik_wrapper .stores > li' : 'set_gtm_apps_links'
    'click .authorization_dialog .social_button' : 'send_social_google_tag_manager'
    'click': 'hide_main_menu'

  initialize: ->
    window.app_options or= {}
    @initialize_user()

    # fix for Facebook Change in Session Redirect Behavior
    window.location.hash = '' if window.location.hash is '#_=_'
    @set_notification_collection()
    @show_flash_notice()
    MathJax.Hub.Config() if MathJax?

    @set_polyglot()
    @set_you_text()
    $('input[placeholder], textarea[placeholder]').placeholder()

  initialize_user: ->
    window.user = user = new Stoege.Models.User
    @listenTo user, 'sync', ->
      delete app_options.user
    user.fetch()

  set_notification_collection: ->
    window.notificationCollection = new Stoege.Collections.Notification()
    notificationsView = new Stoege.Views.Notifications(collection: notificationCollection)

  show_flash_notice: ->
    if app_options.flash_alert
      notificationCollection.add
        content: app_options.flash_alert,
        status: 'alert'
    if app_options.flash_warning
      notificationCollection.add
        content: app_options.flash_warning,
        status: 'warning'
    if app_options.flash_notice
      notificationCollection.add
        content: app_options.flash_notice,
        status: 'notice'

  open_authorization_window: (e) ->
    @authorization_dialog_model or= new Stoege.Models.AuthorizationDialogModel
    @authorization_dialog_view or= new Stoege.Views.AuthorizationDialogView(model: @authorization_dialog_model)

    @set_after_sign_in_path($(e.currentTarget))

    @authorization_dialog_view.open_authorization()

  open_forgot_password_window: (e) ->
    unless  @forgot_password_dialog_model
      @forgot_password_dialog_model = new Stoege.Models.ForgotPasswordDialogModel
      @forgot_password_dialog_view = new Stoege.Views.ForgotPasswordDialogView(model: @forgot_password_dialog_model)

    @close_dialogs()
    @forgot_password_dialog_view.open_forgot_password()

  close_dialogs: ->
    $('.ui-dialog-content').dialog('close')

  navigate_to_route: (e) ->
    e.preventDefault()
    $target = $(e.currentTarget)
    path = $target.data('url')

    # возвращаемся на текущую страницу после регистрации
    if $target.hasClass('registration')
      @set_registration_page_path_to_cookies()
      path = @registration_path(path)

    # переходим по ссылке после регистрации/авторизации
    @set_after_sign_in_path($target)

    # напоминаем подтвердить email, если не подтверждённый email
    if $target.hasClass('email_confirmation_required')
      @show_confirmation_message()
    else
      Backbone.history.navigate(path, true)

  set_after_sign_in_path: ($target) ->
    return if $target.hasClass('from_registration')
    if $target.hasClass('after_sign_in_path')
      current_path = $target.attr('href')
      $.cookie('after_sign_in_path', current_path, { path: '/' })
    else
      $.removeCookie('after_sign_in_path', { path: '/' })

  registration_path: (target_path) ->
    params = Backbone.History.prototype.getQueryParameters(window.location.search)
    registration_params = Backbone.History.prototype.getQueryParameters(target_path)
    $.extend(registration_params, params)

    "#{@root_striped_path()}?#{$.param(registration_params)}"

  #FIX_ME same method in athorization dialog view
  root_striped_path: ->
     root_regexp = new RegExp("^#{Backbone.history.root}")
     Backbone.history.location.pathname.replace(root_regexp, '')

  show_confirmation_message: ->
    window.notificationCollection.add
      content: "#{you_text('Подтвердите', 'Подтверди')} свой email, кликнув по ссылке в письме-подтверждении. <a class='email_confirmation_link blue_button'> Прислать мне письмо повторно </a>"
      status: 'warning'

  set_registration_page_path_to_cookies: (current_path=null) ->
    current_path ?= location.pathname + location.search
    $.cookie('registration_page', current_path, { path: '/' })

  open_feedback: ->
    feedback_dialog_view = new Stoege.Views.FeedbackDialogView
    feedback_dialog_view.open_feedback()

  set_polyglot: ->
    window.polyglot = new Polyglot(locale: 'ru')
    polyglot.extend
      remaining_months: '%{smart_count} месяц |||| %{smart_count} месяца |||| %{smart_count} месяцев'
      points: 'балл |||| балла |||| баллов'
      themes: '%{smart_count} тема |||| %{smart_count} темы |||| %{smart_count} тем'
      pupils: '%{smart_count} учащийся |||| %{smart_count} учащихся |||| %{smart_count} учащихся'
      lessons: '%{smart_count} занятие |||| %{smart_count} занятия |||| %{smart_count} занятий'
      hours: 'час |||| часа |||| часов'
      conspects: 'конспект |||| конспекта |||| конспектов'
      tasks: 'задача |||| задачи |||| задач'
      exercises: 'занятие |||| занятия |||| занятий'
      interective: 'интерактивная |||| интерактивные |||| интерактивных'
      academic: 'академический |||| академических |||| академических'
      electronic: 'электронный |||| электронных |||| электронных'
      during_next: 'следующего |||| следующих |||| следующих'
      during_months: 'месяца |||| месяцев |||| месяцев'
      members: 'участник |||| участника |||| участников'

  set_gtm_apps_links: (e) ->
    $target = $(e.target).closest('li')
    target_class = $target.attr('class')
    target_parent = $target.closest('ul').attr('class')

    platform = switch target_class
      when 'app_store' then 'ios'
      when 'google_play' then 'android'
      when 'window_store' then 'windows'

    source = switch target_parent
      when 'apps_links' then 'footer'
      when 'download_menu' then 'wiki'
      when 'stores' then 'uchebnik'

    dataLayer?.push('event':'textbook_app_link_clicked','platform': platform ,click_source: source)

  send_social_google_tag_manager: (e) ->
    social_type = $(e.target).closest('.social_button').data('type')

    dataLayer?.push('event':'signup_social_pressed', 'signup_type': social_type)

  set_you_text: ->
    window.you_text = (teacher_text, pupil_text) ->
      if user.is_teacher() then teacher_text else pupil_text

  hide_main_menu: ->
    @$('.menu').removeClass('active')
