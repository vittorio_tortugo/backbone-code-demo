class Stoege.Views.SocialButtonsView extends Backbone.View
  initialize: ()->
    _gaq = [["_setAccount", "UA-XXXXXXX-X"], ["_trackPageview"]]

    # Переменная для указания языка для отображения виджетов Google+
    window.___gcfg = lang: "ru"

    # Инициализация ВКонтакте
    window.vkAsyncInit = ->
      VK.init
        apiId: 3254220
        onlyWidgets: true
      VK.Widgets.Like "vk_like",
        type: "mini"
        height: 18
        pageUrl: "http://foxford.ru/"

    # Инициализация facebook
    window.fbAsyncInit = ->
      FB.init
        status: true
        cookie: true
        xfbml: true
        oauth: true

    # Функция асинхронной загрузки      
    ((a, c, f) ->
      g = ->
        d = undefined
        a = c.getElementsByTagName(f)[0]
        b = (b, e) ->
          c.getElementById(e) or (d = c.createElement(f)
          d.src = b
          d.async = not 0
          e and (d.id = e)
          a.parentNode.insertBefore(d, a)
          )

        b "//connect.facebook.net/en_US/all.js#xfbml=1", "facebook-jssdk"
        b "https://platform.twitter.com/widgets.js", "twitter-wjs"
        b "https://apis.google.com/js/plusone.js"
        b "//vk.com/js/api/openapi.js"
        b "//cdn.connect.mail.ru/js/loader.js"
        b "//surfingbird.ru/share/share.min.js"
        b "//assets.pinterest.com/js/pinit.js"
        b ((if "https:" is c.location.protocol then "https://ssl" else "http://www")) + ".google-analytics.com/ga.js"
      (if a.addEventListener then a.addEventListener("load", g, not 1) else a.attachEvent and a.attachEvent("onload", g))
    ) window, document, "script"