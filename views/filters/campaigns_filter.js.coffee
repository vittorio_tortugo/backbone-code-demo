class Stoege.Views.CampaignsFilter extends Backbone.View

  el: ".course_campaign_filter"

  events:
    'click .close_button' : 'remove_filter'

  initialize: ({@params, @root_name}) ->

  remove_filter: ->
    delete @params.campaign_id
    delete @params.page if @params.page
    @$el.remove()

    Backbone.history.navigate("/#{@root_name}?#{$.param(@params)}", true)

  remove_page_param: ->
    delete @params.page if @params.page
