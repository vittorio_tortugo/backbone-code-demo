class Stoege.Views.LibraryFilter extends Backbone.View
  el: '.fxf_library_filter'

  cities_template: JST['backbone/templates/filters/cities']

  events:
    'change select': 'select_change'

  initialize: ({@params, @root_name, @collection_seasons, @collection_grades, @collection_disciplines, @collection_user_types, @collection_levels, @collection_types, @collection_series, @collection_states, @collection_regions, @collection_cities}) ->
    @parse_filter_elements()
    @set_default_filters_value()
    $('#filter_types').children("option[hidden='hidden']").detach()
    @initialize_custom_select()
    @set_and_render_filters()
    @set_filters_for_teacher() if user.is_teacher()

    # TODO аккуратно выпилить, используется в библиотеке курсов и открытых занятиях
    @filter_type = new Backbone.Model type: 'cards'

    @render_cities(@params.region_id)
    @

  initialize_custom_select: ->
    @$('.fxf_select select').select2
      formatSelection: (data) ->
        data_selected = $(data.element).data('selected') or data.text
        return data_selected
      containerCssClass: 'filter_select'
      dropdownCssClass: 'filter_select'

  set_default_filters_value: ->
    # TODO Отрефакторить
    if @collection_grades and not @params.grade_id
      grade_id = user.get 'actual_grade_id'
      if grade_id? and not user.is_teacher() and @params.course_type_id isnt Stoege.Constants.COURSE_TYPES.KPK
        @params.grade_id = grade_id
        @collection_grades.set_selected_by(@params.grade_id)

    if @collection_seasons
      season_id = app_options?.active_season_id or app_options?.newest_season_id
      if season_id
        season_id = parseInt(season_id)
        @params.season_id = season_id
        @collection_seasons.set_selected_by(@params.season_id)

    if @collection_regions
      region_id = app_options?.region_id
      if region_id
        region_id = parseInt(region_id)
        @params.region_id = region_id
        @collection_regions.set_selected_by(@params.region_id)

    if @collection_cities
      city_id = app_options?.city_id
      if city_id
        city_id = parseInt(city_id)
        @params.city_id = city_id
        @collection_regions.set_selected_by(@params.city_id)


    if @collection_user_types and @collection_types
      if user.is_teacher() or @params.course_type_id is Stoege.Constants.COURSE_TYPES.KPK
        @params.course_type_id = Stoege.Constants.COURSE_TYPES.KPK
        @params.user_types_id = Stoege.Constants.USER_TYPES.TEACHER
        @collection_types.set_selected_by @params.course_type_id
      else
        @params.user_types_id = Stoege.Constants.USER_TYPES.PUPIL
      @collection_user_types.set_selected_by @params.user_types_id

  reset_default_filters_value: ->
    @collection_grades.disable_selected() if @collection_grades
    @collection_user_types.disable_selected() if @collection_user_types
    @collection_seasons.disable_selected() if @collection_seasons

  parse_filter_elements: ->
    @$filter_elements_grades = @$('select[name=filter_grades]')
    @$filter_element_disciplines = @$('select[name=filter_disciplines]')
    @$filter_element_user_types = @$('select[name=filter_user_types]')
    @$filter_element_series = @$('select[name=filter_series ]')
    @$filter_element_states = @$('select[name=filter_states ]')
    @$filter_element_levels = @$('select[name=filter_levels ]')
    @$filter_element_types = @$('select[name=filter_types ]')
    @$filter_element_seasons = @$('select[name=filter_seasons ]')
    @$filter_element_regions = @$('select[name=filter_regions ]')
    @$filter_element_cities = @$('select[name=filter_cities ]')

  reset: ->
    # TODO Отрефакторить
    grade_id = if @params.course_type_id is Stoege.Constants.COURSE_TYPES.KPK then user.get('actual_grade_id') else 0
    @set_grade(grade_id, false)
    @set_disciplines(0, false)
    @set_states(0, false)
    @set_series(0, false)
    @set_levels(0, false)
    @set_types(0, false)
    @set_seasons(0, false)
    @set_regions(0, false)
    @$el.find('select').prop('selectedIndex', 0)
    Backbone.history.navigate("/#{@root_name}", true)
    @

  set_filters_for_teacher: ->
    $('#s2id_filter_types').parent().toggleClass('hidden')
    $('#s2id_filter_grades').parent().toggleClass('hidden')
    $('#s2id_filter_levels').parent().toggleClass('hidden')

  select_change: (event) ->
    $target = $(event.target)
    target_value = $target.val()
    switch $target.prop 'name'
      when 'filter_grades' then @set_grade target_value
      when 'filter_disciplines' then @set_disciplines target_value
      when 'filter_user_types' then @set_user_types target_value
      when 'filter_series' then @set_series target_value
      when 'filter_states' then @set_states target_value
      when 'filter_levels' then @set_levels target_value
      when 'filter_types' then @set_types target_value
      when 'filter_seasons' then @set_seasons target_value
      when 'filter_regions' then @set_regions target_value
      when 'filter_cities' then @set_cities target_value
    @

  set_grade: (grade_id, silent = true) ->
    return unless @collection_grades?

    @set_filter @collection_grades, grade_id, 'grade_id', silent
    @set_active_select_color(grade_id, $('#s2id_filter_grades'))

  set_disciplines: (discipline_id, silent = true) ->
    return unless @collection_disciplines?

    @set_filter @collection_disciplines, discipline_id, 'discipline_id', silent
    @set_active_select_color(discipline_id, $('#s2id_filter_disciplines'))

  set_user_types: (user_types_id, silent = true) ->
    return unless @collection_user_types?

    @set_filters_for_teacher()
    @reset()
    if @params.course_type_id is Stoege.Constants.COURSE_TYPES.KPK
      @set_default_filters_value()
    else
      @reset_default_filters_value()

    @set_filter @collection_user_types, user_types_id, 'user_types_id', silent

  set_series: (series_id, silent = true) ->
    return unless @collection_series?

    @set_filter @collection_series, series_id, 'series_id', silent
    @set_active_select_color(series_id, $('#s2id_filter_series'))

  set_states: (state_id, silent = true) ->
    return unless @collection_states?

    @set_filter @collection_states, state_id, 'state', silent
    @set_active_select_color(state_id, $('#s2id_filter_states'))

  set_levels: (level_id, silent = true) ->
    return unless @collection_levels?

    @set_filter @collection_levels, level_id, 'level_id', silent
    @set_active_select_color(level_id, $('#s2id_filter_levels'))

  set_types: (course_type_id, silent = true) ->
    return unless @collection_types?

    @set_filter @collection_types, course_type_id, 'course_type_id', silent
    @set_active_select_color(course_type_id, $('#s2id_filter_types'))

  set_seasons: (season_id, silent = true) ->
    return unless @collection_seasons?

    @set_filter @collection_seasons, season_id, 'season_id', silent
    @set_active_select_color(season_id, $('#s2id_filter_seasons'))

  set_regions: (region_id, silent = true) ->
    return unless @collection_regions?

    @set_filter @collection_regions, region_id, 'region_id', silent
    @set_filter @collection_cities, 0, 'city_id', silent

    @set_active_select_color(region_id, $('#s2id_filter_regions'))

    @render_cities(parseInt(region_id))

  set_cities: (city_id, silent = true) ->
    return unless @collection_cities?

    @set_filter @collection_cities, city_id, 'city_id', silent
    @set_active_select_color(city_id, $('#s2id_filter_cities'))

  set_active_select_color: (id, $value) ->
    $value.toggleClass('red', not (id is '0'))

  set_filter: (collection, item_id = 0, param_name, silent) ->
    item_id = parseInt(item_id) if item_id is '0'

    if item_id is 0
      delete @params[param_name]
    else
      @params[param_name] = item_id

    @remove_page_param()
    @set_and_render_filters()

    if param_name is 'user_types_id'
      $type_filter = $('#s2id_filter_types').parent()
      if item_id is 0
        @set_types(3, true) if @collection_types?
        $type_filter.hide()
      else
        $type_filter.show()
        delete @params[param_name]
        
    Backbone.history.navigate("/#{@root_name}?#{$.param(@params)}", silent)

  remove_page_param: ->
    delete @params.page if @params.page

  select_filter_item: (collection, param_name, $filter_element) =>
    return unless collection?

    $filter_element.val collection.get_id_of_selected() if collection.change_selected_on(@params[param_name])

    $select_chosen = $filter_element.parent().find('.select2-chosen')
    $active_option = $filter_element.children('option').filter(':selected')

    $select_chosen.html($active_option.data("selected") or $active_option.text())
    @set_active_select_color($active_option.val(), $select_chosen) if param_name isnt "user_types_id"

  set_and_render_filters: ->
    @select_filter_item @collection_grades, 'grade_id', @$filter_elements_grades
    @select_filter_item @collection_disciplines, 'discipline_id', @$filter_element_disciplines
    @select_filter_item @collection_user_types, 'user_types_id', @$filter_element_user_types
    @select_filter_item @collection_series, 'series_id', @$filter_element_series
    @select_filter_item @collection_states, 'state', @$filter_element_states
    @select_filter_item @collection_levels, 'level_id', @$filter_element_levels
    @select_filter_item @collection_types, 'course_type_id', @$filter_element_types
    @select_filter_item @collection_seasons, 'season_id', @$filter_element_seasons
    @select_filter_item @collection_regions, 'region_id', @$filter_element_regions
    @

  render_cities: (region_id) ->
    if region_id? and region_id isnt 0
      $('#s2id_filter_cities').fadeIn('fast')

      region_id = parseInt(region_id)   
      @collection_cities.fetch
        data:
          region_id: region_id

      @listenToOnce @collection_cities, 'sync', =>
        $('#filter_cities').html(@cities_template(cities: @collection_cities.toJSON()))

        @select_filter_item @collection_cities, 'city_id', @$filter_element_cities

    else
      $('#s2id_filter_cities').hide('fast')
