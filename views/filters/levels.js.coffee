class Stoege.Views.LevelsFilter extends Backbone.View

  el: '.levels'

  template: JST["backbone/templates/course/level_filter"]

  events:
    'click li:not(.selected)' : 'set_level'

  initialize: ({@params, @root_name, @param_name}) ->
    @listenTo(@collection, 'change:selected', @render, @)
    @set_menu()

  set_level: (e)->
    level_id = $(e.target).closest('li').data('id')    
    level = @collection.get(level_id)
    active_level = @collection.where(selected: true)[0]

    if level.get('selected')
      level.set(selected: false) 
    else 
      active_level.set(selected: false) if active_level?
      level.set(selected: true)   

    if level_id == 0
      delete @params["level_id"]
    else
      @params["level_id"] = level_id

    @remove_page_param()
    Backbone.history.navigate("/#{@root_name}?#{$.param(@params)}", true)
    
  render: (model) ->
    @$el.html(@template(levels: @collection.toJSON()))
    @

  remove_page_param: ->
    delete @params['page'] if @params['page']

  set_menu: =>
    level = @collection.get(@params["level_id"]) || @collection.get(0)
    @collection.where(selected: true)[0].set(selected: false) if @collection.where(selected: true)[0]?
    level.set(selected: true)
