class Stoege.Views.GradesFilter extends Backbone.View

  el: "#filter_grades"

  events:
    'change' : 'set_grade'

  initialize: ({@params, @root_name}) ->
    $('#filter_grades').customSelect(customClass: "custom_select")   
    @set_menu()

  set_grade: (e) ->
    grade_id = $(e.target).find(':selected').val()
    grade = @collection.get(grade_id)
    active_grade = @collection.findWhere(selected: true)

    if grade.get('selected')
      grade.set(selected: false) 
    else 
      active_grade?.set(selected: false)
      grade.set(selected: true)

    if grade_id is '0'
      delete @params.grade_id
    else
      @params.grade_id = grade_id

    @remove_page_param()
    Backbone.history.navigate("/#{@root_name}?#{$.param(@params)}", true)

  remove_page_param: ->
    delete @params.page if @params.page

  set_menu: =>
    user_grade_id = user.get 'grade_id'
    return unless user_grade_id

    @params.grade_id ?= user_grade_id
    grade_model = @collection.get(@params.grade_id)
    return unless grade_model?

    @collection.findWhere(selected: true)?.set(selected: false)
    grade_model.set(selected: true)

    $('#filter_grades').val(@params.grade_id)
    $('.filter_select .custom_selectInner').html(grade_model.get('text'))
