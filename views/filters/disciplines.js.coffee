# TODO проверить, что эта штука где то используется

class Stoege.Views.DisciplinesFilter extends Backbone.View

  el: '.disciplines'

  template: JST["backbone/templates/course/discipline_filter"]

  events:
    'click li:not(.selected)': 'set_disciplines'

  initialize: ({@params, @root_name}) ->
    @listenTo(@collection, 'change:selected', @render, @)
    @set_menu()

  set_disciplines: (e) ->
    discipline_id = $(e.target).closest('li').data('id')
    discipline = @collection.get("#{discipline_id}")
    active_discipline = @collection.where(selected: true)[0]

    if discipline.get('selected')
      discipline.set(selected: false)
    else
      active_discipline.set(selected: false) if active_discipline?
      discipline.set(selected: true)

    if discipline_id == 0
      delete @params["discipline_id"]
    else
      @params["discipline_id"] = discipline_id

    @remove_page_param()
    Backbone.history.navigate("/#{@root_name}?#{$.param(@params)}", true)

  render: ->
    @$el.html(@template(disciplines: @collection.toJSON()))
    @

  set_menu: =>
    discipline = @collection.get(@params["discipline_id"]) || @collection.get(0)
    @collection.where(selected: true)[0].set(selected: false) if @collection.where(selected: true)[0]?
    discipline.set(selected: true)

  remove_page_param: ->
    delete @params['page'] if @params['page']

  remove_view: ->
    @undelegateEvents()
    @stopListening(@collection, 'change:selected')
