class Stoege.Views.PageView extends Backbone.View
  el: '.page_wrapper'
  
  page_template: JST["backbone/templates/page"]
  
  events:
    'click .page_menu li a' : 'click_menu_element'
    'click .task_arrows a' : 'click_menu_element'
    'click .page_content_nav li a' : 'click_menu_element'
    'click .toggle_header, .toggle_arrow' : 'toggle_content'

  initialize: ->
    if $('.page_wrapper').length
      if $('.page_wrapper').hasClass('task_wrapper')
        @task_page = true

      @insert_breadcrumbs('active')
      @show_subsections()
      @spinner_height()

      @if_ie = (navigator.userAgent.indexOf('MSIE') isnt -1)
      @ie_version = parseInt(navigator.userAgent.substr(navigator.userAgent.indexOf('MSIE') + 5, 3)) or 0
      MathJax.Hub.Queue ["Typeset", MathJax.Hub, 'mathjax_block']

      $(window).resize =>
        @spinner_height()
  
  # располагаю спинер на пол высоты экрана
  spinner_height: ->
    spinner_top = $(window).height()/2 - 130
    $('#page_spinner img').css('top', spinner_top)

  # устанавливаю высоту загружаемого контента
  set_content_height: (state, add_height, $block = $('.page_content_main')) =>
    @change_content_height(state, add_height)
    $block.imagesLoaded ()=>
      @change_content_height(state, add_height)

  show_subsections: ->
    $active_element = $('.page_menu a.active')
    $active_element_parent = $active_element.parent('li')
    $opened_sections = $('.page_menu li.opened')
    $subsection = $active_element_parent.find('li')

    # показываю подразделы при прямом переходе по url
    if $active_element_parent.hasClass('page_menu_subsection') and $active_element.closest('.page_menu_section').hasClass('closed')
      @toggle_section($active_element.closest('.page_menu_section'))
      @toggle_section($opened_sections)

    # показываю подразделы при клике на секцию
    else if $active_element_parent.hasClass('page_menu_section')
      if $active_element_parent.hasClass('closed') && $subsection.length
        @toggle_section($active_element_parent)
        @toggle_section($opened_sections)
      else if $subsection.length is 0
        @toggle_section($opened_sections)
    else if $active_element.length is 0
      @toggle_section($opened_sections)

  toggle_section: (section)=>
    section.children('ul').slideToggle('300')
    section.toggleClass('opened closed')

  # получаю url страницы при клике на элементах бокового и верхнего меню
  click_menu_element: (e)=>
    if not (@if_ie and @ie_version <= 9)
      $link = $(e.target)
      if $link.is('span')
        $link = $link.closest('a')

      if !$link.hasClass('active')
        $('#page_spinner').show()
        path = $link.attr('href')
        Backbone.history.navigate(path, true)

      return false

  set_active_class: (path) =>
    $('.page_menu li a.active').removeClass('active')
    $('.page_menu li a[href="' + path + '"]').addClass('active')

  show_page: (path) ->
    if $('.page_wrapper').hasClass('task_wrapper')
      @task_page = true

    if $('.page_wrapper').hasClass('conspect_wrapper')
      @conspect_page = true

    # если это task, то делаем js запрос
    data_type = if @task_page then '.js' else '.json'
    $.get "#{path}#{data_type}", (data) =>
      unless @task_page
        template = @page_template
          conspect_page: @conspect_page
          course: data.course
          discipline: data.discipline
          header: data.name
          content: data.content
          discipline_sections: data.discipline_sections
        $('.page_wrapper .page_content_passive').html(template)

        title = if data.title is '' then 'Центр онлайн-обучения 100EGE.ru' else data.title
        $('title').text(title)

      @set_active_class(path)
      @show_subsections()
      #@insert_breadcrumbs(if data.breadcrumbs then 'passive' else 'active')
      $('#page_spinner').hide()
      @page_animation()

  # анимация fade страниц
  page_animation: ->
    $page_content_wrapper = $('.page_content_wrapper')
    $('.page_content_active').fadeOut 200, =>
      $('.page_content_active').remove()
      $('.page_content_passive').removeClass('page_content_passive').addClass('page_content_active').fadeIn 200, =>
        $('<div class="page_content_passive"></div>').insertAfter($page_content_wrapper.find('.page_content_active'))
        $('.page_content_passive').css('display', 'none')
        MathJax.Hub.Queue(
          ["Typeset", MathJax.Hub, 'mathjax_block']
        )
        $('.page_content').trigger(jQuery.Event("show_page"))

  # верхнее меню (хлебные крошки)
  insert_breadcrumbs: (state)=>
    # если это не страница задач

    unless @task_page or @conspect_page
      url = window.location.pathname.split('/').pop(-1)
      $.get url + '.json', (data, status, xhr) =>
        li = ''
        count = 1
        if data.breadcrumbs?
          $.each data.breadcrumbs, (i, val) ->
            if val.menu_name != null
              li += '<li class="page_nav_arrow"></li>' if count isnt 1
              li += '<li><a href="/' + val.url + '">' + val.menu_name + '</a></li>'
            count += 1
          $('.page_content_' + state +  ' .page_content_nav').html(li)
        else
          $('.page_content_nav').html('<li>'+$('.hidden_title').text()+'</li>')

  arrow_left_pos: =>
    return if @task_page

    scroll_left = $(window).scrollLeft()
    left_pos = parseInt($('.page_arrow').offset().left - scroll_left)
    $('.page_wrapper .page_arrow_pic').css('left', left_pos + 20)

    false
  
  toggle_content: (e)=>
    $target = $(e.target)
    $arrow = $target.closest('.toggle_element').find('.toggle_arrow')

    $target.closest('.toggle_element').children('.toggle_content').slideToggle(300)
    $arrow.toggleClass("down")

    # меняю высоту контента после сворачивания
    #if $arrow.hasClass("down")
    #  $target.closest('.toggle_element').children('.toggle_content').slideUp(300, ()=>
    #    $arrow.toggleClass("down")
    #  )
    # меняю высоту контента до раскрытия
    #else
    #  $target.closest('.toggle_element').children('.toggle_content').slideDown(300, ()=>
    #    $arrow.toggleClass("down")
    #  )

