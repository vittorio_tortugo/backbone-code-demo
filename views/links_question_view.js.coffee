class Stoege.Views.LinksQuestionView extends Backbone.View

  el: '.links_block'

  initialize: =>
    @make_answers_draggable($("ul.links_collection_second li"))
    @add_hover_class($("ul.links_collection_second li"))
    @make_fields_droppable($("ul.links_collection_first li .field"))
    @min_field_height = 30

  add_hover_class: ($list) ->
    $list.hover(
      -> $(@).find('.answer_layer').addClass('hover_in')
      -> $(@).find('.answer_layer').removeClass('hover_in')
    )

  # ответы из первоначального списка
  make_answers_draggable: ($list) ->
    $list.each ()->
      $(@).draggable
        revert: "invalid"
        zIndex: 10
        opacity: 0.7
        containment: $(@).closest('.task_questions_links')
        helper: "clone"

  # выбранный ответ
  make_answer_draggable: ($answer) ->
    $answer.draggable
      zIndex: 10
      opacity: 0.7
      containment: $answer.closest('.task_questions_links')
      helper: "clone"
      start: (e, ui)=>
        $(e.target).hide()
        $droppable_field = $(e.target).siblings('.field')
        $droppable_field.show().height(@min_field_height)

        $input = $droppable_field.siblings('input.linked_answer')
        $input.val('')
        @make_fields_droppable($droppable_field)
      stop: (e, ui)=>
        $(e.target).remove()

  make_fields_droppable: ($list) ->
    $list.droppable
      hoverClass: "drop"
      accept: "ul.links_collection_second li, ul.links_collection_first li .answer"
      tolerance: "pointer"
      over: (e, ui)=>
        $draggable_answer = ui.draggable
        answer_height = $draggable_answer.outerHeight()
        $droppable_field = $(e.target)
        droppable_field_height = $droppable_field.outerHeight()

        # если droppable является сам ответ
        if $droppable_field.hasClass('answer')
          $droppable_field.find('.answer_content').hide()
          if answer_height > droppable_field_height
            $droppable_field.height(answer_height - 24)
          else
            $droppable_field.height(droppable_field_height - 24)

        else
          $droppable_field.height(answer_height - 2)

        if answer_height > $droppable_field.parent().height()
          $droppable_field.parent().height(answer_height)
        else
          $droppable_field.parent().height('auto')

      out: (e, ui)=>
        answer_height = ui.draggable.outerHeight()
        $droppable_field = $(e.target)
        $question = $droppable_field.siblings('.text')

        # если droppable является сам ответ
        if $droppable_field.hasClass('answer')
          $droppable_field.find('.answer_content').show()
          $droppable_field.height('auto')
        else
          $droppable_field.height(@min_field_height)

        if answer_height > $question.height()
          $droppable_field.parent().height('auto')

      drop: (e, ui) =>
        $draggable_answer = ui.draggable
        simple_answer_id = $draggable_answer.attr('data-simple_answer_id')

        $droppable_field = $(e.target)
        draggable_html = $draggable_answer.html()

        $input = $droppable_field.siblings('input.linked_answer')
        $input.val(simple_answer_id)

        $answer_row = $droppable_field.parent()

        # вставляю новый ответ
        $answer_row.find('.text').after("<div class='answer' data-simple_answer_id='#{simple_answer_id}'>#{draggable_html}</div>")
        $droppable_field.hide().droppable("destroy")
        $new_answer = $droppable_field.siblings('.answer')

        if $droppable_field.hasClass('answer')
          $droppable_field.remove()

        @make_answer_draggable($new_answer)
        # делаю ответы droppable
        @make_fields_droppable($new_answer)
        @add_hover_class($new_answer)

        @add_for_olympiads_linked_id($answer_row) if $('.olympiads').length

  #фикс для олимиад. замена value в инпуте.
  add_for_olympiads_linked_id: ($answer_row) ->
    $answer_row.find('.linked_answer').attr 'value', $answer_row.find('.answer').data('simple_answer_id')
    

