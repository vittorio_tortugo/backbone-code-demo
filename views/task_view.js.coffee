class Stoege.Views.TaskView extends Stoege.Views.Application

  el: '.task_wrapper'

  events:
    'click input.error, select.error' : 'remove_error'
    'click .page_content_active .task_questions_radio label' : 'set_radio_button'
    'click .page_content_active .task_questions_checkbox label' : 'set_checkbox_button'
    'click .page_content_active .task_hints .blue_button' : 'use_hints'
    'click .page_content_active .task_questions_text .text_add_field' : 'add_field'
    'click .page_content_active .task_hints .task_show_hints' : 'show_hidden_block'
    'click .page_content_active .task_solution_wrapper .task_show_solution' : 'show_hidden_block'
    'submit .page_content_active .edit_user_answer' : 'submit_form'
    'click .page_content_active .task_show_essay_answer .text' : 'toggle_block'
    'keyup .task_questions_essay textarea' : 'enable_submit_button'
    'change .task_questions_essay textarea' : 'enable_submit_button'
    'click .fail_button' : 'fail_task'

  toggle_element_template: JST["backbone/templates/toggle_element"]
  hint_template: JST["backbone/templates/hint"]
  points_template: JST["backbone/templates/points"]
  progress_bar_template: JST["backbone/templates/progress_bar"]
  solution_template: JST["backbone/templates/solution"]
  protocol_template: JST["backbone/templates/protocol"]
  text_answer_template: JST["backbone/templates/text_answer"]
  linked_answer_template: JST["backbone/templates/linked_answer"]
  essay_checking_template: JST["backbone/templates/essay_checking"]

  initialize: =>
    @file_uploaded = false

    @remove_radio_style()
    @upload_file()
    @set_custom_select()
    @set_textareasize()
    @update_progress()

    $progress_radial = @$('.progress_radial')
    @set_url_params
      only_progress: true,
      lesson:
        solved_percent: $progress_radial.data('solved_percent')
        progress_percent: $progress_radial.data('progress_percent')

    $('.page_content').on 'show_page', =>
      new Stoege.Views.LinksQuestionView()
      @upload_file()
      @set_custom_select()
      @set_textareasize()
      clearInterval(@check_proticol_interval)

  use_hints: (e) =>
    $target = $(e.target)
    return if $target.hasClass('disabled')
    url = $target.data('url')
    $.ajax
      url: url + ".json"
      type: "PUT"
      dataType: "json"
      data: { hint: true }
      beforeSend: =>
        @show_submit_spinner($target)
      success: (data) =>
        @hide_submit_spinner($target)
        @show_new_hint(data)

  upload_file: ->
    $page_content = $('.page_content_active')
    $user_answer_form = $page_content.find('.edit_user_answer')
    return unless $user_answer_form.find('.task_questions_programming').length

    $user_answer_form.fileupload
      add: (e, data) =>
        @file_uploaded = true
        $('.programming_file_name').text(data.files[0].name)

        if data.files[0].size > 20000
          @show_error($('.programming_file_upload input'), $('.programming_file') ,'Размер файла не должен превышать 20KB')

        $user_answer_form.find('input.blue_button').off 'click'
        $user_answer_form.find('input.blue_button').on 'click', =>
          data.submit()
            .error (xhr, status, error) =>
               json = $.parseJSON(xhr.responseText)
               $.each json, (field, text) =>
                 @show_error($("##{field}"), $("##{field}").closest('.select_programming_language, .programming_file'), text[0])
            .success (data, status, xhr) =>
              $user_answer_form.find('.blue_button').off 'click'
              $page_content.find('.task_hints_use').addClass('disabled')
              $page_content.find('.blue_button').addClass('disabled').attr('disabled', 'disabled')
              $('.task_questions_programming .programming_processing').removeClass('hidden_block')
              $('.task_questions_programming .programming_form').addClass('hidden_block')
              @check_protocol_status()

      done: (e, data) =>
        $('.programming_file_name').text('')
        @file_uploaded = false

  check_protocol_status: =>
    j = 0
    i = 1
    @check_proticol_interval = setInterval ( =>
      @get_protocol_status()
      j++
      if j <= 6
        i = 1
      else if j > 6 and j <=10
        i = 3
      else
        clearInterval(@check_proticol_interval)
    ), 5000*i

  get_protocol_status: =>
    $page_content = $('.page_content_active')
    $user_answer_form = $page_content.find('.edit_user_answer')
    $.ajax
      url: $user_answer_form.attr('action') + '.json'
      success: (data) =>
        unless data.processing
          clearInterval(@check_proticol_interval)

          if data.solved is null
            unless data.user_answers.status is 'error'
              @show_new_hint(data)
              new Stoege.Views.AlertView(message: data.message)

            $page_content.find('.task_hints_use').removeClass('disabled')
            $page_content.find('.disabled_button').addClass('blue_button').removeClass('disabled_button').attr('disabled', false)
            $('.task_questions_programming .programming_processing').addClass('hidden_block')
            $('.task_questions_programming .programming_form').removeClass('hidden_block')

          else if data.solved is true
            $('.task_questions_programming .programming_processing').remove()
            $('.task_questions_programming .programming_form').remove()
            @process_solved_true(data)

          else if data.solved is false
            $('.task_questions_programming .programming_processing').remove()
            $('.task_questions_programming .programming_form').remove()
            @process_solved_false(data)

          content = @protocol_template(data.user_answers)
          @insert_toggle_element($('.page_content_active .task_questions_programming_protocol'), 'Протокол проверки', content, true)

          @set_url_params(data)

  submit_form: (e) ->
    e.preventDefault()

    if @$('.task_questions_programming').length
      @show_error($('.select_programming_language select'), $('.select_programming_language') ,'Прикрепите исходный код программы') unless @file_uploaded
      return
    if @$('.essay').length
      @submit_essay_form()
    else
      @submit_task_form()

  submit_essay_form: ->
    $user_answer_form = @$('.edit_user_answer')
    $.ajax
      type: "PUT"
      url: $user_answer_form.attr('action')
      data: $user_answer_form.serialize()
      dataType: "json"
      beforeSend: =>
        @show_submit_spinner()
      success: (data) =>
        text = $user_answer_form.find('textarea').val().replace(/[\r\n]/g, '<br>')
        points = $('.task_points.answer .task_points_digit').text()
        $('.task_questions_block.essay').replaceWith(@essay_checking_template(text: text, days: data.checking_days_count, points: points, info_class: 'checking'))
        $('.task_questions_new_answer').remove()

  submit_task_form: ->
    $user_answer_form = @$('.edit_user_answer')
    $.ajax
      type: "POST"
      url: $user_answer_form.attr('action') + '.json'
      data: $user_answer_form.serialize()
      dataType: "json"
      beforeSend: =>
        @show_submit_spinner()
      success: (data) =>
        @hide_submit_spinner()
        if data.solved is null
          @show_new_hint(data)
          window.notificationCollection.add
            content: data.message
            status: 'alert'

        else if data.solved is false
          @show_correct_answers(data)
          @remove_radio_style()
          @process_solved_false(data)

        else if data.solved is true
          @show_correct_answers(data)
          @remove_radio_style()
          @process_solved_true(data)

        @set_url_params(data)

      error: (xhr, status, error) =>
        @hide_submit_spinner()
        errors = $.parseJSON(xhr.responseText)
        if errors?
          $.each errors, (i, message) =>
            if i == "base"
              window.notificationCollection.add
                content: errors.base[0]
                status: 'alert'
            else
              $text_input = $("input[id^='questions_#{i}']")
              @show_error($text_input, $text_input.parent(), message[0])

  fail_task: ->
    return if @$('.fail_button').hasClass('disabled')
    return unless confirm("#{you_text('Вы уверены', 'Ты уверен')}, что #{you_text('хотите', 'хочешь')} сдаться?")

    $.ajax
      type: "POST"
      url: @$('.fail_button').data('url')
      dataType: "json"
      success: (data) =>
        if data.solved is false
          @show_correct_answers(data)
          @$('.task_hints_block').remove()
          @process_solved_false(data)
          @remove_radio_style()
          @remove_droppable_links()
        @set_url_params(data)

  remove_droppable_links: =>
    return unless @$('.field.ui-droppable').length

    @$('.field.ui-droppable').remove()

  process_solved_true: (data) ->
    window.notificationCollection.add
      content: data.message
      status: 'notice'

    @show_solution(data.solution)
    @$('.task_hints_block').remove()

    @change_menu_elemet_state('success')

    points_text =  @set_points_text(data.points)
    @$('.task_points.answer').replaceWith(@points_template(points_class: 'correct_answer', points: data.points, points_text: points_text, text: 'Вы получили'))
    @$('.task_questions_submit').remove()
    @$('.task_next_link').removeClass('hidden_block') if @$('.task_next_link').length

    @update_progress(data.lesson)

  process_solved_false: (data) ->
    window.notificationCollection.add
      content: data.message
      status: 'alert'

    @show_solution(data.solution)

    @$('.task_points.answer').replaceWith(@points_template(points_class: 'wrong_answer', points: 0, points_text: 'баллов', text: ' получено'))
    @$('.task_questions_submit').remove()
    @$('.task_next_link').removeClass('hidden_block') if @$('.task_next_link').length

    @change_menu_elemet_state('fail')
    @update_progress(data.lesson)


  show_correct_answers: (data) =>
    correct_answers = data.correct_answers or []
    $.each correct_answers, (question_id, answers) =>
      $field_wrap = @$('.edit_user_answer div[data-id="' + question_id + '"]')
      $field_type = $field_wrap.attr('data-type')

      if $field_type is 'text'
        correct_answers = @form_list(answers)
        user_answers = @form_list(data.user_answers?[question_id] or '')

        @tmpl = @text_answer_template( correct_answers: correct_answers, user_answers: user_answers)
        $field_wrap.find('.text_answer').html(@tmpl)

        $field_wrap.find('.text_add_field').remove()

      else if $field_type is 'radio'
        $radio_field_wraps = $field_wrap.find('.task_questions_radio_answer')
        @set_final_classes($radio_field_wraps, answers)

      else if $field_type is 'checkbox'
        $checkbox_field_wraps = $field_wrap.find('.task_questions_checkbox_answer')
        @set_final_classes($checkbox_field_wraps, answers)

      else if $field_type is 'links'
        $.each answers, (linked_answer_id, simple_answer_ids) =>
          $answer_row = $field_wrap.find(".links_collection_first li[data-linked_answer_id='#{linked_answer_id}']")

          for simple_answer_id in simple_answer_ids
            $simple_answer = $field_wrap.find(".links_collection_second li[data-simple_answer_id='#{simple_answer_id}']")
            if $simple_answer.length
              simple_answer_content = $simple_answer.find('.answer_content').html()

          user_answer_content = $answer_row.find('.answer .answer_content').html()
          user_answer_id = $answer_row.find('.answer').attr('data-simple_answer_id')

          if parseInt(user_answer_id) in simple_answer_ids
            user_answer_content = false

          @tmpl = @linked_answer_template( correct_answer: simple_answer_content, incorrect_answer: user_answer_content)

          $answer_row.find('.text').after(@tmpl)
          $answer_row.find('.answer').remove()

        $field_wrap.find('.links_collection_second_title').remove()
        $field_wrap.find('.links_collection_second').remove()

  form_list: (list) =>
    list_length = (value for key, value of list when value.length != 0).length

    li = ""
    if list_length > 1
      li += "<ul>"
      $.each list, (index, list_element) =>
        if list_element.length
          li += "<li>#{list_element}</li>"
      li += "</ul>"
    else
      $.each list, (index, list_element) =>
        li += list_element
    return li


  # классы для чекбоксов и радиобаттонов
  set_final_classes: ($field_wraps, answer_id) =>
    $field_wraps.addClass('solved')
    $field_wraps.each ->
      $input = $(@).find('input')
      $value = parseInt($input.val())
      is_checked = $input.prop('checked')

      # есть ли значение $value в массиве answer_id
      $index = $.inArray($value, answer_id)

      if ($index isnt -1) and is_checked
        $input.siblings('label').addClass("active correct")
      else if ($index is -1) and is_checked
        $input.siblings('label').addClass("active incorrect")
      else if ($index isnt -1) and !is_checked
        $input.siblings('label').addClass("passive correct")
      else if ($index is -1) and !is_checked
        $input.siblings('label').addClass("passive incorrect")

  show_solution: (solution) ->
    return unless solution
    $('.task_questions').after(@solution_template(content: solution))
    @set_mathjax('mathjax_solution')

  update_progress: (lesson) ->
    solved_tasks_count = parseInt($('.task_progress_bar .task_statistics_tasks .solved_tasks_count').text()) + 1
    tasks_count = $('.task_progress_bar .task_statistics_tasks .tasks_count').text()
    max_result = $('.task_progress_bar .task_statistics_points .max_result').text()

    $('.task_progress_bar').replaceWith(@progress_bar_template(lesson: lesson, solved_tasks_count: solved_tasks_count, tasks_count: tasks_count, max_result: max_result))

  remove_radio_style: ->
    $('.task_questions_radio').each ->
      $radio_field_wraps = $(@).find('.task_questions_radio_answer')
      # если есть хоть один правильно выбранный радиобаттон
      if $radio_field_wraps.find('label.active.correct').length
        $radio_field_wraps.find('label.passive.correct').removeClass('correct').addClass('incorrect')

  show_new_hint: (data) ->
    hint_cost = @$('.task_hints_block .hint_cost').text()
    points_text = @$('.task_hints_block .points_text').text()
    answer_points = parseFloat(@$('.task_points.answer .task_points_digit').text())

    # удаляю блок подсказок
    if data.hint.position == data.hint.hints_count
      @$('.task_hints_block').remove()

    # вставляю новую подсказку
    @$('.task_hints_list').append(@hint_template(hint: data.hint, hint_cost: hint_cost, points_text: points_text))

    @change_menu_elemet_state('started')
    @set_mathjax('mathjax_hints')
    # изменяю баллы
    answer_points = (answer_points - hint_cost)
    answer_points = answer_points.toFixed(1) if answer_points % 1 != 0
    # увеличиваю количество попыток
    points_text = @set_points_text(answer_points)
    @$('.task_current_attempt').text(parseInt(@$('.task_current_attempt').text()) + 1)

    @$('.task_points.answer').replaceWith(@points_template(points_class: 'answer', points: answer_points, points_text: points_text, text: ' За правильный ответ'))

  show_hidden_block: (e) ->
    $target = $(e.target)
    $target.parent().find('.task_hints_list, .task_solution').removeClass('hidden_block')
    $target.remove()

  change_menu_elemet_state: (state) ->
    $('ul.page_menu_list li.page_menu_section a.active .icon').removeClass('started, not_started').addClass(state)

  insert_toggle_element: ($container, title, content, protocol=false) =>
    $page_content = $('.page_content_active')
    @template = @toggle_element_template( title: title, content: content)

    if protocol
      $container.html(@template)
    else
      $container.append(@template)

    # скрол окна, если нового блока не видно
    $last_toggle_line = $page_content.find('.toggle_line:last')
    $last_toggle_header = $page_content.find('.toggle_element:last .toggle_header')

  set_radio_button: (e) ->
    $label = $(e.target)
    return if $label.hasClass('active') or $label.parent('.task_questions_radio_answer').hasClass('solved')

    # снимаю выделение с предыдущего элемента
    $active_label = $label.closest('.task_questions_radio').find('label.active')
    $active_label.removeClass('active')
    $active_label.siblings('input[type="radio"]').prop('checked', false)
    # выделяю новый
    $label.addClass('active')
    $label.siblings('input[type="radio"]').prop('checked', true)

  set_checkbox_button: (e) ->
    $label = $(e.target)
    return if $label.parent('.task_questions_checkbox_answer').hasClass('solved')

    if $label.hasClass('active')
      # снимаю выделение с элемента
      $label.removeClass('active')
      $label.siblings('input[type="checkbox"]').prop('checked', false)
    else
      # выделяю элемент
      $label.addClass('active')
      $label.siblings('input[type="checkbox"]').prop('checked', true)

  remove_error: (e) =>
    $target = $(e.target)
    $parent = $target.closest('.programming_file')
    $parent = $target.parent() unless $parent.length
    @show_error($target, $parent, '')

  set_mathjax: (block) =>
    MathJax.Hub.Queue(
      ["Typeset", MathJax.Hub, block]
    )

  add_field: (e) ->
    $target = $(e.target)
    $wrapper = $target.closest('.task_questions_text')
    if $wrapper.find('input').length < 50
      $input_field = $wrapper.find('input').eq(0).clone().val('')
      $wrapper.find('.text_answer').append($input_field)

  set_custom_select: ->
    $('#programming_language_id').customSelect(customClass: "custom_select") if $('#programming_language_id').length
    if $('#programming_language_id option').length > 1
      $('#programming_language_id option').first().hide()
      @$('.select_programming_language .custom_selectInner').html('<span class="select_placeholder">Укажи язык программирования</span>')

  set_textareasize: ->
    return unless $('.essay')
    @$('textarea').autosize
      minHeight: 225

  toggle_block: (e) ->
    $('.task_show_essay_answer').toggleClass('close')
    $('.task_essay_answer').toggleClass('hidden_block')

  enable_submit_button: ->
    if @$('.task_questions_essay textarea').val()
      @$('input.blue_button').prop('disabled', false).removeClass('disabled')
    else
      @$('input.blue_button').prop('disabled', true).addClass('disabled')

  set_points_text: (points) ->
    if (points % 1) == 0
      polyglot.t('points', { smart_count: points })
    else
     'балла'

  set_url_params: (data) ->
    current_params = window.location.search.split('?')[1]?.split('&')
    current_params or= []
    is_mobile = true for param in current_params when param is 'layout=mobileapp'
    return unless is_mobile?

    new_params = ['layout=mobileapp']
    unless data.only_progress?
      success = if data.solved? then data.solved else 'solving'
      new_params.push("success=#{success}")
      new_params.push("scores=#{parseFloat(data.points)}") if data.solved

    new_params.push("user_performance=#{data.lesson.solved_percent}") if data.lesson?.solved_percent?
    new_params.push("user_progress=#{data.lesson.progress_percent}") if data.lesson?.progress_percent?

    if window.history.replaceState
      url = "#{window.location.origin}#{window.location.pathname}?#{new_params.join('&')}"
      window.history.replaceState(null, document.title, url)
    else
      window.location.href = url

  update_progress: ->
    $progress_elements = $('.fxf_progress.fxf_progress_circle')
    progress_view = new Stoege.Views.Progress
    progress_view.draw_circle_progress $(progress) for progress in $progress_elements
    @
