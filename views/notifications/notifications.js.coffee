class Stoege.Views.Notifications extends Backbone.View
  el: '.notifications'

  initialize: ->
    @listenTo(@collection, 'add', @add_one, @)
    @listenTo(@collection, 'remove', @remove_one, @)

  add_one: (model) ->
    notificationView = new Stoege.Views.Notification(model: model)
    notification_el = notificationView.render().el
    @$el.prepend(notification_el)
    $(notification_el).slideDown(300)

  remove_one: (model) ->
    model.trigger('removed')
