class Stoege.Views.Notification extends Backbone.View

  template: JST['backbone/templates/notification']

  className: 'notification'

  events:
    'click .email_confirmation_link' : 'send_email_confirmation'
    'click .close_button' : 'remove_model'
    'click .navigation_link' : 'remove_model'
    'click a' : 'remove_model'

  initialize: ->
    @listenTo(@model, 'removed', @close, @)

  render: ->
    @$el.html(@template(@model.toJSON()))
    _.delay =>
      @$el.addClass 'active'
    , 300
    @

  remove_model: ->
    @model.collection.remove(@model)

  close: ->
    @$el.removeClass 'active'

    # ждем пока отработает css анимация
    _.delay =>
      @$el.remove()
    , 1000

  send_email_confirmation: =>
    $.ajax
      type: 'POST'
      url: '/api/user/confirmation'
    .done ->
      window.notificationCollection.add
        content: 'На твой email высланы инструкции для подтверждения регистрации'
        status: 'notice'
    @close
