class Stoege.Views.NotificationPhone extends Stoege.Views.Application

  el: '.notification_phone_wrapper'

  events:
    'click .close_button' : 'close'

  initialize: ->
    @open()

  open: ->
    #72 hours = 25920000
    
    cookie_count = $.cookie('notification_phone_count')

    if cookie_count?
      if parseInt(cookie_count) isnt 2
        time = parseInt(new Date() - new Date($.cookie('notification_phone_date')))/1000
        @animate_notification() if time >= 25920000          
    else
      @animate_notification()

  animate_notification: -> 
    @$el.toggleClass('active')

  close: ->  
    cookie_count = $.cookie('notification_phone_count')

    @animate_notification()

    if cookie_count? then @set_cookie(null, 2) else @set_cookie(new Date(), 1)

  set_cookie: (date, count) ->
    $.cookie('notification_phone_date', date, { path: '/', expires: 20000 }) if date?
    $.cookie('notification_phone_count', count, { path: '/', expires: 20000 }) if count?