class Stoege.Views.GradesSelect extends Backbone.View

  el: '.select_grades'

  events: 
    'click .value' : 'toggle_open'
    'click li' : 'select_grades'

  initialize: ->
    @listenTo(@collection, 'sync', @render_options, @)
    @listenTo(@collection, 'change:active', @change_active_attribute, @)

    @collection.fetch()

  render_options: ->     
    for option in @collection.toJSON()
      @$('.options').prepend("<li data-value='#{option.name}'><span>#{option.index}</span></li>")
    @
    
  toggle_open: (e)->
    @$el.toggleClass('selected')

  select_grades: (e) ->
    $target = $(e.target).closest('li')
    value = $target.data('value')

    @collection.findWhere(active: true)?.set(active: false)
    @collection.findWhere(name: value).set(active: true)    

    @$('.value').html(value)

  change_active_attribute: (grade) ->
    $target = @$("li[data-value='#{grade.get('name')}']")

    $target.toggleClass('active', grade)
