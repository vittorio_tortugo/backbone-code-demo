class Stoege.Views.HelloYoutube extends Backbone.View

  el: '.youtube_landing_wrapper'

  events:
    'click .navigation > li[data-disable="false"]:not(.active)' : 'scroll_slider'
    'click .navigation > li[data-disable="true"]' : 'blink_grades'
    'click .purpose_list > li:not(.active)' : 'set_active_purpose'
    'click .take_gift_button' : 'send_gift_request'
    'keypress .mail_block input' : 'send_request_enter'

  initialize: ->
    @animate = false
    @$slides = @$('.youtube_list > li')
    @$first_slide = @$('.first_slide_wrapper')
    @$second_slide = @$('.second_slide_wrapper')
    @$third_slide = @$('.third_slide_wrapper')

    @$('.mail_block input').mask("+7 (999) 999 99 99", autoclear: false)

    @listenTo(@collection, 'change:active', @activate_second_slide, @)

    @set_slider_height()
    $(window).resize =>
      @set_slider_height()

    $('.first_slide_wrapper, .second_slide_wrapper, .third_slide_wrapper').on "mousewheel DOMMouseScroll", (e) =>
      @animate_slides(e)

    @disable_arrow_buttons()

    $('body').addClass('youtube')
    $(window).scrollTop(0)
    _.delay (->
      $(window).scrollTop(0)
    ), 1000

  disable_arrow_buttons: (e) ->
    window.addEventListener "keydown", (e) ->
      if([32, 33, 34, 35, 36].indexOf(e.keyCode) > -1)
        e.preventDefault()
    , false

  set_slider_height: ->
    window_height = $(window).height()

    @$slides.height(window_height)
    @$('.youtube_list').css('min-height', window_height)

    @set_purpose_menu_height()

  set_purpose_menu_height: ->
    window_height = $(window).height()
    @$('.purpose_list').height(window_height - @$('.second_slide_text').outerHeight())

  animate_slides: (e) ->
    return if @animate
    e.preventDefault()
    event_wheel =  e.originalEvent.wheelDelta || e.originalEvent.detail*-1

    if event_wheel > 0
      @set_active_slide('-')
    else
      @set_active_slide('+') if @collection.findWhere(active: true)? or $('.purpose_list li.active').length
      @$first_slide.addClass('transform') if @collection.findWhere(active: true)?

    #@set_active_navigation(@$('.youtube_list > li.active[data-disable="false"]').data('id'))

  set_active_slide: (direction) ->
    $active_slide = @$('.youtube_list > li.active[data-disable="false"]')
    if direction is '+'
      $last = @$('.youtube_list > li[data-disable="false"]').last()
      return if $last.hasClass('active')

      $slide_next = $active_slide.next()

      $active_slide.removeClass('active')
      $slide_next.addClass('active')
      @animate = true
      $(window).scrollTo $slide_next, 400, =>
        @animate = false
        @set_active_navigation(@$('.youtube_list > li.active[data-disable="false"]').data('id'))
    else
      $first = @$('.youtube_list > li[data-disable="false"]').first()
      return if $first.hasClass('active')

      $slide_prev = $active_slide.prev()

      $active_slide.removeClass('active')
      $slide_prev.addClass('active')
      @$first_slide.removeClass('transform') if @$first_slide.hasClass('active')
      @animate = true
      $(window).scrollTo $slide_prev, 400, =>
        @animate = false
        @set_active_navigation(@$('.youtube_list > li.active[data-disable="false"]').data('id'))

  activate_second_slide: (grade) ->
    return unless grade.get('active')

    $second_slide = @$('.navigation, .youtube_list').find('li[data-id=2]')

    $second_slide.attr('data-disable', 'false')
    @$first_slide.addClass('transform')
    @set_active_slide('+')
    @set_active_navigation(2)

  scroll_slider: (e) ->
    $target = $(e.target).closest('li')
    id = $target.data('id')

    switch (id)
      when 1
        @$first_slide.removeClass('transform')
        $(window).scrollTo(0, 400)
      when 2
        @$first_slide.addClass('transform')
        $(window).scrollTo(@$second_slide, 400)
      when 3
        @$first_slide.addClass('transform')
        $(window).scrollTo(@$third_slide, 400)

    @set_active_navigation(id)


  set_active_navigation: (id) ->
    @$(".navigation li, .youtube_list > li").removeClass('active')
    @$(".navigation li[data-id=#{id}], .youtube_list > li[data-id=#{id}]").addClass('active')

    @set_navigation_color(id)
    @set_purpose_menu_height()

  set_active_purpose: (e) ->
    $target = $(e.target).closest('li')

    @$('.purpose_list > li.active')?.removeClass('active')
    $target.addClass('active')

    @$('.navigation, .youtube_list').find('li[data-id=3]').attr('data-disable', 'false')
    @set_active_slide('+')
    @set_active_navigation(3)

  set_navigation_color: (id) ->
    @$('.navigation').toggleClass('black', id is 2)

  send_gift_request: ->
    $mail_block = @$('.mail_block')
    grade_id = @collection.findWhere(active: true).get('id')
    level_id =  @$('.purpose_list > li.active').data('id')
    landing_url = window.location.pathname

    $.ajax
      type: "POST",
      url: landing_url
      data:
        phone: $mail_block.find('input').val()
        landing_name: landing_url.replace '/', ''
        grade_id: grade_id
        level_id: level_id
        type: '1'
      success: =>
        $mail_block.removeClass('error')
        @change_gift_content()
      error: (error)->
        $mail_block.addClass('error')
        error_message = error.responseJSON['error']
        $mail_block.find('.text_error').html(error_message) if error_message
        $mail_block.addClass('error')

  send_request_enter: (e) ->
    if e.keyCode == 13
      @send_gift_request()

  blink_grades: ->
    $select_grades = @$('.select_grades')

    $select_grades.addClass('blink')

    _.delay (=>
      $select_grades.removeClass('blink')
    ), 100

  change_gift_content: ->
    $mail_block_wrapper = @$('.mail_block_wrapper')

    $mail_block_wrapper.addClass('transform')
