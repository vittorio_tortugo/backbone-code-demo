class Stoege.Views.FadingSlider extends Backbone.View

  el: '.fading_slider'

  events:
    'click .arrow_left' : 'manualy_slide_left'
    'click .arrow_right' : 'manualy_slide_right'

  initialize: ->
    @total_number = 5
    @current_number = 1
    @animating = false
    @timing = setInterval(@auto_slide_right, 5000)

  manualy_slide_left: ->
    clearInterval(@timing)
    @slide_left()
    @timing =setInterval(@auto_slide_right, 14000)

  manualy_slide_right: ->
    clearInterval(@timing)
    @slide_right()
    @timing = setInterval(@auto_slide_right, 14000)

  auto_slide_right: =>
    @slide_right()

  slide_left: ->
    return if @animating
    @animating = true
    @current_number -= 1
    @current_number = @total_number if @current_number < 1
    @switch_fading_active_item()

  slide_right: ->
    return if @animating
    @animating = true
    @current_number += 1
    @current_number = 1 if @current_number > @total_number
    @switch_fading_active_item()

  switch_fading_active_item: ->
    $slide = @$el.find(".slide")
    $active_element = @$(".slider_menu li[data-id=#{@current_number}]")
    _this = @

    $slide.fadeOut 300, ->
      $(@).attr('src', $active_element.data('image')).fadeIn(300)

    @$('.slider_menu li.active').slideUp 300, ->
      $(@).removeClass('active')
      $active_element.slideDown 300, ->
        $(@).addClass('active')
        _this.animating = false
