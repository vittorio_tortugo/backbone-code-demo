class Stoege.Views.VideoPlayer extends Backbone.View
  
  el: '#main_video'

  initialize: ->
    player =
      playVideo: ->
        if typeof (YT) is "undefined" or typeof (YT.Player) is "undefined"
          window.onYouTubePlayerAPIReady = ->
            player.loadPlayer("main_video", "SWTMxaHUmKo")
            return
          $.getScript "//www.youtube.com/player_api"
        else
          player.loadPlayer("main_video", "SWTMxaHUmKo")
        return

      loadPlayer: ->
        window.myPlayer = new YT.Player("main_video",
          playerVars:
            modestbranding: 1
            rel: 0
            showinfo: 0
            autoplay: 0

          height: '100%'
          width: '100%'
          videoId: "SWTMxaHUmKo"
          events:
            onStateChange: (event) ->
              dataLayer.push('event': 'tutorial_video_play') if dataLayer? and event.data is 1
        )

    player.playVideo()