class Stoege.Views.LandingEmailDialogView extends Stoege.Views.Dialog

  template: JST["backbone/templates/landings/mobile_email_dialog"]

  className: 'email_dialog'

  events:
    'click .close_button' : 'destroy'
    'click .submit_button' : 'send_email'
    'keypress .email_content input': 'send_email_request_enter'

  initialize: ->
    $(window).resize =>
      @set_dialog_position()

  open_dialog_for_email: (mobile_os)->
    @close_dialogs()
    @$el.html(@template(inner_text: mobile_os))
    @open(600)
    @set_dialog_position()

  close_dialogs: ->
    $('.ui-dialog-content').dialog('close')

  destroy: ->
    @$el.empty()
    @$el.dialog('close')

  send_email_request_enter: (e) ->
    if e.keyCode == 13
      @send_email()

  send_email: ->
    if window.location.pathname is '/mobile'
      dialog_location = 'mobile'
    else
      dialog_location = 'footer'
    $.post(
      '/mobile', {email: $('input[name="email"]').val(), landing_name: dialog_location, type: $('input[name="email"]').attr('class')}
    )
    .done (response) ->
      $('.response').html("<div class='success'>почта отправлена</div>")
    .fail (response) ->
      $('.response').html("<div class='error'>#{response.responseJSON.error}</div>")
