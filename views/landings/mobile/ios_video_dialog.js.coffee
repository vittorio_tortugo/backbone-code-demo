#= require ../../dialogs/dialog.js.coffee
class Stoege.Views.IosVideoDialogView extends Stoege.Views.Dialog

  className: 'video_dialog'

  initialize: ->
    $(window).resize =>
      @set_dialog_position()

  open_video: ->
    @close_dialogs()
    video_url = @model.get('url')
    @$el.html "<iframe src='#{video_url}' width='640' height='360' frameborder='0' webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>"
    @open()
    @set_dialog_position()
    @$el.parent().addClass('ios_video_dialog_wrapper')

  close_dialogs: ->
    $('.ui-dialog-content').dialog('close')

  destroy: ->
    @$el.empty()
    @$el.dialog('close')
