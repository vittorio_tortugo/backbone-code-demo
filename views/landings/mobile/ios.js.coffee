class Stoege.Views.Ios extends Backbone.View

  el: '.landing_ios_wrapper'

  events:
    'click .play_video': 'play_video'
    'click .google_play_link' : 'open_dialog_for_email'
    'click .windows_store_link' : 'open_dialog_for_email'

  main_template: JST["backbone/templates/landings/main"]

  initialize: ->
    @video_url = 'https://www.youtube.com/embed/SWTMxaHUmKo?autoplay=1&rel=0'
    @render()
    $('body').addClass('landing_ios')

    @throttled_parallax = _.throttle @ipad_parallax, 10

    $(window).scroll =>
      @throttled_parallax()


  play_video: (e) ->
    e.preventDefault()
    @video_dialog_model or= new Stoege.Models.VideoDialogModel(url: @video_url)
    @video_dialog_view or= new Stoege.Views.IosVideoDialogView(model: @video_dialog_model)
    @video_dialog_view.open_video()
    @

  ipad_parallax: ->
    wrapper_offset = $('.parallax_process').offset().top
    scroll_top = $(window).scrollTop()
    diff = wrapper_offset - scroll_top
    @$('.parallax_process .icons_big').css({ transform: "translateY(#{0.2 * diff}px)"})
    @$('.parallax_process .ipad').css({ transform: "translateY(#{-0.2 * diff}px)"})

  open_dialog_for_email: (e) ->
    mobile_os = $(e.target).data('mobile')
    @email_dialog_view = new Stoege.Views.LandingEmailDialogView
    @email_dialog_view.open_dialog_for_email(mobile_os)


