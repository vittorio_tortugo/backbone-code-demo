#= require ../parallax_slider.js.coffee
class Stoege.Views.IosAdvantagesSlider extends Stoege.Views.ParallaxSlider

  el: '.main_parallax_slider_wrapper'

  events:
    'click .presentation_navigation .point' : 'manually_change_slide'

  initialize: (options) ->
    @current_number = 1
    @express = false
    @total_number = 4
    @template_name = 'ios_parallax_slider'
    @animating = false
    @template = JST["backbone/templates/landings/vertical_parallax_slider"]
    @$parallax_slider = @$('.parallax_slider')
    @render()
    @set_timing()
    @set_class_for_ie()

  render: ->
    @$parallax_slider.html(@template(current_number: @current_number, total_number: @total_number, template_name: @template_name, express: @express))
    @render_navigation()

  auto_slide_up: =>
    @slide_up()

  slide_up: ->
    return if @animating
    @animating = true
    @current_number += 1
    @current_number = 1 if @current_number > @total_number
    @$parallax_slider.append(@template(current_number: @current_number, total_number: @total_number, template_name: @template_name, express: @express))
    @$parallax_slider.find('>li').first().addClass('parallax_transition_up')
    @animate_slide()
    @render_navigation()

  animate_slide: () ->
    slide_height = parseInt($('.parallax_slider_wrapper, .main_parallax_slider_wrapper').height())

    $first_slide = @$parallax_slider.find('> li').first()
    $first_slide.find('.parallax_right_part').animate
      top: "+=#{slide_height}px"
    , 700, 'easeInCubic'

    @$parallax_slider.animate
      top: "-=#{slide_height}px"
    , 700, 'easeInQuad', =>
      @$parallax_slider.css('top', '0px')
      @$parallax_slider.find('> li').first().remove()
      @animating = false

  set_timing: ->
    @timing = setInterval(@auto_slide_up, 5000)

  set_class_for_ie: ->
    ua = window.navigator.userAgent
    @$parallax_slider.addClass('ie') if ua.indexOf('MSIE ') > 0 or ua.indexOf('Trident/') > 0

  render_navigation: ->
    $('.presentation_navigation .point.active').removeClass('active')
    $('.presentation_navigation .point').eq(@current_number - 1).addClass('active')

  manually_change_slide: (e) ->
    return if @animating

    $point = $(e.currentTarget)
    index = $point.index()
    return if index is @current_number - 1

    @animating = true
    @current_number = index + 1
    @current_number = 1 if @current_number > @total_number
    @$parallax_slider.append(@template(current_number: @current_number, total_number: @total_number, template_name: @template_name, express: @express))
    @$parallax_slider.find('>li').first().addClass('parallax_transition_up')
    @animate_slide()
    @render_navigation()
    clearTimeout(@timing)
    @set_timing()
