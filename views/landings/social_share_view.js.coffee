class Stoege.Views.SocialShare extends Backbone.View

  el: '.social'

  initialize: ->
    @init_share_links()
    @

  init_share_links: ->
    share_url = location.href.split('/').splice(0, 5).join('/')
    @$('div').ShareLink
      url: share_url
      text:  @model.get('text')
      width: 640
      height: 480
    @
