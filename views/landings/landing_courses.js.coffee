class Stoege.Views.LandingCourses extends Backbone.View
  
  el: '.courses_wrapper'

  events:
    'mouseenter .courses_list > li' : 'set_course_hover_color'
    'mouseleave .courses_list > li' : 'set_course_default_color'

  template: JST["backbone/templates/landings/courses"]

  initialize: ->
    @current_route =  window.location.pathname
    @get_level_and_title_names_from_route()
    @courses_collection = new Stoege.Collections.LandingCourses
    @listenTo(@courses_collection, 'sync', @render, @)
    @courses_collection.fetch
      data:
        level_name: @level_name
        express: @express

  render: ->
    @$el.html(@template(courses: @courses_collection, level_name: @level_name, title: @title, button_name: @button_name, express: @express))

  set_course_hover_color: (e) ->
    $target = $(e.target).closest('li')
    hover_color = $target.data('hover_color')
    $target.css('background', "##{hover_color}")

  set_course_default_color: (e) ->
    $target = $(e.target).closest('li')
    color = $target.data('color')

    $target.css('background', "##{color}")

  get_level_and_title_names_from_route: ->
    @express = false
    if @current_route.match(/^\/olymp/)
      @level_name = 'Олимпиадный'
      @title = 'Начни подготовку к олимпиаде сейчас'
      @button_name = 'Все курсы для подготовки к олимпиадам'
    else if @current_route.match(/^\/ege/)
      @level_name = 'ЕГЭ'
      @title = 'Начни подготовку к ЕГЭ сейчас'
      @button_name = 'Все курсы для подготовки к ЕГЭ'
      @express = @current_route.indexOf('ege_express') > 0
    else if @current_route.match(/^\/gia/)
      @level_name = 'ГИА'
      @title = 'Начни подготовку к ГИА сейчас'
      @button_name = 'Все курсы для подготовки к ГИА'
      @express = @current_route.indexOf('gia_express') > 0
    else if @current_route.match(/^\/basic/)
      @level_name = 'Базовый'
      @title = 'Выбери предмет, который хочешь подтянуть'
      @button_name = 'Все курсы базового уровня'
    else if @current_route.match(/^\/advanced/)
      @level_name = 'Углубленный'
      @title = 'Выйди за рамки школьной программы'
      @button_name = 'Все курсы углубленного уровня'
