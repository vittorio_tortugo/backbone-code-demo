class Stoege.Views.Landing extends Backbone.View

  el: '.main_content'

  landing_template: JST["backbone/templates/landings/landing"]
  change_aim_template: JST["backbone/templates/landings/change_aim"]
  landing_info_template: JST["backbone/templates/landings/landing_info"]

  initialize: ({@page}) ->
    @current_route =  Backbone.history.fragment
    @get_current_number_from_route()
    @listenTo(@page, 'change:title', @change_title, @)
    @listenTo(@page, 'change:meta_description', @change_meta_description, @)

    @set_meta_info()

    @clear_previous_page()
    @render()
    new Stoege.Views.ParallaxSlider()
    new Stoege.Views.LandingCourses()
    new Stoege.Views.FixedMenu()
    @render_landing_info()
    @render_change_aim()

  clear_previous_page: ->
    @$('.fixed_menu_wrapper').remove()
    @$('.plus_menu_wrapper').remove()
    @$('.main_parallax_slider_wrapper').remove()
    @$('.levels_menu_wrapper').remove()
    @$('.disciplines_menu_wrapper').remove()
    @$('.comments_wrapper').remove()
    @$('.parallax_slider_wrapper').remove()
    @$('.courses_wrapper').remove()
    @$('.landing_info_wrapper').remove()
    @$('.change_aim_info').remove()

  render: ->
    @$('.presentation').after(@landing_template(current_number: @current_number, express: @express))

  render_landing_info: ->
    if @$('.landing_info_wrapper').length
      @$('.landing_info_wrapper').replaceWith(@landing_info_template(current_route: @current_route))
    else
      @$('.fading_wrapper').after(@landing_info_template(current_route: @current_route))

  render_change_aim: ->
    @$('.free_lessons_info_wrapper').html(@change_aim_template())

  get_current_number_from_route: ->
    @express = false
    if @current_route.match(/^\$/)
      @current_number = 1
    else if @current_route.match(/ege/g) 
      @current_number = 2
      @express = @current_route.indexOf('ege_express') is 0
    else if @current_route.match(/gia/g)
      @current_number = 3
      @express = @current_route.indexOf('gia_express') is 0
    else if @current_route.match(/basic/g)
      @current_number = 4
    else if @current_route.match(/advanced/g)
      @current_number = 5
    else if @current_route.match(/olymp/g)
      @current_number = 6


  set_meta_info: ->
    switch @current_route
      when 'olymp' then @page.set
          title: 'Подготовка к олимпиаде. Курсы подготовки к олимпиадам онлайн.'
          meta_description: 'Подготовка школьников к олимпиадам в Фоксфорд - это всегда эффективно и недорого.'
      when 'gia' then @page.set
          title: 'Курсы подготовки к ГИА онлайн для 9 класса.'
          meta_description: 'Подготовка к ГИА для школьников 9 классов, решение заданий для сдачи ГИА с лучшими учителями в режиме онлайн.'

  change_title: ->
    document.title = @page.get('title')

  change_meta_description: ->
    $('meta[name=description]').attr('content', @page.get('meta_description'))
