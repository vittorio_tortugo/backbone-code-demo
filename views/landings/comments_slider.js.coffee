class Stoege.Views.CommentsSlider extends Backbone.View
  el: '.comments_wrapper'

  events:
    'click .arrow_left' : 'slide_left'
    'click .arrow_right' : 'slide_right'

  template: JST["backbone/templates/main/comment_slider_item"]

  initialize: ->
    @animated = true
    @listenTo(@model, 'sync', @render, @)
    @model.fetch()

  slide_left: ->
    @direction = "-"
    @model.fetch()

  slide_right: ->
    @direction = "+"
    @model.fetch()

  render: ->
    return unless @animated
    @animated = false

    if @direction == '-'
      @$('.main_comment_list').prepend(@template(@model.toJSON()))
    else
      @$('.main_comment_list').append(@template(@model.toJSON()))

    if @direction? then @animate_slide() else @animated = true
    @

  animate_slide: =>
    slide_width = $('.main_comment_list > li:eq(0)').width()
    
    $slider_wrapper = @$('.main_comment_list')
    if @direction == "+"
      $slider_wrapper.animate
        left: "-=#{slide_width}px"
      , 500, =>
        $slider_wrapper.css('left', '0px')
        $slider_wrapper.find('> li').first().remove()
        @animated = true
    else
      $slider_wrapper.css('left', "-#{slide_width}px")
      $slider_wrapper.animate
        left: "+=#{slide_width}px"
      , 500, =>
        $slider_wrapper.find('> li').last().remove()
        $slider_wrapper.css('left', 0)
        @animated = true



