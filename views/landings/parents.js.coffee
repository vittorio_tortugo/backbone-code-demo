class Stoege.Views.Parents extends Backbone.View

  el: '.main_content'

  main_template: JST["backbone/templates/landings/parents_main"]
  disciplines_menu_template: JST["backbone/templates/landings/disciplines_menu"]
  levels_menu_template: JST["backbone/templates/landings/levels_menu"]
  comments_template: JST["backbone/templates/landings/comments"]
  parents_info_template: JST["backbone/templates/landings/parents_info"]

  initialize: ->
    @clear_previous_page()
    @render()
    @disciplines_collection = new Stoege.Collections.LandingDisciplines
    @listenTo(@disciplines_collection, 'sync', @render_disciplines_menu, @)
    @disciplines_collection.fetch()
    @render_levels_menu()
    @render_comments()
    @render_free_lessons_info()
    new Stoege.Views.ParallaxSlider(total_number: 5, template_name: 'parents_parallax_slider')
    @comments_model = new Stoege.Models.CommentSlider
    new Stoege.Views.CommentsSlider(model: @comments_model)

  clear_previous_page: ->
    @$('.parallax_slider_wrapper').remove()
    @$('.main_parallax_slider_wrapper').remove()
    @$('.courses_wrapper').remove()
    @$('.landing_info_wrapper').remove()
    @$('.plus_menu_wrapper').remove()
    @$('.levels_menu_wrapper').remove()
    @$('.disciplines_menu_wrapper').remove()
    @$('.comments_wrapper').remove()

  render: ->
    @$('.presentation').after(@main_template())

  render_comments: ->
    @$('.fading_wrapper').after(@comments_template())

  render_levels_menu: ->
    @$('.levels_menu_wrapper').html(@levels_menu_template())

  render_disciplines_menu: ->
    @$('.disciplines_menu_wrapper').html(@disciplines_menu_template(disciplines: @disciplines_collection.toJSON()))

  render_free_lessons_info: ->
    @$('.free_lessons_info_wrapper').html(@parents_info_template())

