class Stoege.Views.FixedMenu extends Backbone.View

  el: '.fixed_menu_wrapper'

  events:
    'click li': 'scroll_to_panel'
    'click .fox_head': 'scroll_to_begin'

  initialize: ->
    @visibility_menu()
    $(window).scroll =>
      @visibility_menu()

  visibility_menu: ->
    window_scroll = $(window).scrollTop()
    $presentation_block = $('.presentation, .uchebnik_presentation_wrapper')
    header_offset_bottom = $presentation_block.offset().top + $presentation_block.height()

    if window_scroll >= header_offset_bottom - $('.fixed_menu').height()
      @$el.slideDown('fast')
    else
      @$el.slideUp('fast')

  scroll_to_panel: (e) ->
    target_panel = $(".#{$(e.target).closest('li').data('panel')}")
    target_offset = offset: - $('.fixed_menu').height()
    $(window).scrollTo(target_panel, 200, target_offset)

  scroll_to_begin: ->
    $(window).scrollTo(0, 200)

