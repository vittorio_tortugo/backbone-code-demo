class Stoege.Views.ExpressPromo extends Stoege.Views.Application

  el: '.express_landing_wrapper'

  events:
    'click .sale_block .red_button' : 'send_coupon'
    'keypress .sale_block input' : 'send_request_enter'
    'click .registration .red_button' : 'registration_request'
    'keypress .registration input' : 'send_request_enter_registration'

  initialize: (options) ->
    @lead = new Stoege.Models.Lead
    @params = options.params
    @disciplines = @initialize_disciplines()

    @set_slider_height()
    $(window).resize =>
      @set_slider_height()

    @set_slides_info(@params['discipline_id'])

    $('.video iframe').iframeTracker
      blurCallback: =>
        @activate_video()

    @$(".send_coupon input#phone, #user_phone").mask("+7 (999) 999 99 99", autoclear: false)
    @$('.user_grade_id select').customSelect(customClass: "custom_select")
    $('body').addClass('express')

  initialize_disciplines: ->
    disciplines =
      1:
        name: "Обществознание"
        text: "Подготовка к ЕГЭ по обществознанию<br/>за 2 месяца"
        youtube_link: 'http://www.youtube.com/embed/i4Le0DRGX54?rel=0'
        course_link: 'http://foxford.ru/courses/231'
      2:
        name: "Информатика"
        text: "Подготовка к ЕГЭ по информатике<br/>за 2 месяца"
        youtube_link: 'http://www.youtube.com/embed/MHBXZeK63Dg?rel=0'
        course_link: 'http://foxford.ru/courses/236'
      3:
        name: "Физика"
        text: "Подготовка к ЕГЭ по физике<br/>за 2 месяца"
        youtube_link: 'http://www.youtube.com/embed/vO5_bo0wBrQ?rel=0'
        course_link: 'http://foxford.ru/courses/233'
      4:
        name: "Русский язык"
        text: "Подготовка к ЕГЭ по русскому языку<br/>за 2 месяца"
        youtube_link: 'http://www.youtube.com/embed/ouyPBoP0cKg?rel=0'
        course_link: 'http://foxford.ru/courses/235'
      5:
        name: "Математика"
        text: "Подготовка к ЕГЭ по математике<br/>за 2 месяца"
        youtube_link: 'http://www.youtube.com/embed/6wHOO3qWIfU?rel=0'
        course_link: 'http://foxford.ru/courses/230'
      6:
        name: "Русский язык"
        text: "Подготовка к ГИА по русскому языку<br/>за 2 месяца"
        youtube_link: 'http://www.youtube.com/embed/WPM5rY4tseQ?rel=0'
        course_link: 'http://foxford.ru/courses/228'
      7:
        name: "Математика"
        text: "Подготовка к ГИА по математике<br/>за 2 месяца"
        youtube_link: 'http://www.youtube.com/embed/kZpL87rl81M?rel=0'
        course_link: 'http://foxford.ru/courses/232'
      8:
        name: "Математика"
        text: 'Подготовка к ЕГЭ по математике (часть С) <br/>за 2 месяца'
        youtube_link: 'http://www.youtube.com/embed/kVSqf4QVx7g?rel=0'
        course_link: 'http://foxford.ru/courses/229'
      9:
        name: "Русский язык"
        text: 'Подготовка к сочинению ЕГЭ<br/> по русскому языку за 2 месяца'
        youtube_link: 'http://www.youtube.com/embed/Kdpydm_afPc?rel=0'
        course_link: 'http://foxford.ru/courses/234'
      10:
        name: "Общий"
        text: 'Подготовка к ЕГЭ 2015 за 2 месяца'
        youtube_link: 'http://www.youtube.com/embed/1unyd5qTr5o?rel=0'
        course_link: 'http://foxford.ru/courses?page=1&course_type_id=2'

    return disciplines

  set_slider_height: ->
    window_height = $(window).height()

    @$('.first_slide_wrapper, .third_slide_wrapper').height(window_height)
    @$('.express_list').css('min-height', window_height)
    @$('.second_slide_content_wrapper').height(window_height - @$('.second_slide_text').outerHeight())

  send_coupon: ->
    $send_block = $('.send_coupon')
    type = @params.type
    if _.contains([1, 2], parseInt(type))
      @send_coupon_phone(@lead, $send_block, type)
    else
      @send_coupon_email(@lead, $send_block, type)

  send_coupon_phone: (lead, $send_block, type)->
    phone = $('input#phone').val().replace(/[\s()]/g, '').substr(1,11)
    lead.save(
      { phone: phone, landing_name: "express_#{@params.discipline_id}", type: type },
      success: (model) =>
        @lead = model
        $send_block.removeClass('error')
        dataLayer.push('event':'landing_express_promo_phone_submit') if dataLayer?
      complete: ->
        window.location.replace($send_block.find('.red_button').data('url')) unless $send_block.hasClass('error')
      error: (model, responce) ->
        $send_block.addClass('error')
    )

  send_coupon_email: (lead, $send_block, type) ->
    email = $('input#email').val()

    lead.save(
      { email: email, landing_name: "express_#{@params.discipline_id}", type: type },
      success: (model) =>
        @lead = model
        $send_block.removeClass('error')
        dataLayer.push('event':'landing_express_promo_phone_submit') if dataLayer?
      complete: ->
        window.location.replace($send_block.find('.red_button').data('url')) unless $send_block.hasClass('error')
      error: (model, responce) ->
        $send_block.addClass('error')
    )

  set_slides_info: (discipline_id) ->
    $first_slide_wrapper = @$('.first_slide_wrapper')
    discipline = @disciplines[discipline_id]
    $first_slide_wrapper.attr('data-background', discipline_id)
    $first_slide_wrapper.find('.main_header').html discipline.text
    $first_slide_wrapper.find('.video iframe').attr('src', discipline.youtube_link)
    $('.third_slide_wrapper .white_button').attr('href', discipline.course_link)

  send_request_enter: (e) ->
    if e.keyCode == 13
      @send_coupon()

  send_request_enter_registration: (e) ->
    if e.keyCode == 13
      @registration_request()

  registration_request: (e) ->
    e.preventDefault()
    $send_block = $('.registration')
    type = @params.type

    @lead.set(type: type)
    if parseInt(type) == 1
      @lead.set
        id: $.cookie('lead_id')
        name: $('input#name').val()
        email: $('input#email').val()
    else if parseInt(type) == 2
      @lead.set
        id: parseInt($.cookie('lead_id'))
        first_name: $('#user_first_name').val()
        last_name: $('#user_last_name').val()
        name: $('#user_first_name').val() + ' ' + $('#user_last_name').val()
        grade_id: $('#user_grade_id').val()
        password: $('#user_password').val()
        email: $('#user_email').val()
    else if parseInt(type) == 3
      @lead.set
        id: parseInt($.cookie('lead_id'))
        first_name: $('#user_first_name').val()
        last_name: $('#user_last_name').val()
        name: $('#user_first_name').val() + ' ' + $('#user_last_name').val()
        grade_id: $('#user_grade_id').val()
        password: $('#user_password').val()
        phone: $('#user_phone').val().replace(/[\s()]/g, '').substr(1,11)
    else if parseInt(type) == 4
      @lead.set
        id: parseInt($.cookie('lead_id'))
        first_name: $('#user_first_name').val()
        last_name: $('#user_last_name').val()
        name: $('#user_first_name').val() + ' ' + $('#user_last_name').val()
        grade_id: $('#user_grade_id').val()
        password: $('#user_password').val()
    else if parseInt(type) == 5
      @lead.set
        id: parseInt($.cookie('lead_id'))
        name: $('input#name').val()
        phone: $('input#user_phone').val().replace(/[\s()]/g, '').substr(1,11)

    @lead.save(null,
      success: (model, responce) =>
        $send_block.removeClass('error')
        $('.error_note_wrapper').remove()
      complete: ->
        window.location.replace($send_block.find('.red_button').data('url')) unless $send_block.hasClass('error')
      error: (model, responce) =>
        $('.error_note_wrapper').remove()
        json = $.parseJSON(responce.responseText)
        $.each json.errors, (key, value) =>
          $input = @$('#user_' + key)
          @show_error($input, $input.parent(), value[0])

        $send_block.addClass('error')
    )

  activate_video: ->
    dataLayer.push('event':'landing_express_promo_video_play') if dataLayer?
