class Stoege.Views.Grade10Ege extends Backbone.View

  el: '.main_content'

  main_template: JST["backbone/templates/landings/grade_10_ege_main"]
  landing_info_template: JST["backbone/templates/landings/landing_info"]

  events:
    'click .scroll_to_form' : 'scroll_to_form'
    'click .take_gift_button' : 'send_gift_request'
    'keypress .mail_block input' : 'send_request_enter'

  initialize: ->
    @clear_previous_page()
    @render()
    @render_landing_info()
    new Stoege.Views.ParallaxSlider()
    @set_gift_form_height()
    $(window).resize =>
      @set_gift_form_height()

  clear_previous_page: ->
    @$('.parallax_slider_wrapper').remove()
    @$('.main_parallax_slider_wrapper').remove()
    @$('.courses_wrapper').remove()
    @$('.landing_info_wrapper').remove()
    @$('.plus_menu_wrapper').remove()
    @$('.levels_menu_wrapper').remove()
    @$('.disciplines_menu_wrapper').remove()
    @$('.comments_wrapper').remove()

  render: ->
    @$('.presentation').after(@main_template())

  render_landing_info: ->
    current_route = 'grade_10_ege'
    if ($landing_info_wrapper = @$('.landing_info_wrapper')).length
      $landing_info_wrapper.replaceWith(@landing_info_template(current_route: current_route))
    else
      @$('.fading_wrapper').after(@landing_info_template(current_route: current_route))

  scroll_to_form: ->
    $(window).scrollTo($('.gift_form_wrapper'), 200, offset: -76)
    false

  set_gift_form_height: ->
    window_height = $(window).height()
    @$('.gift_form_wrapper').height(window_height)

  send_gift_request: ->
    $mail_block = @$('.mail_block')
    dataLayer.push('event': '10grade_get_gift_pressed') if dataLayer?
    $.ajax
      type: "POST",
      url: "/grade_10_ege"
      data:
        email: $mail_block.find('input').val()
        landing_name: 'grade_10_ege'
        type: '1'
      success: =>
        $mail_block.removeClass('error')
        @change_gift_content()
      error: ->
        $mail_block.addClass('error')

  send_request_enter: (e) ->
    if e.keyCode is 13
      @send_gift_request()

  change_gift_content: ->
    @$('.mail_block_wrapper').addClass('transform')

