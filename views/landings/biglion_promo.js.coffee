class Stoege.Views.BiglionPromo extends Backbone.View
  el: '.biglion_promo_wrapper'

  events:
    'click .presentation_button.with_arrow' : 'slide_to_about'

  slide_to_about: ->
  	$(window).scrollTo($(".video_presentation_wrapper"), 200)