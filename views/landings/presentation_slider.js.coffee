class Stoege.Views.PresentationSlider extends Backbone.View

  el: ".presentation"

  events:
    'click .arrow_left' : 'slide_left'
    'click .arrow_right' : 'slide_right'
    'click .opacity_button' : 'change_route_and_scroll_down'
    'click .scroll-button': 'scroll_to_info'

  initialize: ({@main})->
    @total_number = 6
    @marketing_ab = gon?.ab_test_1 or []
    @current_route =  window.location.pathname
    @get_current_number_from_route()
    @animating = false
    @set_slider_height()
    $(window).resize =>
      @set_slider_height()
    @render()
    @set_init_tranform()
    $(window).scroll =>
      @set_scroll_transform()

  template: JST["backbone/templates/landings/presentation_slider"]

  render: ->
    @$('.presentation_slider').html(@template(current_number: @current_number, total_number: @total_number, main: @main, express: @express, marketing_ab: @marketing_ab))
    @$presentation_background = @$('.presentation_background')

  set_init_tranform: ->
    @current_scale = false
    translate_y = parseInt($(window).scrollTop()/3)
    # если есть scroll_top при начальной загрузке слайдера, то убераем анимацию у изображения и выставляем tranlate_y
    return unless translate_y
    @set_transform(translate_y, 1.08)

  set_scroll_transform: ->
    @current_scale ||= @get_current_scale()
    translate_y = parseInt($(window).scrollTop()/3)
    # при скролле замораживаем текущий scale у изображения
    @set_transform(translate_y, @current_scale)

  set_transform: (translate_y, scale)->
    @$presentation_background.css
      'transform': "translate3d(0px, #{translate_y}px, 0px) scale(#{scale})", 'animation': 'none'
      '-o-transform': "translate3d(0px, #{translate_y}px, 0px) scale(#{scale})", '-o-animation': 'none'
      '-webkit-transform': "translate3d(0px, #{translate_y}px, 0px) scale(#{scale})", '-webkit-animation': 'none'
      '-moz-transform': "translate3d(0px, #{translate_y}px, 0px) scale(#{scale})", '-moz-animation': 'none'

  get_current_scale: ->
    # берем текущий scale
    matrix = @$presentation_background.css("-webkit-transform") ||
    @$presentation_background.css("-moz-transform") ||
    @$presentation_background.css("-ms-transform") ||
    @$presentation_background.css("-o-transform") ||
    @$presentation_background.css("transform")
    if matrix?
      scale = matrix.split('(')[1].split(',')[0]
    else
      scale = 1
    scale

  set_slider_height: ->
    @$el.height $(window).height() if $(window).width() > 1023

  slide_left: ->
    @current_number -= 1
    @current_number = @total_number if @current_number < 1
    @$('.presentation_slider').prepend(@template(current_number: @current_number, total_number: @total_number, main: @main, express: @express, marketing_ab: @marketing_ab))
    @$presentation_background = @$('.presentation_background').first()
    @set_init_tranform()
    @animate_slide('-')

  slide_right: ->
    @current_number += 1
    @current_number = 1 if @current_number > @total_number
    @$('.presentation_slider').append(@template(current_number: @current_number, total_number: @total_number, main: @main, express: @express, marketing_ab: @marketing_ab))
    @$presentation_background = @$('.presentation_background').last()
    @set_init_tranform()
    @animate_slide("+")

  animate_slide: (direction) ->
    return if @animating
    @animating = true
    slide_width = @$el.width()

    $slider_wrapper = @$('.presentation_slider')
    if direction == "+"
      $slider_wrapper.animate
        left: "-=#{slide_width}px"
      , 500, =>
        $slider_wrapper.css('left', '0px')
        $slider_wrapper.find('> li').first().remove()
        @animating = false
    else
      $slider_wrapper.css('left', "-#{slide_width}px")
      $slider_wrapper.animate
        left: "+=#{slide_width}px"
      , 500, =>
        $slider_wrapper.find('> li').last().remove()
        $slider_wrapper.css('left', 0)
        @animating = false

  get_current_number_from_route: ->
    @express = false

    if @current_route.match(/^\/$/) or @current_route.match(/^\/forgot_password/) or @current_route.match(/^\/reset_password/)
      @current_number = 1
    else if @current_route.match(/^\/ege/)
      @current_number = 2
      @express = @current_route.indexOf('ege_express') > 0
    else if @current_route.match(/^\/gia/)
      @current_number = 3
      @express = @current_route.indexOf('gia_express') > 0
    else if @current_route.match(/^\/basic/)
      @current_number = 4
    else if @current_route.match(/^\/advanced/)
      @current_number = 5
    else if @current_route.match(/^\/olymp/)
      @current_number = 6
    else if @current_route.match(/^\/grade_10_ege_phone/)
      @current_number = 8
    else if @current_route.match(/^\/grade_10_ege/)
      @current_number = 7

  change_route_and_scroll_down: ->
    path = switch @current_number
      when 1 then '/'
      when 2 then '/ege'
      when 3 then '/gia'
      when 4 then '/basic'
      when 5 then '/advanced'
      when 6 then '/olymp'
      when 7 then '/grade_10_ege'
      when 8 then '/grade_10_ege_phone'

    Backbone.history.navigate(path, true)

    window_height = $(window).height()
    $(window).scrollTo(window_height, 600,
      easing: 'easeOutQuad'
      axis: 'y'
    )

  scroll_to_info: ->
    $(window).scrollTo($('.fixed_menu_wrapper+div'), 200)
    @
