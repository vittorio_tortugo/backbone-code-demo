class Stoege.Views.Uchebnik extends Backbone.View

  el: '.main_content'

  events:
    'click .scroll-button': 'scrollButtonClickHandler'

  scrollButtonClickHandler: ->
    $(window).scrollTo($('.scroll_button_anchor'), 200)
    @

