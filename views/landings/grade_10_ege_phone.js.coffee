#= require ./grade_10_ege.js.coffee
class Stoege.Views.Grade10EgePhone extends Stoege.Views.Grade10Ege

  initialize: ->
    super
    @$('.phone_block input').mask("+7 (999) 999 99 99", autoclear: false)

  send_gift_request: ->
    $phone_block = @$('.phone_block')

    $.ajax
      type: "POST",
      url: "/grade_10_ege_phone"
      data:
        phone: $phone_block.find('input').val()
        landing_name: 'grade_10_ege_phone'
        type: '1'
      success: =>
        $phone_block.removeClass('error')
        @change_gift_content()
      error: (error)->
        error_message = error.responseJSON['error']
        $phone_block.find('.text_error').html(error_message) if error_message
        $phone_block.addClass('error')

  change_gift_content: ->
    @$('.phone_block_wrapper').addClass('transform')
