class Stoege.Views.ParallaxSlider extends Backbone.View

  el: ".parallax_slider"

  events:
    'click .arrow_left' : 'manualy_slide_left'
    'click .arrow_right' : 'manualy_slide_right'

  initialize: (options) ->
    @current_route =  Backbone.history.fragment.split('?')[0]
    @current_number = 1
    @get_template_from_route()
    @animating = false
    @template = JST["backbone/templates/landings/parallax_slider"]
    @render()
    @set_timing()
    @set_class_for_ie()

  render: ->
    @$el.html(@template(current_number: @current_number, total_number: @total_number, template_name: @template_name, express: @express))

  manualy_slide_left: ->
    clearInterval(@timing) if @timing
    @slide_left()
    @set_timing()

  manualy_slide_right: ->
    clearInterval(@timing) if @timing
    @slide_right()
    @set_timing()

  auto_slide_right: =>
    @slide_right()

  slide_left: ->
    return if @animating
    @animating = true
    @current_number -= 1
    @current_number = @total_number if @current_number < 1
    @$el.prepend(@template(current_number: @current_number, total_number: @total_number, template_name: @template_name, express: @express))
    @$el.find('>li').last().addClass('parallax_transition_left')
    @animate_slide('-')

  slide_right: ->
    return if @animating
    @animating = true
    @current_number += 1
    @current_number = 1 if @current_number > @total_number
    @$el.append(@template(current_number: @current_number, total_number: @total_number, template_name: @template_name, express: @express))
    @$el.find('>li').first().addClass('parallax_transition_right')
    @animate_slide("+")

  animate_slide: (direction) ->
    slide_width = parseInt($('.parallax_slider_wrapper, .main_parallax_slider_wrapper').width())

    if direction == "+"
      $first_slide = @$el.find('> li').first()
      $first_slide.find('.parallax_left_part').animate
        left: "+=#{slide_width}px"
      , 700, 'easeInCubic'

      @$el.animate
        left: "-=#{slide_width}px"
      , 700, 'easeInQuad', =>
        @$el.css('left', '0px')
        @$el.find('> li').first().remove()
        @animating = false
    else
      @$el.css('left', "-#{slide_width}px")

      $last_slide = @$el.find('> li').last()
      $last_slide.find('.parallax_left_part').animate
        left: "-=#{slide_width}px"
      , 700, 'easeInCubic'

      @$el.animate
        left: "+=#{slide_width}px"
      , 700, 'easeInCubic', =>
        @$el.find('> li').last().remove()
        @$el.css('left', 0)
        @animating = false
        @$el.removeClass('parallax_transition')

  set_timing: ->
    @timing = setInterval(@auto_slide_right, 5000)

  get_template_from_route: ->
    @express = false

    switch @current_route
      when '', 'forgot_password', 'reset_password'
        @total_number = 5
        @template_name = 'main_parallax_slider'
      when 'olymp'
        @total_number = 4
        @template_name = 'olymp_parallax_slider'
      when 'ege'
        @total_number = 4
        @template_name = 'ege_parallax_slider'
      when 'ege_express'
        @total_number = 4
        @template_name = 'ege_parallax_slider'
        @express = true
      when 'gia'
        @total_number = 4
        @template_name = 'gia_parallax_slider'
      when 'gia_express'
        @total_number = 4
        @template_name = 'gia_parallax_slider'
        @express = true
      when 'basic'
        @total_number = 3
        @template_name = 'basic_parallax_slider'
      when 'advanced'
        @total_number = 3
        @template_name = 'advanced_parallax_slider'
      when 'parents'
        @total_number = 3
        @template_name = 'parents_parallax_slider'
      when 'grade_10_ege'
        @total_number = 5
        @template_name = 'main_parallax_slider'
      when 'grade_10_ege_phone'
        @total_number = 5
        @template_name = 'main_parallax_slider'

  set_class_for_ie: ->
    ua = window.navigator.userAgent

    @$el.addClass('ie') if ua.indexOf('MSIE ') > 0 or ua.indexOf('Trident/') > 0

