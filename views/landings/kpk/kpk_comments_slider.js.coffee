class Stoege.Views.KpkCommentsSlider extends Stoege.Views.CommentsSlider
  el: '.comments_wrapper'

  events:
    'click .arrow_left' : 'slide_left'
    'click .arrow_right' : 'slide_right'

  template: JST["backbone/templates/landings/kpk_comment_slider_item"]

  initialize: ->
    @animated = true
    @kpk_comments_collection = new Stoege.Collections.KpkCommentsCollection()
    @render_random_comments()

  slide_left: ->
    @direction = "-"
    @render_random_comments()

  slide_right: ->
    @direction = "+"
    @render_random_comments()

  render_random_comments: ->
    boy_comment = @kpk_comments_collection.get_random_boy_comment()
    girl_comment = @kpk_comments_collection.get_random_girl_comment()
    @model.set
      boy_comment: boy_comment.attributes
      girl_comment: girl_comment.attributes
    @render()



