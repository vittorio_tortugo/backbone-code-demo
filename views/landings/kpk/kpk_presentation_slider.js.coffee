#= require ../presentation_slider
class Stoege.Views.KpkPresentationSlider extends Stoege.Views.PresentationSlider

  template: JST["backbone/templates/landings/kpk_presentation_slider"]

  initialize: ->
    @animating = false
    @set_slider_height()
    $(window).resize =>
      @set_slider_height()
    @render()
    @set_init_tranform()
    $(window).scroll =>
      @set_scroll_transform()

  render: ->
    @$('.presentation_slider').html(@template(app_options))
    @$presentation_background = @$('.presentation_background')