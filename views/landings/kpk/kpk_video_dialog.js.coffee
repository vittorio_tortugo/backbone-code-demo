#= require ../../dialogs/dialog.js.coffee
class Stoege.Views.KpkVideoDialogView extends Stoege.Views.Dialog

  className: 'video_dialog'

  open_video: ->
    @close_dialogs()
    video_url = @model.get('url')
    @$el.html "<iframe src='#{video_url}' width='640' height='360' frameborder='0' webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>"
    @open()
    @$el.parent().addClass('kpk_video_dialog_wrapper')

  close_dialogs: ->
    $('.ui-dialog-content').dialog('close')

  destroy: ->
    @$el.html ''
    @$el.dialog('close')
