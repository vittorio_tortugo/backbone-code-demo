class Stoege.Views.Kpk extends Backbone.View

  el: '.landing_kpk_wrapper'

  events:
    'click .play_video': 'play_video'

  main_template: JST["backbone/templates/landings/main"]
  comments_template: JST["backbone/templates/landings/comments"]

  initialize: ->
    user.be_teacher()
    @video_url = 'https://www.youtube.com/embed/1vTHcM2dyHc?autoplay=1&rel=0'
    @clear_previous_page()
    @render()
    @render_comments()
    @comments_model = new Stoege.Models.CommentSlider
    new Stoege.Views.KpkCommentsSlider(model: @comments_model)
    $('body').addClass('landing_kpk')

  clear_previous_page: ->
    @$('.parallax_slider_wrapper').remove()
    @$('.main_parallax_slider_wrapper').remove()
    @$('.courses_wrapper').remove()
    @$('.landing_info_wrapper').remove()
    @$('.plus_menu_wrapper').remove()
    @$('.levels_menu_wrapper').remove()
    @$('.disciplines_menu_wrapper').remove()
    @$('.comments_wrapper').remove()

  render_comments: ->
    @$('.landing_kpk_pluses_wrapper').after(@comments_template())

  play_video: (e) ->
    e.preventDefault()
    @video_dialog_model or= new Stoege.Models.VideoDialogModel(url: @video_url)
    @video_dialog_view or= new Stoege.Views.KpkVideoDialogView(model: @video_dialog_model)
    @video_dialog_view.open_video()
    @