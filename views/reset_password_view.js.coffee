class Stoege.Views.ResetPasswordView extends Stoege.Views.Application

  el: '.reset_password_dialog .dialog_content'

  events:
    'keyup input.password' : 'enable_submit_button'
    'change input.password' : 'enable_submit_button'
    'click input.error' : 'remove_error'

  initialize: (options) ->
    @set_ajax_callbacks()
    @current_path = options.current_path

  set_ajax_callbacks: ->
    @$el
      .on 'ajax:beforeSend', =>
        @show_submit_spinner()
      .on 'ajax:success', (evt, data, status, xhr) =>
        window.location = @current_path
      .on 'ajax:error', (evt, xhr, status, error) =>
        @hide_submit_spinner()
        json = $.parseJSON(xhr.responseText)
        $.each json, (key, value) =>
          $input = @$('input.password')
          @show_error($input, $input.parent(), value[0])

  enable_submit_button: ->
    if @$('input.password').val()
      @$('input.blue_button').prop('disabled', false).removeClass('disabled')
    else
      @$('input.blue_button').prop('disabled', true).addClass('disabled')
