class Stoege.Views.Subscription extends Stoege.Views.Application

  el: '#new_subscriber'

  events:
    'click input.error' : 'remove_error'

  initialize: ->
    @set_ajax_callbacks()

  set_ajax_callbacks: ->
    @$el
      .on 'ajax:beforeSend', =>
        @show_submit_spinner()
      .on 'ajax:success', (evt, data, status, xhr) =>
        @hide_submit_spinner()
        @show_notification()
        @trigger('subscription_success')
      .on 'ajax:error', (evt, xhr, status, error) =>
        @hide_submit_spinner()
        json = $.parseJSON(xhr.responseText)
        $.each json, (key, value) =>
          $input = @$('#subscriber_' + key)
          @show_error($input, $input.parent(), value[0])

  show_notification: ->
    window.notificationCollection.add
      content: "#{you_text('Вы подписаны', 'Ты подписан')} на нашу рассылку"
      status: 'notice'
