class Stoege.Views.SmartBanner extends Backbone.View

  el: '#smartbanner'

  events:
    'click .banner_close_button' : 'close'

  template: JST["backbone/templates/smart_banner"]

  initialize: ->
    @model = new Stoege.Models.SmartBannerModel()

    ios_device = $.browser.ipad
    if ios_device and not $.browser.safari
      @platform = 'ios'
      @model.get_link @platform

      @render_and_open()

      @add_style_params()
      $(window).scroll =>
        @add_style_params()
      $(window).resize =>
        @add_style_params()

  render_and_open: ->
    #24 hours = 86400
    cookie_count = $.cookie('smart_banner_count')
    if cookie_count?
      if parseInt(cookie_count) isnt 2
        time = parseInt(new Date() - new Date($.cookie('smart_banner_date')))/1000
        @open() if time >= 86400
    else
      @open()

    dataLayer?.push('event':'textbook_app_smartbanner_clicked','platform': @platform)

  open: ->
    @$el.html(@template(model: @model.toJSON()))
    @$el.addClass('show')

  close: ->
    cookie_count = $.cookie('smart_banner_count')
    @$el.removeClass('show')

    if cookie_count? then @set_cookie(null, 2) else @set_cookie(new Date(), 1)

  set_cookie: (date, count) ->
    $.cookie('smart_banner_date', date, { path: '/', expires: 20000 }) if date?
    $.cookie('smart_banner_count', count, { path: '/', expires: 20000 }) if count?

  add_style_params: ->
    @$banner_content = @$('.banner_content')
    @$button = @$('.view_button')

    return unless @$banner_content.length

    orientation = if window.innerHeight > window.innerWidth then 'portrait' else 'landscape'
    window_scroll_left = $(window).scrollLeft()
    padding_right = 140
    window_width = @$banner_content.width() + padding_right

    scale =
      if orientation is 'portrait'
        window.innerWidth/(window_width)
      else
        window.innerHeight/(window_width)

    # выставляем позиционирование для контента
    @set_banner_content_position(scale, window_scroll_left)

    # выставляем позиционирование для кнопки
    @set_button_position(scale, orientation, window_scroll_left)

  set_banner_content_position: (scale, window_scroll_left) ->
    # выставляем скейл
    banner_structure_styles = "transform: scale(#{scale});-webkit-transform: scale(#{scale});margin-top: -#{@$banner_content.height()/2}px"
    @$banner_content.attr("style", banner_structure_styles)

    #...после спозиционировали контент вне зависимости от зума
    @$banner_content.css
      left: "#{window_scroll_left - @$banner_content.offset().left}px"

    @$el.attr("style", "height: #{@$banner_content.height() * scale + 20}px;")

  set_button_position: (scale, orientation, window_scroll_left) ->
    @$button.attr("style", "transform: scale(#{scale});-webkit-transform: scale(#{scale});margin-top: -#{(@$button.outerHeight()/2)}px") if scale?

    @$button.css
      right: 'auto'
      left: "#{@$button.offset().left*-1 + window_scroll_left + window.innerWidth - (@$button.outerWidth()+10)*scale}px"
