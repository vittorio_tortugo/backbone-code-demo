class Stoege.Views.Promos extends Backbone.View

  el: '.promos_wrapper'

  promo_template: JST["backbone/templates/promo"]

  initialize: ->
    @render_timer()

  render_timer: ->
    @$('.promos_list > li .timer').each ->
      new Stoege.Views.Timer(el: $(@))
