class Stoege.Views.HeaderView extends Backbone.View

  el: '#header'

  events:
    'click .user_name' : 'show_header_menu'
    'click .contacts_icon' : 'show_contacts_menu'
    'mouseenter .header_menu_wrapper .dropdown': 'dropdown_hover'
    'click .dropdown_trigger': 'toggle_main_menu'
    'click .header_switcher_wrapper': 'show_site_zone_switch_dialog'
    'click .header_logo_wrapper': 'show_site_zone_switch_dialog'

  initialize: ->
    @$el.closest('.wrapper_add').on 'click', (e) =>
      @close_header_menu(e)
      @close_contacts_menu(e)

  show_header_menu: (e) ->
    e.stopPropagation()
    $element = $(e.target)
    @toggle_menu($element, 'user_menu', !$element.closest('.header_user_wrapper').hasClass('active'))
    @

  toggle_main_menu: (event) ->
    @$('.menu').toggleClass('active')
    false

  close_header_menu: (e) ->
    @toggle_menu($(e.target), 'user_menu', false)
    @

  show_contacts_menu: (e) ->
    e.stopPropagation()
    $element = $(e.target)
    @toggle_menu($element, 'contacts_menu', !$element.parent().hasClass('active'))
    @

  close_contacts_menu: (e) ->
    @toggle_menu($(e.target), 'contacts_menu', false)
    @

  toggle_menu: ($element, element_class, flag = true) ->
    if flag
      $menu = $element.siblings(".#{element_class}")
      $menu.parent().toggleClass('active', flag)
      $menu.children('ul').slideToggle('fast')
    else
      $menu = @$el.find(".#{element_class}")
      $menu.children("ul").slideUp 'fast', =>
        $menu.parent().toggleClass('active', flag)
    @

  dropdown_hover: (e) ->
    $target = $(e.target).closest('.dropdown')
    $target.addClass('in_hover')
    _.delay ( =>
      $target.removeClass('in_hover')
      ), 80

  show_site_zone_switch_dialog: ->
    new Stoege.Views.SiteSwitcherDialogView()
    false