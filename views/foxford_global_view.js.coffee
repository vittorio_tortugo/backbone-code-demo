#= require ./global_view
class Stoege.Views.FoxfordGlobalView extends Stoege.Views.GlobalView
  initialize: ->
    super
    @set_pages_routes()
    new Stoege.Views.EmailSubscription() if not user.is_authorized() and window is window.top
    if user.is_authorized() and not user.has_phone() and location.pathname.indexOf('account') < 0
      new Stoege.Views.NotificationPhone()

  set_pages_routes: ->
    $page_routes = $('#page_routes')
    if $page_routes.length
      list_of_page_routes = $page_routes.attr('data-page_routes')
      page_routes =  list_of_page_routes + ','
      page_routes += list_of_page_routes.split(',').join('/:path,') + '/:path,'
      page_routes += list_of_page_routes.split(',').join('/:path/:path,') + '/:path/:path'
      page_routes = page_routes.split(',')

      page_routes_hash = {}

      for page_route in page_routes
        page_routes_hash[page_route] = 'show'

      Stoege.Routers.PagesRouter.prototype.routes = page_routes_hash
