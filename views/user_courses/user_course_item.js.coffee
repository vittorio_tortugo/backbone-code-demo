class Stoege.Views.UserCourseItem extends Backbone.View

  tagName: 'li'
  className: 'user_course'

  template: JST["backbone/templates/user_courses/user_course_item"]

  render: ->
    @$el.html(@template(users_course: @model))
    @
