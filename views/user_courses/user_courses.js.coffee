class Stoege.Views.UserCourses extends Backbone.View

  el: '.users_courses_content'

  events:
    'mouseenter .users_courses_list > li' : 'set_course_hover_color'
    'mouseleave .users_courses_list > li' : 'set_course_default_color'

  initialize: ({@types, @collection}) ->
    @listenTo(@collection, 'sync', @render, @)
    @$spinner = @$('.loading_spinner')
    $(window).scroll =>
      @load_more_courses()

  load_courses: (@params) ->
    @reset_view()
    params['state'] or= 'active'
    @collection.load_courses params

  render: ->
    @collection.each (course) =>
      course_view = new Stoege.Views.UserCourseItem(model: course)
      year = course.get('starts_from')
      @$("[data-year=#{year}] .users_courses_list").append(course_view.render().el)
      @$("[data-year=#{year}]").removeClass 'hidden'

    if @$('.users_courses_wrapper > .users_courses').length - @$('.users_courses_wrapper > .users_courses.hidden').length > 1
      @$('.has_one_visible_year').removeClass('has_one_visible_year')

    @$spinner.addClass('hidden')

    unless @collection.fullCollection.length
      @show_not_found_block()
    @

  show_not_found_block: ->
    @$('.courses_not_found_wrapper').removeClass('hidden')

  load_more_courses: ->
    return unless @collection.hasNextPage() and @collection.length
    scroll_bottom = $(window).scrollTop() + $(window).height()
    course_offset = @$('.users_courses_wrapper li.user_course').last().offset()
    if course_offset and course_offset.top < scroll_bottom
      @$spinner.removeClass('hidden')

      # TODO сейчас делается полный перерендер всех курсов, в идеале можно его не делать
      @collection.getNextPage
        data: @params

  # TODO объединить два метода в один
  set_course_hover_color: (e) ->
    $target = $(e.target).closest('li').find('.course_icon_wrapper')
    dark_color = $target.data('hover_color')
    $target.css('background', "##{dark_color}")

  set_course_default_color: (e) ->
    $target = $(e.target).closest('li').find('.course_icon_wrapper')
    color = $target.data('color')
    $target.css('background', "##{color}")

  reset_view: ->
    @$spinner.removeClass('hidden')
    @$('.users_courses_list')
      .empty()
    @$('users_courses')
      .addClass('hidden')
