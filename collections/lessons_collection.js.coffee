class Stoege.Collections.LessonsCollection extends Backbone.Collection
  model: Stoege.Models.LessonsModel

  set_active_model: (model_id) ->
    active_models = @where active: true
    for model in active_models
      model.set {active: false}, {silent: true}
    (@get(model_id) or @first())?.set active: true

  load: ->
    for lesson in $('.lesson_box .lesson')
      current_lesson_id = parseInt $(lesson).data('lesson_id')
      @add
        id: current_lesson_id
        course_id: @course_id
        progress: $(lesson).find('.fxf_progress_circle').data('progress')
    @
