class Stoege.Collections.LandingCourses extends Backbone.Collection

  url: '/api/landing_courses'

  set_link: (express, level_name) ->
    if level_name is "ЕГЭ" and express
      '/courses?course_type_id=2&grade_id=6'
    else
      "/courses?level_id=#{@.at(0).get('level_id')}"

