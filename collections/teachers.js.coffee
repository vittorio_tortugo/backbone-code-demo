class Stoege.Collections.Teachers extends Backbone.PageableCollection
  model: Stoege.Models.Teacher

  mode: 'infinite'

  state:
    firstPage: 1

  queryParams:
    totalPages: null
    totalRecords: null
    pageSize: null
    vip: true
    directions: 
      "1" :"desc"

  url: "/api/teachers"

  parseLinks: (resp, options) ->
    next: "/api/teachers"
