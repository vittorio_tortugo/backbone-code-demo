class Stoege.Collections.CoursesLibraryList extends Backbone.PageableCollection
  model: Stoege.Models.CoursesLibraryList

  mode: 'infinite'

  state:
    firstPage: 1

  queryParams:
    totalPages: null
    totalRecords: null
    pageSize: null
    directions:
      "1": "desc"

  url: "/api/courses"

  parseLinks: (resp, options) ->
    next: "/api/courses"
