class Stoege.Collections.EventsUsersList extends Backbone.PageableCollection
  model: Stoege.Models.EventsUsersList

  mode: 'infinite'

  state:
    firstPage: 1
    event_id: 0
    pageSize: 11
    totalRecords: 0

  queryParams:
    totalPages: null
    totalRecords: null
    pageSize: null
    directions:
      "1": "desc"

  url: =>
    "/api/events/#{@state.event_id}/user_events"

  parse: (response) ->
    @users_count = response.users_count or 0
    @state.totalRecords = @users_count
    response.users

  parseLinks: (resp, options) ->
    next: "/api/events/#{@state.event_id}/user_events"
