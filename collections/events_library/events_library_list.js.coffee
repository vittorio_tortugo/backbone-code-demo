class Stoege.Collections.EventsLibraryList extends Backbone.PageableCollection
  model: Stoege.Models.EventsLibraryList

  mode: 'infinite'

  state:
    firstPage: 1

  queryParams:
    totalPages: null
    totalRecords: null
    pageSize: null
    directions:
      "1": "desc"

  url: "/api/events"

  parseLinks: (resp, options) ->
    next: "/api/events"
