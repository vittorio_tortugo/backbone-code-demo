class Stoege.Collections.Rating extends Backbone.PageableCollection
  model: Stoege.Models.Rating

  mode: 'infinite'

  state:
    firstPage: 1

  queryParams:
    totalPages: null
    totalRecords: null
    pageSize: null

  url: "/api/rating"

  parseLinks: (resp, options) ->
    next: "/api/rating"