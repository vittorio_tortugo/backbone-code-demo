class Stoege.Collections.Notification extends Backbone.Collection
  model: Stoege.Models.Notification

  add_error: (text) ->
    @add
      status: 'alert'
      content: text

  add_warning: (text) ->
    @add
      status: 'warning'
      content: text

  add_success: (text) ->
    @add
      status: 'notice'
      content: text
