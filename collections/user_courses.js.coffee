class Stoege.Collections.UserCourses extends Backbone.PageableCollection
  model: Stoege.Models.UserCourses

  mode: 'infinite'

  state:
    firstPage: 1

  queryParams:
    totalPages: null
    totalRecords: null
    pageSize: null

  url: '/api/users_courses'
    
  parseLinks: (resp, options) ->
    next: '/api/users_courses'

  load_courses: (params) ->
    @state.currentPage = 1
    @fullCollection.reset()
    @getFirstPage
      data: params
      fetch: true