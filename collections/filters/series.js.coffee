#= require ./_filterable_collection
class Stoege.Collections.Series extends Stoege.Collections.FilterableCollection
  model: Stoege.Models.FilterableModel

  initialize: ->
    for item in $('#filter_series > option')
      @add
        id: $(item).attr('value')
        selected: $(item).attr('selected')
        text: $(item).text()