#= require ./_filterable_collection
class Stoege.Collections.Types extends Stoege.Collections.FilterableCollection
  model: Stoege.Models.FilterableModel

  initialize: ->
    if $('.types_menu').length
      for type in $('.types_menu > li')
        $type = $(type)
        @add
          id: $type.data('id')
          selected: $type.hasClass('selected')
          name: $type.find('span').html()
    else
      for type in $('#filter_types > option')
        $type = $(type)
        @add
          id: $type.val()
          selected: $type.attr('selected')
          name: $type.text()