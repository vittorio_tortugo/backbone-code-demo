#= require ./_filterable_collection
class Stoege.Collections.Grades extends Stoege.Collections.FilterableCollection
  model: Stoege.Models.FilterableModel

  initialize: ->
    for grade in $('#filter_grades > option')
      $grade = $(grade)
      @add
        id: $grade.attr('value')
        selected: $grade.attr('selected')
        text: $grade.text()