#= require ./_filterable_collection
class Stoege.Collections.States extends Stoege.Collections.FilterableCollection
  model: Stoege.Models.FilterableModel

  initialize: ->
    if $('.states_menu').length
      for state in $('.states_menu > li')
        $state = $(state)
        @add
          id: $state.data('id')
          selected: $state.hasClass('selected')
          name: $state.find('span').html()
    else if $('#filter_states').length
      for state in $('#filter_states > option')
        $state = $(state)
        @add
          id: $state.attr('value')
          selected: $state.attr('selected')
          text: $state.text()