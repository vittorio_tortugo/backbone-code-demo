#= require ./_filterable_collection
class Stoege.Collections.Disciplines extends Stoege.Collections.FilterableCollection
  model: Stoege.Models.FilterableModel

  initialize: ->
    if $('.disciplines').length
      for discipline in $('.disciplines > li')
        @add
          id: $(discipline).data('id')
          selected: $(discipline).hasClass('selected')
          text: $(discipline).find('.text').html()
          color: $(discipline).find('.mark').css('background-color')
    else if $('#filter_disciplines').length
      for discipline in $('#filter_disciplines > option')
        @add
          id: $(discipline).attr('value')
          selected: $(discipline).attr('selected')
          text: $(discipline).text()