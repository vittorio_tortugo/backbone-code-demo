#= require ./_filterable_collection
class Stoege.Collections.UserTypes extends Stoege.Collections.FilterableCollection
  model: Stoege.Models.FilterableModel

  initialize: ->
    for user_type in $('#filter_user_types > option')
      @add
        id: $(user_type).val()
        selected: $(user_type).attr('selected')
        text: $(user_type).text()
