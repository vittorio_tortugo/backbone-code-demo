class Stoege.Collections.FilterableCollection extends Backbone.Collection
  change_selected_on: (id) ->
    item = @get id
    if item
      @disable_selected()
      item.set_selected()
      return true
    return false

  get_selected_item: ->
    @findWhere selected: true

  get_id_of_selected: ->
    @get_selected_item()?.get 'id'

  set_selected_by: (id) ->
    @get(id)?.set_selected()

  disable_selected: ->
    item = @get_selected_item()
    item?.set('selected', false)