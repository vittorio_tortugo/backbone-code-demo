#= require ./_filterable_collection
class Stoege.Collections.Regions extends Stoege.Collections.FilterableCollection
  model: Stoege.Models.FilterableModel

  initialize: ->
    for region in $('#filter_regions > option')
      $region = $(region)
      @add
        id: $region.attr('value')
        selected: $region.attr('selected')
        text: $region.text()