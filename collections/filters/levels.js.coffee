#= require ./_filterable_collection
class Stoege.Collections.Levels extends Stoege.Collections.FilterableCollection
  model: Stoege.Models.FilterableModel

  initialize: ->
    for level in $('#filter_levels > option')
      @add
        id: $(level).val()
        selected: $(level).attr('selected')
        text: $(level).text()