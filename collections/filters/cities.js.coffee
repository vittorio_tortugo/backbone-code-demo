#= require ./_filterable_collection
class Stoege.Collections.Cities extends Stoege.Collections.FilterableCollection
  model: Stoege.Models.FilterableModel

  url: -> "#{@root}/api/cities"

  initialize: ({@root})->