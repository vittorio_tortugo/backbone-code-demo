class Stoege.Collections.TeacherComments extends Backbone.PageableCollection

  model: Stoege.Models.Comment

  mode: 'infinite'

  state:
    firstPage: 1

  queryParams:
    totalPages: null
    totalRecords: null
    pageSize: 3

  initialize: (options) ->
    @teacher_id = options.teacher_id

  url: ->
    "/api/teachers/#{@teacher_id}/comments/"

  parseLinks: (resp, options) ->
    next: "/api/teachers/#{@teacher_id}/comments/"
