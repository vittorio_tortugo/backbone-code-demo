class Stoege.Collections.Comments extends Backbone.PageableCollection

  model: Stoege.Models.Comment

  mode: 'infinite'

  state:
    firstPage: 1

  queryParams:
    totalPages: null
    totalRecords: null

  url: ->
    "/api/courses/#{@course_id}/comments/"

  parseLinks: (resp, options) ->
    next: "/api/courses/#{@course_id}/comments/"