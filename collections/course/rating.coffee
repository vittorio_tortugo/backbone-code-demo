class Stoege.Collections.CourseRating extends Backbone.PageableCollection
  model: Stoege.Models.Rating

  mode: 'infinite'

  state:
    firstPage: 1

  queryParams:
    totalPages: null
    totalRecords: null
    pageSize: null

  url: ->
    "/api/courses/#{@course_id}/ratings"

  parse: (response) ->
    @user_rating = response.user_rating
    response.ratings

  parseLinks: (resp, options) ->
    next: "/api/courses/#{@course_id}/ratings"