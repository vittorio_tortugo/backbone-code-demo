class Stoege.Collections.TopMenuCollection extends Backbone.Collection
  initialize: ->
    @add title: 'Программа', id: 'program', icon: 'fxf_icon_program_blue', icon_active: 'fxf_icon_program', active: false
    @add title: 'О курсе', id: 'about', icon: 'fxf_icon_info_blue', icon_active: 'fxf_icon_info', active: false
    @add title: 'Рейтинг', id: 'rating', icon: 'fxf_icon_rating_blue', icon_active: 'fxf_icon_rating', active: false
    @add title: 'Отзывы', id: 'comments', icon: 'fxf_icon_comments_blue', icon_active: 'fxf_icon_comments', active: false

  set_active_model: (model_id) ->
    active_models = @where active: true
    for model in active_models
      model.set {active: false}, {silent: true}
    (@get(model_id) or @first()).set active: true