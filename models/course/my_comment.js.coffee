class Stoege.Models.MyComment extends Stoege.Models.Comment

  defaults:
    created_at: new Date().format("dd.mm.yyyy")
    commentable_type: "Course"
    text: ''
    point: 0

    user_id: ->
      user.get 'id'

    commentable_id: ->
      @course.get('id')

  urlRoot: ->
    "/api/courses/#{@course.get('id')}/comments"

  initialize: (options) ->
    @course = options.course
    @set(user_avatar_url: user.get('medium_avatar_url'))
