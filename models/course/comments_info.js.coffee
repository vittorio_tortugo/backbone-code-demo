class Stoege.Models.CommentInfo extends Backbone.Model
  url: ->
    "/api/courses/#{@course_id}/comments_info"

  initialize: ({@course_id}) ->