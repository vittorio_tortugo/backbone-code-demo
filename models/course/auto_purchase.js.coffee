class Stoege.Models.AutoPurchase extends Backbone.Model
  
  urlRoot: ->
    "/api/courses/#{@course_id}/auto_purchase"

  initialize: (options) ->
    @course_id = options.course_id

  load: ->
    @set(auto_purchase: $(".paid_access_wrapper .auto_transfer").data('auto_purchase'))

  toggle: ->
    @set(auto_purchase: !@get('auto_purchase'))
    @save({auto_purchase: @toJSON()}, type: 'PUT')
