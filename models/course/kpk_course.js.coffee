class Stoege.Models.KpkCourse extends Backbone.Model
  url: ->
    "/api/courses/#{@course_id}/users_courses"

  initialize: ({@course_id}) ->