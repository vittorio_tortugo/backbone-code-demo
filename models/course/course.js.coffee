class Stoege.Models.Course extends Backbone.Model
  # TODO отрефакторить модель, сделать полноценную работу с данными
  # (fetch вместо парсинга dom) + сделать kpk полноценной моделью
  defaults:
    id: null

  urlRoot: '/api/courses'

  initialize: ->
    @lessons_collection or= new Stoege.Collections.LessonsCollection
    @top_menu_collection or= new Stoege.Collections.TopMenuCollection
    @lessons_collection.course = @
    @lessons_collection.course_id = @id
    @lessons_collection.load()

  # TODO: deprecated
  load: ->
    @set(comments_count: $(".info .rating_wrapper .value.pseudo").data('without_user_comment'))
    @set(average_points: $(".info .rating_wrapper .rating_stars .rating_progress").data("average_points"))
    @set(access: $(".course_header_info").data("access"))
    @trigger('loaded')

  activate_tab: (tab_id, lesson_id) ->
    active_lesson_id = @lessons_collection.findWhere(active: true)?.id or lesson_id or @lessons_collection.first().id
    #TODO тут какая то магия с порядком вызвова методом нужно отрефакторить в http://jira.netology-group.ru/browse/STOEGE-5252
    @lessons_collection.set_active_model active_lesson_id
    @top_menu_collection.set_active_model tab_id
