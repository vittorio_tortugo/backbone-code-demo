class Stoege.Models.User extends Backbone.Model

  be_teacher: ->
    $.cookie('is_teacher', true, { path: '/', expires: 7 })

  be_pupil: ->
    $.removeCookie('is_teacher')
    $.removeCookie('is_teacher', { path: window.location.pathname })

  fetch: ->
    @set(_.clone window.app_options?.user)
    @trigger 'sync'

  has_phone: ->
    @get('has_phone')

  is_authorized: ->
    !!@get 'id'

  is_teacher: ->
    @get('is_teacher') or ($.cookie('is_teacher') and not @is_authorized())
