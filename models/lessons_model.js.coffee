class Stoege.Models.LessonsModel extends Backbone.Model
  defaults:
    active: false

  url: ->
    "/api/courses/#{@get 'course_id'}/lessons/#{@id}"

  parse: (@response) ->
    @registration_data_url = '/?registration=true'
    @registration_classes = 'navigation_link registration after_sign_in_path'
    @purchase_classes = 'navigation_link'
    @pay_data_url = "/courses/#{@get 'course_id'}/pay"

    @response.webinar_status_class = 'disabled' if @response.webinar_status is 'not_available'

    @response.title_formatted = "<b>#{(title = @response.title.split(' ')).shift()}</b> #{title.join(' ')}"
    @response.is_finished_webinar = @response.webinar_status in ['video_available', 'video_not_available', 'video_expected']

    @response.need_show_homework = @need_show_homework()
    @response.need_show_homework_progress = @need_show_homework_progress()
    @response.need_show_conspect = @need_show_conspect()

    # параметры для домашней работы
    if @response.is_finished_webinar
      @update_homework_response()
    else
      if @response.access_future_lessons
        if @response.task_expected and @response.tasks_published
          @update_homework_response()
        else
          @response.homework_classes = 'disabled'
      else
        @response.homework_classes = 'disabled'

    # параметры для теоретического материала
    if not @response.is_finished_webinar or (@response.is_finished_webinar and not @response.conspect_blocks_count > 0)
      @response.conspect_classes = 'disabled'
    else
      if @response.access_state is 'not_registered'
        @update_conspect_response(@registration_classes, @registration_data_url)
      else if response.access_state is 'not_purchased'
        @update_conspect_response(@purchase_classes, @pay_data_url)

    # TODO: переделать эту хрень!111
    if @response.webinar_status in ['video_available', 'webinar_ready_to_start', 'webinar_started']
      if @response.access_state is 'not_purchased'
        @response.webinar_classes = @registration_classes
      else if @response.access_state is 'not_registered'
        @response.webinar_classes = @registration_classes
      else if @response.access_state isnt 'not_registered'
        @response.webinar_classes = 'video_available'
    else
      @response.webinar_classes = 'disabled'

    @response

  update_homework_response: ->
    if @response.access_state is 'not_registered'
      @response.homework_classes = @registration_classes
      @response.homework_data_url = @registration_data_url
      @response.webinar_data_url = @registration_data_url
    else if @response.access_state is 'not_purchased'
      @response.homework_classes = @purchase_classes
      @response.homework_data_url = @pay_data_url
      @response.webinar_data_url = @pay_data_url

  update_conspect_response: (classes, data_url) ->
    @response.conspect_classes = classes
    @response.conspect_data_url = data_url
    @response.webinar_data_url = data_url

  need_show_homework: ->
    return true unless @response.is_finished_webinar
    return @response.task_expected and @response.tasks_published

  need_show_homework_progress: ->
    return true if @response.is_finished_webinar
    return @response.task_expected and @response.tasks_published

  need_show_conspect: ->
    return true unless @response.is_finished_webinar
    return @response.conspect_expected