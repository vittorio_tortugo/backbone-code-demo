class Stoege.Models.UserCourses extends Backbone.Model

  course_progress_class: ->
    if @get('progress') >= 0 and  @get('progress') <= 33
      'progress_background_33'
    else if @get('progress') >= 33 and  @get('progress') <= 66
      'progress_background_66'
    else
      'progress_background_100'
