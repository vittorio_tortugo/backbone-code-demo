class Stoege.Models.AuthorizationDialogModel extends Backbone.Model
  defaults:
    social: false

  initialize: ->
    content = $('#header #authorization_form').html()
    $('#header #authorization_form').remove()
    @set(content: content)
