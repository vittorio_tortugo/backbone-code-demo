class Stoege.Models.RegistrationDialog extends Backbone.Model
  defaults:
    provider: ''
    authorization: false
    role: 'pupil'

  url: '/user/registration'
