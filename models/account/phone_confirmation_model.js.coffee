class Stoege.Models.AccountPhoneConfirmationModel extends Backbone.Model
  defaults:
    phone_confirmed: null
    state: 'not_confirmed'
  url: '/api/user/phone_confirmation'
  parse: (response)->
    response.state = if response.phone_confirmed then 'confirmed' else 'not_confirmed'
    response