class Stoege.Models.AccountPhoneModel extends Backbone.Model

  defaults:
    id: 0
    phone: ''

  validate: true

  url: '/api/user/phone'

  validate: (attrs, options) ->
    if attrs.phone.length isnt 10
      'Должно быть 10 цифр'

  clear_phone: (phone) ->
    phone.replace(/[^\d]/g, '').substr(1)