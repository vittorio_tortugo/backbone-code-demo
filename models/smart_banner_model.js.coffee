class Stoege.Models.SmartBannerModel extends Backbone.Model

  defaults:
    title: 'Фоксфорд Курсы'
    company: 'OLC «Netology group» LLC'

  get_link: (platform)->
    if platform is 'ios'
      @set('platform', 'ios')
      @set('price', 'Бесплатно - в App Store')
      @set('link', 'https://itunes.apple.com/app/apple-store/id979854305?pt=99690805&ct=smart_banner&mt=8')
