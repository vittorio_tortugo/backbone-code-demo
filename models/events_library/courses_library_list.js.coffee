class Stoege.Models.EventsLibraryList extends Backbone.Model
  defaults:
    grades_grouped: ''
    discipline:
      color: '666'
      dark_color: '888'

  parse: (response) ->
    unless response.discipline
      response.discipline = _.extend @defaults.discipline, small_image: response.default_discipline_image
    response.grades_grouped = @get_grades_grouped response.grades

    response

  get_grades_grouped: (grades) ->
    if (grades_sorted = _.pluck(grades, 'index').sort((a, b) -> a - b)).length
      if grades_sorted.length is 8
        ''
      else if grades_sorted.length > 1
        "#{grades_sorted[0]}–#{grades_sorted[grades_sorted.length - 1]} классы"
      else if grades_sorted.length is 1
        "#{grades_sorted[0]} класс"
      else
        ''
    else
      ''