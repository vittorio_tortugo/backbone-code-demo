class Stoege.Models.Rating extends Backbone.Model

  set_place: (position) ->
    switch position
      when 1 then 'first'
      when 2 then 'second'
      when 3 then 'third'
      else ''

  parse: (response) ->
    response.place = @set_place(response.position)
    return response

