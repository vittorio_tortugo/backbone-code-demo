class Stoege.Models.Teacher extends Backbone.Model
  initialize: (options) ->
    @alias_url = options.alias_url

  defaults:
    courses_opened: false
    comments_opened: false
    animation: false

  url: ->
    "/api/teachers/#{@alias_url}"
