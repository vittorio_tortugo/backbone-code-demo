class Stoege.Models.TeacherMyComment extends Stoege.Models.Comment

  defaults:
    updated_at: new Date().format("dd.mm.yyyy")
    commentable_type: "Teacher"
    approved_status: 'На модерации'
    text: ''
    point: 0

    user_id: ->
      user.get 'id'

    commentable_id: ->
      @teacher.get('id')

  urlRoot: ->
    "/api/teachers/#{@teacher.get('id')}/comments"

  initialize: (options) ->
    @teacher = options.teacher
    @set(user_avatar_url: user.get('medium_avatar_url'))
