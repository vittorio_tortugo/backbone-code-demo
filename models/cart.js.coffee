class Stoege.Models.Cart extends Backbone.Model
  urlRoot: ->
    return if @is_rapida()
      "/api/courses/#{@course_id}/carts/rapida_templates"
    return if @is_qiwi()
      "/api/courses/#{@course_id}/carts/qiwi_bills"
    return "/api/courses/#{@course_id}/carts"

  initialize: (options) ->
    @course_id = options.course_id

  apply_promocode: (promo_code, promo_data) ->
    @set('code', promo_code)
    for key, value of promo_data
      @set key, value

  toJSON: ->
    attributes = _.clone(@attributes)
    if @is_rapida() or @is_qiwi()
      attributes.first_name = user.get 'first_name'
      attributes.last_name = user.get 'last_name'
      attributes.cart_item_type = @get('payment_type')
      attributes = _.pick(attributes, 'first_name', 'last_name', 'cart_item_type', 'phone', 'code')
      attributes
    else
      attributes.cart_item_type = @get('payment_type')
      attributes = _.pick(attributes, 'payment_method', 'auto_purchase', 'code', 'code_campaign_type', 'discount_type', 'cart_item_type')
      cart: attributes

  get_invite_type_title: ->
    switch @get('discount_type')
      when 'entire_course' then 'курс'
      when 'month' then 'месяц занятий'
      when 'week' then 'неделя занятий'
      when 'day' then 'день занятий'
      else ''

  parse: (response) ->
    response.payment_type = response.available_cart_item_types[0] if response.available_cart_item_types
    response.payment_method = response.available_payment_options[0] if response.available_payment_options
    response

  auto_purchase_available: ->
    ((@get('payment_method') is 'card_payment') or
      (@get('payment_method') is 'yandex_payment')) and !@has('code')

  get_hidden_payments: ->
    salon_payment_provider = @get('payment_providers')['salon_payment_provider']
    if salon_payment_provider is 'Rapida'
      _.difference(@get('available_payment_options'), ['card_payment', 'salon_payment'])
    else
      _.difference(@get('available_payment_options'), ['card_payment'])

  get_big_payments_supports: ->
    _.map(@get('big_payments_support'), (num)-> num.replace('_provider', ''))

  get_full_price: ->
    parseInt(@get('full_price').replace(' ', ''))

  is_rapida: ->
    @get('payment_method') is 'salon_payment' and @get('payment_providers')?.salon_payment_provider is 'Rapida'

  is_qiwi: ->
    @get('payment_method') is 'qiwi_payment' and @get('payment_providers')?.qiwi_payment_provider is 'QIWI'

  disable_auto_purchase: ->
    $.ajax
      url:  "/api/courses/#{@course_id}/auto_purchase"
      method: 'PUT'
