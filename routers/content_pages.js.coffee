class Stoege.Routers.ContentPages extends Backbone.Router
  routes:
    'how_to_learn(/)' : 'content_pages'
    'our_partners(/)' : 'content_pages'
    'media(/)' : 'content_pages'
    'prices(/)' : 'content_pages'
    'about_foxford(/)' : 'content_pages'
    'contacts(/)': 'content_pages'

  content_pages: ->
    @close_dialogs()
    
  close_dialogs: ->
    $('.ui-dialog-content').dialog('close')
    @