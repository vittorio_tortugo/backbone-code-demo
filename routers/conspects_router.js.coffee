class Stoege.Routers.ConspectsRouter extends Backbone.Router
  routes:
    'lessons/:id/conspects/:id': 'show'

  loaded: false
  from_url: true

  show: =>
    @load_scripts()
    @slide()

  load_scripts: ()=>
    unless @loaded
      new Stoege.Views.PageView()
      @loaded = true

  slide: ()=>
    unless @from_url
      pathname = window.location.pathname
      Stoege.Views.PageView.prototype.show_page(pathname)
    @from_url = false
    return false
