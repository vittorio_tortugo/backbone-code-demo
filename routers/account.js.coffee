class Stoege.Routers.Account extends Backbone.Router
  routes:
    'account': 'index'

  index: ->
    @phone_model or= new Stoege.Models.AccountPhoneModel
    @phone_confirmation_model or= new Stoege.Models.AccountPhoneConfirmationModel
    @phone_view or= new Stoege.Views.Account.PhoneView(phone_model: @phone_model, phone_confirmation_model: @phone_confirmation_model)
    @account_view or= new Stoege.Views.Account(phone_view: @phone_view)
    @
