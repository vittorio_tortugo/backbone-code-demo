class Stoege.Routers.Rating extends Backbone.Router

  routes:
    'rating': 'index'

  loaded: false

  index: (params = {}) ->
    @params = _.clone(params)
    @grades or= new Stoege.Collections.Grades
    @disciplines or= new Stoege.Collections.Disciplines
    @ratings = new Stoege.Collections.Rating

    @user_ratings or= new Stoege.Collections.UserRating

    @filter_view or= new Stoege.Views.LibraryFilter(collection_disciplines: @disciplines, collection_grades: @grades, root_name: 'rating', params: @params)

    @rating_view.remove_view() if @rating_view
    @rating_view = new Stoege.Views.RatingList(grades: @grades, collection: @ratings, user_ratings: @user_ratings)

    @rating_view.load_rating(@params)

    @close_dialogs()
    @

  close_dialogs: ->
    $('.ui-dialog-content').dialog('close') if $('.ui-dialog-content').length
    @
