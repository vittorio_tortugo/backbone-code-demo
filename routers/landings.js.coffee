class Stoege.Routers.Landings extends Backbone.Router
  routes:
    '': 'main'
    'olymp(/)' : 'landing'
    'ege(/)' : 'landing'
    'ege_express(/)' : 'landing'
    'gia(/)' : 'landing'
    'gia_express(/)' : 'landing'
    'basic(/)' : 'landing'
    'advanced(/)' : 'landing'
    'grade_10_ege(/)' : 'grade_10_ege'
    'grade_10_ege_phone(/)' : 'grade_10_ege_phone'
    'partner_promo(/)': 'partner_promo'
    'parents(/)': 'parents'
    'uchebnik(/)': 'uchebnik'
    'biglion_promo': 'biglion_promo'
    'hello_youtube(/)': 'hello_youtube'
    'hi_youtube(/)': 'hello_youtube'
    'express_promo(/)': 'express_promo'
    'kpk(/)': 'kpk'
    'mobile(/)': 'mobile'

  main: ->
    new Stoege.Views.Main
    @presentation_slider or= new Stoege.Views.PresentationSlider(main: true)
    new Stoege.Views.VideoPlayer() if $('#main_video').length
    @close_dialogs()

  landing: ->
    @page_model or= new Stoege.Models.Page
    new Stoege.Views.Landing(page: @page_model)
    @presentation_slider or= new Stoege.Views.PresentationSlider(main: false)
    @fading_slider or= new Stoege.Views.FadingSlider
    new Stoege.Views.VideoPlayer() if $('#main_video').length
    @close_dialogs()

  partner_promo: ->
    new Stoege.Views.PartnerPromo
    @close_dialogs()

  parents: ->
    new Stoege.Views.Parents
    @fading_slider or= new Stoege.Views.FadingSlider
    @fixed_menu or= new Stoege.Views.FixedMenu
    @presentation_slider or= new Stoege.Views.PresentationSlider(main: false)
    new Stoege.Views.VideoPlayer() if $('#main_video').length
    @close_dialogs()

  uchebnik: ->
    new Stoege.Views.Uchebnik
    @fixed_menu or= new Stoege.Views.FixedMenu()
    @close_dialogs()

  biglion_promo: ->
    new Stoege.Views.BiglionPromo()
    @close_dialogs()

  hello_youtube: ->
    @set_grade()
    new Stoege.Views.GradesSelect(collection: @grades)
    new Stoege.Views.HelloYoutube(collection: @grades)
    @close_dialogs()

  express_promo: (params={}) ->
    new Stoege.Views.ExpressPromo(params: params)
    new Stoege.Views.Timer()

  kpk: ->
    new Stoege.Views.Kpk
    @fixed_menu or= new Stoege.Views.FixedMenu
    @presentation_slider or= new Stoege.Views.KpkPresentationSlider(main: false)
    @close_dialogs()

  grade_10_ege: ->
    new Stoege.Views.Grade10Ege
    @fading_slider or= new Stoege.Views.FadingSlider
    @fixed_menu or= new Stoege.Views.FixedMenu
    @presentation_slider or= new Stoege.Views.PresentationSlider(main: false)
    new Stoege.Views.VideoPlayer() if $('#main_video').length
    @close_dialogs()

  grade_10_ege_phone: ->
    new Stoege.Views.Grade10EgePhone
    @fading_slider or= new Stoege.Views.FadingSlider
    @fixed_menu or= new Stoege.Views.FixedMenu
    @presentation_slider or= new Stoege.Views.PresentationSlider(main: false)
    new Stoege.Views.VideoPlayer() if $('#main_video').length
    @close_dialogs()

  mobile: ->
    new Stoege.Views.Ios()
    @presentation_slider or= new Stoege.Views.IosPresentationSlider(main: false)
    @advantages_slider or= new Stoege.Views.IosAdvantagesSlider()
    @fixed_menu or= new Stoege.Views.FixedMenu
    @close_dialogs()

  close_dialogs: ->
    $('.ui-dialog-content').dialog('close')
    @

  set_grade: ->
    @grades = new Stoege.Collections.GlobalGrades()

