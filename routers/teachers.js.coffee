class Stoege.Routers.Teachers extends Backbone.Router

  routes:
    'teachers(/)(/:alias_url)': 'index'

  index: (alias_url, params = {}) ->
    teachers = new Stoege.Collections.Teachers
    @teachers_list_view.remove_view() if @teachers_list_view
    @teachers_list_view = new Stoege.Views.TeachersList(collection: teachers, alias_url: alias_url, params: params)

    @collection_disciplines = new Stoege.Collections.Disciplines
    @discipline_view = new Stoege.Views.LibraryFilter(collection_disciplines: @collection_disciplines, root_name: 'teachers', params: params) unless @discipline_view?

    @close_dialogs()

  close_dialogs: ->
    $('.ui-dialog-content').dialog('close') if $('.ui-dialog-content').length
