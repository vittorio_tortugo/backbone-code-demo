class Stoege.Routers.CoursesLibrary extends Backbone.Router
  routes:
    'courses(/)': 'index'

  index: (params = {}) =>
    @params = _.clone(params)
    @types or= new Stoege.Collections.Types
    @grades or= new Stoege.Collections.Grades
    @levels or= new Stoege.Collections.Levels
    @disciplines or= new Stoege.Collections.Disciplines
    @courses = new Stoege.Collections.CoursesLibraryList
    @user_types = new Stoege.Collections.UserTypes

    @filter_view or= new Stoege.Views.LibraryFilter(collection_disciplines: @disciplines, collection_user_types: @user_types, collection_levels: @levels, collection_types: @types, collection_grades: @grades, root_name: 'courses', params: @params)
    @courses_library_view?.remove_view()
    @courses_library_view = new Stoege.Views.CoursesLibraryList
      collection_grades: @grades
      collection: @courses
      params: @params
    @close_dialogs()

    if params['user_type_changes']
      Backbone.history.navigate('/courses', false)
      new Stoege.Views.UserTypeChangesDialogView

  close_dialogs: ->
    $ui_dialog_content.dialog('close') if ($ui_dialog_content = $('.ui-dialog-content')).length
