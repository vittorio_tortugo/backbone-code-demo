class Stoege.Routers.PasswordRouter extends Backbone.Router
  routes:
    'forgot_password': 'new'
    'reset_password': 'edit'

  initialize: ->
    @forgotPasswordDialogModel = new Stoege.Models.ForgotPasswordDialogModel
    @forgotPasswordDialogView = new Stoege.Views.ForgotPasswordDialogView(model: @forgotPasswordDialogModel)
    @resetPasswordDialogModel = new Stoege.Models.ResetPasswordDialogModel
    @resetPasswordDialogView = new Stoege.Views.ResetPasswordDialogView(model: @resetPasswordDialogModel)

  new: ->
    new Stoege.Views.Main
    @presentation_slider or= new Stoege.Views.PresentationSlider(main: true)
    new Stoege.Views.VideoPlayer() if $('#main_video').length
    @close_dialogs()
    @forgotPasswordDialogView.open_forgot_password()

  edit: (params) ->
    new Stoege.Views.Main
    @presentation_slider or= new Stoege.Views.PresentationSlider(main: true)
    new Stoege.Views.VideoPlayer() if $('#main_video').length
    @close_dialogs()
    @resetPasswordDialogView.open_reset_password(params)

  close_dialogs: ->
    $('.ui-dialog-content').dialog('close')
