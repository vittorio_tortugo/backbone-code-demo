class Stoege.Routers.FeedbackRouter extends Backbone.Router
  routes:
    'feedback(/)': 'new'

  new: ->
    feedback_dialog_view = new Stoege.Views.FeedbackDialogView
    feedback_dialog_view.open_feedback()

  close_dialogs: ->
    $('.ui-dialog-content').dialog('close')
