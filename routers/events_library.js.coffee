class Stoege.Routers.EventsLibrary extends Backbone.Router
  routes:
    'events(/)': 'index'
    'events/:id(/)': 'show'

  index: (params = {}) =>
    @params = _.clone(params)
    @grades = new Stoege.Collections.Grades unless @grades?
    @series = new Stoege.Collections.Series unless @series?
    @states = new Stoege.Collections.States unless @states?
    @disciplines = new Stoege.Collections.Disciplines unless @disciplines?
    @events_list = new Stoege.Collections.EventsLibraryList unless @events_list?

    @filter_view = new Stoege.Views.LibraryFilter(collection_states: @states, collection_series: @series, collection_disciplines: @disciplines, collection_grades: @grades, root_name: 'events', params: @params) unless @filter_view?

    @events_library_view.remove_view() if @events_library_view
    @events_library_view = new Stoege.Views.EventsLibraryList
      grades: @grades
      series: @series
      states: @states
      disciplines: @disciplines
      collection: @events_list
      params: @params
      filter_view: @filter_view
    @close_dialogs()

  show: (event_id) =>
    @users_collection = new Stoege.Collections.EventsUsersList unless @users_collection?
    @users_collection.state.event_id = event_id
    @event_page_view.remove_view() if @event_page_view
    @event_page_view = new Stoege.Views.EventsPageView
      event_id: event_id
      users_collection: @users_collection

    @close_dialogs()
    @

  close_dialogs: ->
    $('.ui-dialog-content').dialog('close') if $('.ui-dialog-content').length
