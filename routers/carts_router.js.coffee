class Stoege.Routers.CartRouter extends Backbone.Router
  routes:
    'courses/:course_id/pay': 'open_cart'

  open_cart: (course_id) ->
    @cart = new Stoege.Models.Cart(course_id: course_id)
    @cartView = new Stoege.Views.CartDialog(model: @cart)
