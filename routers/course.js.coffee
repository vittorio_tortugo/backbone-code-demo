class Stoege.Routers.Course extends Backbone.Router
  routes:
    'courses/:id/:tab_id/:lesson_id(/)': 'show_course'
    'courses/:id/:tab_id(/)': 'show_course'
    'courses/:id(/)': 'show_course'

  show: (@course_id, @tab_id, @lesson_id) ->
    @tab_id or= 'program'
    @course_model or= new Stoege.Models.Course(id: @course_id)
    @course_view or= new Stoege.Views.Course(course_model: @course_model)
    @top_menu_view or= new Stoege.Views.CourseLessonTopMenuView(course_model: @course_model)
    @lesson_id or= @course_model.lessons_collection.first()?.id
  
  init_course: ->
    @course_model = new Stoege.Models.Course(id: @course_id) unless @course_model?
    @course_view = new Stoege.Views.Course(course_model: @course_model) unless @course_view?
    @top_menu_view = new Stoege.Views.CourseLessonTopMenuView(course_model: @course_model) unless @top_menu_view?

  show_course: (@course_id, @tab_id, @lesson_id) ->
    @init_course()
    @top_menu_view.render()
    @course_model.activate_tab(@tab_id, @lesson_id)
    @course_view.render()
    @close_ui_dialogs()
    @set_google_analytics()

  close_ui_dialogs: ->
    $('.ui-dialog-content').dialog('close') if $('.ui-dialog-content').length

  set_google_analytics: ->    
    ga 'set', 'page', window.location.pathname
    ga 'send', 'pageview'
