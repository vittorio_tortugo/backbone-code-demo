class Stoege.Routers.UserCourses extends Backbone.Router
  routes:
    'my-courses': 'index'

  # Инициализуем вручную, для того, чтобы инициализация происходила только при попадании в роут, а не при запуске приложения
  manual_initialize: ->
    @types = new Stoege.Collections.Types
    @user_courses = new Stoege.Collections.UserCourses
    @user_courses_view = new Stoege.Views.UserCourses(types: @types, collection: @user_courses)
    @initialized = true

  index: (params = {}) ->
    @manual_initialize() unless @initialized
    @params or= _.clone(params)
    @user_courses_view.load_courses(params)
    $('.ui-dialog-content').dialog('close')
