class Stoege.Routers.PagesRouter extends Backbone.Router

  loaded: false
  from_url: true

  show: =>
    @load_scripts()
    @slide()
    @close_dialogs()

  load_scripts: ()=>
    unless @loaded
      new Stoege.Views.PageView()
      @loaded = true

  slide: ()=>
    unless @from_url
      pathname = window.location.pathname
      Stoege.Views.PageView.prototype.show_page(pathname)
    @from_url = false
    return false

  close_dialogs: ->
    $('.ui-dialog-content').dialog('close') if $('.ui-dialog-content').length
