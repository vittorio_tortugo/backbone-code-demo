class Stoege.Routers.TasksRouter extends Backbone.Router
  routes:
    'lessons/:lesson_id/tasks/:task_id': 'show'

  show: ->
    @task_view = new Stoege.Views.TaskView unless @task_view?
    @links_questions_view = new Stoege.Views.LinksQuestionView unless @links_questions_view?
    @page_view = new Stoege.Views.PageView unless @page_view?
    @show_page()

  show_page: ->
    active_path = $('ul.page_menu_list .page_menu_section a.active').attr('href')
    pathname = window.location.pathname
    if active_path? and active_path isnt pathname
      Stoege.Views.PageView.prototype.show_page(pathname)
      return false
